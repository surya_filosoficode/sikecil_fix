<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_main extends CI_Model{

#=============================================================================#
#-------------------------------------------pembelian-------------------------#
#=============================================================================#
    // input
    // id_user, id_vendor, jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_pinjaman,
    // tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_pembelian, id_produk, jml_produk, disc, total_bayar, time_update

    public function insert_pembelian($id_user, $id_vendor, $jenis_bayar, $tgl_tempo_start, $tgl_tempo_finish, $sisa_pinjaman, $sts_pinjaman, $tipe_bayar, $keterangan_bayar, $deskripsi, $no_faktur, $tgl_pembelian, $id_produk, $jml_produk, $disc, $harga_satuan, $total_bayar, $time_update){
        $insert = $this->db->query("select insert_pembelian('".$id_user."','".$id_vendor."','".$jenis_bayar."','".$tgl_tempo_start."','".$tgl_tempo_finish."','".$sisa_pinjaman."','".$sts_pinjaman."','".$tipe_bayar."','".$keterangan_bayar."','".$deskripsi."','".$no_faktur."','".$tgl_pembelian."','".$id_produk."','".$jml_produk."','".$disc."','".$total_bayar."','".$time_update."','".$harga_satuan."') as id_pembelian");
        return $insert;
    }

    public function get_pembelian($where){
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->join("produk pr", "pm.id_prd = pr.id_prd");
        $this->db->order_by('pm.tgl_pembelian', 'desc');
        $data = $this->db->get_where("pembelian pm", $where)->result();

        return $data;
    }

    public function get_pembelian_sum($where){
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->join("produk pr", "pm.id_prd = pr.id_prd");
        $this->db->select_sum('pm.total_bayar');
        $this->db->order_by('pm.tgl_pembelian', 'desc');
        $data = $this->db->get_where("pembelian pm", $where)->result();

        return $data;
    }
#=============================================================================#
#-------------------------------------------pembelian-------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------penjualan-------------------------#
#=============================================================================#
    // input
    // id_user, id_vendor, jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_pinjaman,
    // tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_pembelian, id_produk, jml_produk, disc, total_bayar, time_update

    public function insert_penjualan($id_user, $id_vendor, $jenis_bayar, $tgl_tempo_start, $tgl_tempo_finish, $sisa_pinjaman, $sts_pinjaman, $tipe_bayar, $keterangan_bayar, $deskripsi, $no_faktur, $tgl_pembelian, $id_produk, $jml_produk, $disc, $harga_satuan, $total_bayar, $time_update){
        $insert = $this->db->query("select insert_penjualan('".$id_user."','".$id_vendor."','".$jenis_bayar."','".$tgl_tempo_start."','".$tgl_tempo_finish."','".$sisa_pinjaman."','".$sts_pinjaman."','".$tipe_bayar."','".$keterangan_bayar."','".$deskripsi."','".$no_faktur."','".$tgl_pembelian."','".$id_produk."','".$jml_produk."','".$disc."','".$total_bayar."','".$time_update."','".$harga_satuan."') as id_penjualan");
        return $insert;
    }

    public function get_penjualan($where){
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->join("produk pr", "pm.id_prd = pr.id_prd");
        $this->db->order_by('pm.tgl_penjualan', 'desc');
        $data = $this->db->get_where("penjualan pm", $where)->result();
        return $data;
    }

    public function get_penjualan_sum($where){
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->join("produk pr", "pm.id_prd = pr.id_prd");
        $this->db->select_sum('pm.total_bayar');
        $this->db->order_by('pm.tgl_penjualan', 'desc');
        $data = $this->db->get_where("penjualan pm", $where)->result();
        return $data;
    }
#=============================================================================#
#-------------------------------------------penjualan-------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------pengeluaran-----------------------#
#=============================================================================#
    // input
    // id_user, id_vendor, jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_pinjaman,
    // tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_pembelian, id_produk, jml_produk, disc, total_bayar, time_update

    public function insert_pengeluaran($id_user, $id_vendor, $jenis_bayar, $tgl_tempo_start, $tgl_tempo_finish, $sisa_pinjaman, $sts_hutang, $tipe_bayar, $keterangan_bayar, $deskripsi, $no_faktur, $tgl_pengeluaran, $id_biaya, $keterangan_biaya, $total_biaya, $time_update){
        $insert = $this->db->query("select insert_pengeluaran('".$id_user."','".$id_vendor."','".$jenis_bayar."','".$tgl_tempo_start."','".$tgl_tempo_finish."','".$sisa_pinjaman."','".$sts_hutang."','".$tipe_bayar."','".$keterangan_bayar."','".$deskripsi."','".$no_faktur."','".$tgl_pengeluaran."','".$id_biaya."','".$keterangan_biaya."','".$total_biaya."','".$time_update."') as id_pengeluaran");
        return $insert;
    }

    public function get_pengeluaran($where){
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->order_by('pm.tgl_pengeluaran', 'desc');
        $data = $this->db->get_where("pengeluaran pm", $where)->result();
        return $data;
    }

    public function get_pengeluaran_sum($where){
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->select_sum('pm.total_biaya');
        $this->db->order_by('pm.tgl_pengeluaran', 'desc');
        $data = $this->db->get_where("pengeluaran pm", $where)->result();
        return $data;
    }
#=============================================================================#
#-------------------------------------------pengeluaran-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------piutang---------------------------#
#=============================================================================#

    public function insert_piutang($id_user, $no_faktur, $tgl_bayar, $cara_bayar, $total_bayar, $ket_piutang, $tgl_input, $time_update){
        $insert = $this->db->query("select insert_piutang('".$id_user."','".$no_faktur."','".$tgl_bayar."','".$cara_bayar."','".$total_bayar."','".$ket_piutang."','".$tgl_input."','".$time_update."') as id_piutang");
        return $insert;
    }

    public function get_piutang($where){
        $this->db->select("pt.id_piutang, pt.no_faktur, pt.tgl_bayar, pt.cara_bayar, pt.total_bayar as total_bayar_piutang, pt.ket_piutang

            , pm.id_penjualan, pm.id_vdr, pm.jenis_bayar, pm.tgl_tempo_start, pm.tgl_tempo_finish, pm.sisa_pinjaman
            , pm.sts_pinjaman, pm.tipe_bayar, pm.deskripsi, pm.tgl_penjualan, pm.id_prd, pm.jml_produk, pm.total_bayar as total_bayar_penjualan, pm.sts_active

            , vd.nama_vdr

            , pr.nama_prd, pr.id_tipe, pr.satuan_prd");

        $this->db->join("penjualan pm", "pm.id_penjualan = pt.no_faktur");
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->join("produk pr", "pm.id_prd = pr.id_prd");
        $data = $this->db->get_where("piutang pt", $where)->result();
        return $data;
    }

    public function get_piutang_sum($where){
        $this->db->join("penjualan pm", "pm.id_penjualan = pt.no_faktur");
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->join("produk pr", "pm.id_prd = pr.id_prd");
        $this->db->select_sum('pt.total_bayar');
        $data = $this->db->get_where("piutang pt", $where)->result();
        return $data;
    }
#=============================================================================#
#-------------------------------------------piutang---------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------hutang---------------------------#
#=============================================================================#

    public function insert_hutang($id_user, $no_faktur, $tgl_bayar, $cara_bayar, $total_bayar, $ket_hutang, $tgl_input, $time_update){
        $insert = $this->db->query("select insert_hutang('".$id_user."','".$no_faktur."','".$tgl_bayar."','".$cara_bayar."','".$total_bayar."','".$ket_hutang."','".$tgl_input."','".$time_update."') as id_hutang");
        return $insert;
    }

    public function get_hutang_pembelian($where){
        $this->db->select("pt.id_hutang, pt.no_faktur, pt.tgl_bayar, pt.cara_bayar, pt.total_bayar, pt.ket_hutang,

            pm.id_vdr, pm.jenis_bayar, pm.tgl_tempo_start, pm.tgl_tempo_finish, pm.sisa_pinjaman, pm.sts_pinjaman, pm.tipe_bayar, pm.deskripsi, pm.tgl_pembelian, pm.id_prd, pm.jml_produk, pm.total_bayar as total_bayar_pembelian,  pm.sts_active,

            vd.nama_vdr,

            pr.nama_prd, pr.id_tipe, pr.satuan_prd");
        $this->db->join("pembelian pm", "pm.id_pembelian = pt.no_faktur");
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->join("produk pr", "pm.id_prd = pr.id_prd");
        $data = $this->db->get_where("hutang pt", $where)->result();
        return $data;
    }

    public function get_hutang_pengeluaran($where){
        $this->db->select("pt.id_hutang, pt.no_faktur, pt.tgl_bayar, pt.cara_bayar, pt.total_bayar, pt.ket_hutang,

            pm.id_vdr, pm.jenis_bayar, pm.tgl_tempo_start, pm.tgl_tempo_finish, pm.sisa_pinjaman, pm.sts_pinjaman, pm.tipe_bayar, pm.deskripsi, pm.tgl_pengeluaran, pm.id_biaya, pm.keterangan_biaya, pm.total_biaya as total_biaya,  pm.sts_active,

            vd.nama_vdr"
        );

        $this->db->join("pengeluaran pm", "pm.id_pengeluaran = pt.no_faktur");
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $data = $this->db->get_where("hutang pt", $where)->result();
        return $data;
    }


    public function get_hutang_pembelian_sum($where){
        $this->db->join("pembelian pm", "pm.id_pembelian = pt.no_faktur");
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->join("produk pr", "pm.id_prd = pr.id_prd");
        $this->db->select_sum('pt.total_bayar');
        $data = $this->db->get_where("hutang pt", $where)->result();
        return $data;
    }

    public function get_hutang_pengeluaran_sum($where){
        $this->db->join("pengeluaran pm", "pm.id_pengeluaran = pt.no_faktur");
        $this->db->join("kontak vd", "pm.id_vdr = vd.id_vdr");
        $this->db->select_sum('pt.total_bayar');
        $data = $this->db->get_where("hutang pt", $where)->result();
        return $data;
    }
#=============================================================================#
#-------------------------------------------hutang---------------------------#
#=============================================================================#
}
?>