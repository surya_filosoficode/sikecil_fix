<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_main extends CI_Model{

#=============================================================================#
#-------------------------------------------user_login------------------------#
#=============================================================================#

	public function user_login($where, $where_or){
        $this->db->select("id_user, username, email, nama_com, alamat_com, tlp_com, sts_active, sts_delete, nama_user");

        $this->db->or_where("(username = '".$where_or["username"]."'");
        $this->db->or_where("email = '".$where_or["email"]."')");
        
        $data = $this->db->get_where("user", $where)->row_array();
        return $data;
    }

#=============================================================================#
#-------------------------------------------user_login------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------insert_user-----------------------#
#=============================================================================#
    public function insert_user($username, $email, $nama_user, $password, $nama_com, $alamat_com, $time_update, $id_admin, $tlp_com, $sts_active){
    	return $this->db->query("select insert_user(\"".$username."\", \"".$email."\", sha2(\"".$password."\", '256'), \"".$nama_com."\", \"".$alamat_com."\", \"".$time_update."\", \"".$id_admin."\", \"".$tlp_com."\", \"".$sts_active."\", \"".$nama_user."\") as id_user")->row_array();
    }

    public function get_id_from_email($where){
    	return $this->db->get_where("user", $where)->row_array();
    }

    public function insert_user_vert($data){
    	return $this->db->insert("user_vert", $data);
    }
#=============================================================================#
#-------------------------------------------insert_user-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------forget_password-------------------#
#=============================================================================#
    public function get_user_forget($where_or){
        $this->db->or_where("(username = '".$where_or["username"]."'");
        $this->db->or_where("email = '".$where_or["email"]."')");
        
        $data = $this->db->get("user")->row_array();
        return $data;
    }

    public function insert_user_vert_ch_pass($data){
        return $this->db->insert("user_vert_ch_pass", $data);
    }
#=============================================================================#
#-------------------------------------------forget_password-------------------#
#=============================================================================#


}
?>