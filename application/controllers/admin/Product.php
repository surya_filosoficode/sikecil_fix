<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");

        $this->load->library("encrypt");
		
		$this->load->library("get_identity");
		$this->load->library("response_message");

		$session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"] != 1  && $session["is_log"] != 1){
                redirect(base_url()."back-admin/login");
            }
        }else{
            redirect(base_url()."back-admin/login");
        }
	}

#=============================================================================#
#-------------------------------------------product_tipe----------------------#
#=============================================================================#
    public function index_product_tipe(){
    	$data["page"] = "product_tipe";
    	$data["list_produk_tipe"] = $this->mm->get_data_all_where("produk_tipe", array("is_delete"=>"0"));

    	// print_r($data);
        $this->load->view("index", $data);
    }

    public function val_form(){
        $config_val_input = array(
                array(
                    'field'=>'ket_tipe',
                    'label'=>'ket_tipe',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_tipe(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "ket_tipe"=>""
                );

        if($this->val_form()){
            $ket_tipe = $this->input->post("ket_tipe");
            
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $data = array(
                        "id_tipe"=>"",
                        "ket_tipe"=>$ket_tipe,
                        "is_delete"=>"0",
                        "time_update"=>$time_update,
                        "id_admin"=>$admin_del
                    );

            $insert = $this->mm->insert_data("produk_tipe", $data);
            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "ket_tipe"=>strip_tags(form_error('ket_tipe'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_tipe_update(){
        $id = $this->encrypt->decode($this->input->post("id_tipe"));
        $data = $this->mm->get_data_each("produk_tipe", array("id_tipe"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }
    
    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'ket_tipe',
                    'label'=>'ket_tipe',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_tipe(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "ket_tipe"=>""
                );

        if($this->val_form_update()){
            $ket_tipe = $this->input->post("ket_tipe");
            $id_tipe = $this->input->post("id_tipe");
            
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array(
                        "ket_tipe"=>$ket_tipe,
                        "is_delete"=>"0",
                        "time_update"=>$time_update,
                        "id_admin"=>$admin_del
                    );
            $where = array(
                        "id_tipe"=>$id_tipe
                    );

            $update = $this->mm->update_data("produk_tipe", $set, $where);
            if($update){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
            $msg_detail = array(
                            "ket_tipe"=>strip_tags(form_error('ket_tipe'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'id_tipe',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_tipe(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete()){
            $id_tipe = $this->encrypt->decode($this->input->post("id_tipe"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $set = array(
                    "is_delete"=>$is_del,
                    "id_admin"=>$admin_del,
                    "time_update"=>$time_del
                );

            $where = array("id_tipe"=>$id_tipe);

            if($this->mm->update_data("produk_tipe", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------product_tipe----------------------#
#=============================================================================#


}
?>