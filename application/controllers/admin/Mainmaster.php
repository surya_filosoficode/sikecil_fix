<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmaster extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");

        $this->load->library("encrypt");
		
		$this->load->library("get_identity");
		$this->load->library("response_message");

		$session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"] != 1  && $session["is_log"] != 1){
                redirect(base_url()."back-admin/login");
            }
        }else{
            redirect(base_url()."back-admin/login");
        }
	}

#=============================================================================#
#-------------------------------------------bayar_tipe----------------------#
#=============================================================================#
    public function index_bayar_tipe(){
    	$data["page"] = "bayar_tipe";
    	$data["list_bayar_tipe"] = $this->mm->get_data_all_where("tipe_bayar", array("is_delete"=>"0"));

    	// print_r($data);
        $this->load->view("index", $data);
    }

    public function val_form_bayar(){
        $config_val_input = array(
                array(
                    'field'=>'cara_bayar',
                    'label'=>'cara_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_tipe_bayar(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "cara_bayar"=>""
                );

        if($this->val_form_bayar()){
            $cara_bayar = $this->input->post("cara_bayar");
            
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $data = array(
                        "id_bayar"=>"",
                        "cara_bayar"=>$cara_bayar,
                        "is_delete"=>"0",
                        "time_update"=>$time_update,
                        "id_admin"=>$admin_del
                    );

            $insert = $this->mm->insert_data("tipe_bayar", $data);
            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "cara_bayar"=>strip_tags(form_error('cara_bayar'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_tipe_update_bayar(){
        $id = $this->encrypt->decode($this->input->post("id_bayar"));
        $data = $this->mm->get_data_each("tipe_bayar", array("id_bayar"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }
    
    public function val_form_update_bayar(){
        $config_val_input = array(
                array(
                    'field'=>'cara_bayar',
                    'label'=>'cara_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_tipe_bayar(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "cara_bayar"=>""
                );

        if($this->val_form_update_bayar()){
            $cara_bayar = $this->input->post("cara_bayar");
            $id_bayar = $this->input->post("id_bayar");
            
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array(
                        "cara_bayar"=>$cara_bayar,
                        "is_delete"=>"0",
                        "time_update"=>$time_update,
                        "id_admin"=>$admin_del
                    );
            $where = array(
                        "id_bayar"=>$id_bayar
                    );

            $update = $this->mm->update_data("tipe_bayar", $set, $where);
            if($update){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
            $msg_detail = array(
                            "cara_bayar"=>strip_tags(form_error('cara_bayar'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete_bayar(){
        $config_val_input = array(
                array(
                    'field'=>'id_bayar',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_tipe_bayar(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_bayar()){
            $id_bayar = $this->encrypt->decode($this->input->post("id_bayar"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $set = array(
                    "is_delete"=>$is_del,
                    "id_admin"=>$admin_del,
                    "time_update"=>$time_del
                );

            $where = array("id_bayar"=>$id_bayar);

            if($this->mm->update_data("tipe_bayar", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------bayar_tipe----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------biaya_tipe----------------------#
#=============================================================================#
    public function index_biaya_tipe(){
        $data["page"] = "biaya_tipe";
        $data["list_biaya_tipe"] = $this->mm->get_data_all_where("tipe_biaya", array("is_delete"=>"0"));

        // print_r($data);
        $this->load->view("index", $data);
    }

    public function val_form_biaya(){
        $config_val_input = array(
                array(
                    'field'=>'ket_tipe_biaya',
                    'label'=>'ket_tipe_biaya',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_tipe_biaya(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "ket_tipe_biaya"=>""
                );

        if($this->val_form_biaya()){
            $ket_tipe_biaya = $this->input->post("ket_tipe_biaya");
            
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $data = array(
                        "id_biaya"=>"",
                        "ket_tipe_biaya"=>$ket_tipe_biaya,
                        "is_delete"=>"0",
                        "time_update"=>$time_update,
                        "id_admin"=>$admin_del
                    );

            $insert = $this->mm->insert_data("tipe_biaya", $data);
            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "ket_tipe_biaya"=>strip_tags(form_error('ket_tipe_biaya'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_tipe_update_biaya(){
        $id = $this->encrypt->decode($this->input->post("id_biaya"));
        $data = $this->mm->get_data_each("tipe_biaya", array("id_biaya"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }
    
    public function val_form_update_biaya(){
        $config_val_input = array(
                array(
                    'field'=>'ket_tipe_biaya',
                    'label'=>'ket_tipe_biaya',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_tipe_biaya(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "ket_tipe_biaya"=>""
                );

        if($this->val_form_update_biaya()){
            $ket_tipe_biaya = $this->input->post("ket_tipe_biaya");
            $id_biaya = $this->input->post("id_biaya");
            
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array(
                        "ket_tipe_biaya"=>$ket_tipe_biaya,
                        "is_delete"=>"0",
                        "time_update"=>$time_update,
                        "id_admin"=>$admin_del
                    );
            $where = array(
                        "id_biaya"=>$id_biaya
                    );

            $update = $this->mm->update_data("tipe_biaya", $set, $where);
            if($update){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
            $msg_detail = array(
                            "ket_tipe_biaya"=>strip_tags(form_error('ket_tipe_biaya'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete_biaya(){
        $config_val_input = array(
                array(
                    'field'=>'id_biaya',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_tipe_biaya(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_biaya()){
            $id_biaya = $this->encrypt->decode($this->input->post("id_biaya"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $set = array(
                    "is_delete"=>$is_del,
                    "id_admin"=>$admin_del,
                    "time_update"=>$time_del
                );

            $where = array("id_biaya"=>$id_biaya);

            if($this->mm->update_data("tipe_biaya", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------biaya_tipe----------------------#
#=============================================================================#


#=============================================================================#
#-------------------------------------------satuan----------------------------#
#=============================================================================#
    public function index_satuan(){
        $data["page"] = "satuan";
        $data["list_satuan"] = $this->mm->get_data_all_where("satuan", array("is_delete"=>"0"));

        // print_r($data);
        $this->load->view("index", $data);
    }

    public function val_form_satuan(){
        $config_val_input = array(
                array(
                    'field'=>'ket_satuan',
                    'label'=>'ket_satuan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_satuan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "ket_satuan"=>""
                );

        if($this->val_form_satuan()){
            $ket_satuan = $this->input->post("ket_satuan");
            
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $data = array(
                        "id_satuan"=>"",
                        "ket_satuan"=>$ket_satuan,
                        "is_delete"=>"0",
                        "time_update"=>$time_update,
                        "id_admin"=>$admin_del
                    );

            $insert = $this->mm->insert_data("satuan", $data);
            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "ket_satuan"=>strip_tags(form_error('ket_satuan'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_tipe_update_satuan(){
        $id = $this->encrypt->decode($this->input->post("id_satuan"));
        $data = $this->mm->get_data_each("satuan", array("id_satuan"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }
    
    public function val_form_update_satuan(){
        $config_val_input = array(
                array(
                    'field'=>'ket_satuan',
                    'label'=>'ket_satuan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_satuan(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "ket_satuan"=>""
                );

        if($this->val_form_update_satuan()){
            $ket_satuan = $this->input->post("ket_satuan");
            $id_satuan = $this->input->post("id_satuan");
            
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array(
                        "ket_satuan"=>$ket_satuan,
                        "is_delete"=>"0",
                        "time_update"=>$time_update,
                        "id_admin"=>$admin_del
                    );
            $where = array(
                        "id_satuan"=>$id_satuan
                    );

            $update = $this->mm->update_data("satuan", $set, $where);
            if($update){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
            $msg_detail = array(
                            "ket_satuan"=>strip_tags(form_error('ket_satuan'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete_satuan(){
        $config_val_input = array(
                array(
                    'field'=>'id_satuan',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_satuan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_satuan()){
            $id_satuan = $this->encrypt->decode($this->input->post("id_satuan"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $set = array(
                    "is_delete"=>$is_del,
                    "id_admin"=>$admin_del,
                    "time_update"=>$time_del
                );

            $where = array("id_satuan"=>$id_satuan);

            if($this->mm->update_data("satuan", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------satuan----------------------------#
#=============================================================================#

}
?>