<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmain extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("Admin_main", "am");

        $this->load->library("encrypt");
		
		$this->load->library("get_identity");
		$this->load->library("response_message");

		$session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"] != 1  && $session["is_log"] != 1){
                redirect(base_url()."back-admin/login");
            }
        }else{
            redirect(base_url()."back-admin/login");
        }
	}
    
    public function index(){
    	$data["page"] = "home";
        $data["data_count_user"] = count($this->mm->get_data_all("user"));
        $data["data_count_penjualan"] = count($this->mm->get_data_all("penjualan"));
        $data["data_count_pengeluaran"] = count($this->mm->get_data_all("pengeluaran"));
        $data["data_count_pembelian"] = count($this->mm->get_data_all("pembelian"));
        $data["data_count_hutang"] = count($this->mm->get_data_all("hutang"));
        $data["data_count_piutang"] = count($this->mm->get_data_all("piutang"));

        $data_all = array();

        $data_user = $this->mm->get_data_all("user");
        foreach ($data_user as $key => $value) {
            $data_all[$value->id_user]["user"] = $value;
            $data_all[$value->id_user]["count"]["produk"]       = count($this->mm->get_data_all("produk", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["kontak"]       = count($this->mm->get_data_all("kontak", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["penjualan"]    = count($this->mm->get_data_all("penjualan", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["pengeluaran"]  = count($this->mm->get_data_all("pengeluaran", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["pembelian"]    = count($this->mm->get_data_all("pembelian", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["hutang"]       = count($this->mm->get_data_all("hutang", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["piutang"]      = count($this->mm->get_data_all("piutang", array("id_user"=>$value->id_user)));
        }

        $data["data_all"] = $data_all;

        // print_r($data);
        $this->load->view("index", $data);
    }


#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#
    public function index_admin(){
    	$data["page"] = "page_admin";
    	$data["admin"] = $this->am->select_admin_all(array("is_delete"=>"0"));

        $this->load->view("index", $data);
    }

   	public function val_form(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails|is_unique[admin.email]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required|is_unique[admin.username]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    ) 
                ),array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'pass',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repass',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "username"=>"",
                    "nama"=>"",
                    "pass"=>"",
                    "repass"=>""
                );

        if($this->val_form()){
            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $username = $this->input->post("username");
            $pass = $this->input->post("pass");
            $repass = $this->input->post("repass");

            $admin_del = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update = date("Y-m-d h:i:s");

            if ($pass == $repass) {
            	// print_r($_POST);
                $insert = $this->db->query("select insert_admin('".$nama."','".$email."','".md5($pass)."','".$time_update."','".$admin_del."','".$username."')");
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "email"=>strip_tags(form_error('email')),
                            "nama"=>strip_tags(form_error('nama')),
                            "username"=>strip_tags(form_error('username')),
                            "pass"=>strip_tags(form_error('pass')),
                            "repass"=>strip_tags(form_error('repass'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_admin_update(){
        $id = $this->encrypt->decode($this->input->post("id_admin"));
        $data = $this->mm->get_data_each("admin", array("id_admin"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }
    
    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_admin(){
    	// print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "nama"=>"",
                    "username"=>""
                );

        if($this->val_form_update()){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));

            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $username = $this->input->post("username");

            $admin_del = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update = date("Y-m-d h:i:s");

            if($this->mm->get_data_each("admin", array("email"=>$email, "id_admin!="=>$id_admin))){
                $msg_detail["email"] = "email sudah terdaftar, silahkan gunakan email yang belum terdaftar";
            }else{
                $set = array(
                        "nama"=>$nama,
                        "email"=>$email,
                        "username"=>$username
                    );

                $where = array(
                            "id_admin"=>$id_admin
                        );

                $update = $this->mm->update_data("admin", $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }            
        }else{
            $msg_detail["email"] = strip_tags(form_error('email'));
            $msg_detail["nama"] = strip_tags(form_error('nama'));
            $msg_detail["username"] = strip_tags(form_error('username'));
                            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete()){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "time_update"=>$time_del
                );

            $where = array("id_admin"=>$id_admin);

            if($this->mm->update_data("admin", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }


    public function get_password(){
        $res_array = array(
                    "status"=>false,
                    "val_key"=>""
                );

        if(isset($_POST["id_admin"])){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));
            $data = $this->mm->get_data_each("admin", array("id_admin"=>$id_admin));

            $res_array = array(
                    "status"=>true,
                    "val_key"=>hash('sha256', $data["password"])
                );
        }
        // print_r($data);
        print_r(json_encode($res_array));
    }

    private function validation_change_pass(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'key',
                    'label'=>'key',
                    'rules'=>'required|alpha_numeric|exact_length[64]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR"),
                        'exact_length[64]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    )
                       
                ),


                array(
                    'field'=>'pass',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'repass',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "pass"=>"",
                    "repass"=>""
                );

        // print_r($_POST);

        if($this->validation_change_pass()){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));
            $key = $this->input->post("key");

            $pass = $this->input->post("pass");
            $repass = $this->input->post("repass");

            if($pass == $repass){
                $data_admin = $this->mm->get_data_each("admin", array("id_admin"=>$id_admin));
                // print_r("</br>");
                // print_r($data_admin);
                // print_r($key);
                // print_r("</br>");
                // print_r(hash('sha256', $data_admin["password"]));
                // print_r("</br>");
                if(hash('sha256', $data_admin["password"]) == $key){
                    $set = array(
                            "password"=>md5($pass),
                        );

                    $where = array(
                                "id_admin"=>$id_admin
                            );

                    $update = $this->mm->update_data("admin", $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }else{
                	// print_r("gasama");
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
            }            
        }else{
            // $msg_detail["id_admin"] = strip_tags(form_error('id_admin'));
            // $msg_detail["key"] = strip_tags(form_error('key'));

            $msg_detail["pass"] = strip_tags(form_error('pass'));
            $msg_detail["repass"] = strip_tags(form_error('repass'));                            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }



    public function activate_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("ACTIVATION_FAIL"));
        if($this->val_form_delete()){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));

            $sts_active = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "status_active"=>$sts_active,
                    "time_update"=>$time_del
                );

            $where = array("id_admin"=>$id_admin);

            if($this->mm->update_data("admin", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("ACTIVATION_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }

    public function unactivate_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("ACTIVATION_FAIL"));
        if($this->val_form_delete()){
            $id_admin = $this->encrypt->decode($this->input->post("id_admin"));

            $sts_active = "0";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "status_active"=>$sts_active,
                    "time_update"=>$time_del
                );

            $where = array("id_admin"=>$id_admin);

            if($this->mm->update_data("admin", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("ACTIVATION_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }


#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------User------------------------------#
#=============================================================================#
    public function index_user(){
    	$data["page"] = "page_user";
    	// $data["user"] = $this->mm->get_data_all_where("user", array("sts_delete"=>"0"));

        $data_all = array();

        $data_user = $this->mm->get_data_all("user");
        foreach ($data_user as $key => $value) {
            $data_all[$value->id_user]["user"] = $value;
            $data_all[$value->id_user]["count"]["produk"]       = count($this->mm->get_data_all("produk", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["kontak"]       = count($this->mm->get_data_all("kontak", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["penjualan"]    = count($this->mm->get_data_all("penjualan", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["pengeluaran"]  = count($this->mm->get_data_all("pengeluaran", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["pembelian"]    = count($this->mm->get_data_all("pembelian", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["hutang"]       = count($this->mm->get_data_all("hutang", array("id_user"=>$value->id_user)));
            $data_all[$value->id_user]["count"]["piutang"]      = count($this->mm->get_data_all("piutang", array("id_user"=>$value->id_user)));
        }

        $data["user"] = $data_all;


    	// print_r($data);
        $this->load->view("index", $data);
    }

    public function val_form_delete_user(){
        $config_val_input = array(
                array(
                    'field'=>'id_user',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_user()){
            $id_user = $this->encrypt->decode($this->input->post("id_user"));

            $where = array("id_user"=>$id_user);
            $delete_user = $this->mm->delete_data("user", $where);

            $array_delete = array();
            if($delete_user){
                $delete_produk = $this->mm->delete_data("produk", $where);
                array_push($array_delete, $delete_produk);

                $delete_kontak = $this->mm->delete_data("kontak", $where);
                array_push($array_delete, $delete_kontak);

                $delete_hutang = $this->mm->delete_data("hutang", $where);
                array_push($array_delete, $delete_hutang);

                $delete_pembelian = $this->mm->delete_data("pembelian", $where);
                array_push($array_delete, $delete_pembelian);

                $delete_pengeluaran = $this->mm->delete_data("pengeluaran", $where);
                array_push($array_delete, $delete_pengeluaran);

                $delete_penjualan = $this->mm->delete_data("penjualan", $where);
                array_push($array_delete, $delete_penjualan);

                $delete_piutang = $this->mm->delete_data("piutang", $where);
                array_push($array_delete, $delete_piutang);
            }

            if(!in_array(false, $array_delete)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }

    public function unactivate_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_user()){
            $id_user = $this->encrypt->decode($this->input->post("id_user"));

            $sts_active = "2";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "sts_active"=>$sts_active,
                    "time_update"=>$time_del
                );

            $where = array("id_user"=>$id_user);

            if($this->mm->update_data("user", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }

    public function activate_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_user()){
            $id_user = $this->encrypt->decode($this->input->post("id_user"));

            $sts_active = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "sts_active"=>$sts_active,
                    "time_update"=>$time_del
                );

            $where = array("id_user"=>$id_user);

            if($this->mm->update_data("user", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------User------------------------------#
#=============================================================================#
}
?>