<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermain extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('user/user_main', 'um');
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("encrypt");
        $this->load->library("sendemail");
        $this->load->library("generate_token");
	}
    
#=============================================================================#
#-------------------------------------------register_user---------------------#
#=============================================================================#

    private function val_form_reg(){
        $config_val_input = array(
                array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required|min_length[5]|is_unique[user.username]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'min_length[5]'=>"%s ".$this->response_message->get_error_msg("USERNAME_LENGHT"),
                        'is_unique[user.username]'=>"%s ".$this->response_message->get_error_msg("USERNAME_AVAIL")
                    )
                       
                ),
                array(
                    'field'=>'email',
                    'label'=>'email',
                    'rules'=>'required|valid_email|is_unique[user.email]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique[user.email]'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )
                       
                ),
                array(
                    'field'=>'nama_user',
                    'label'=>'nama_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'password',
                    'rules'=>'required|min_length[4]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'min_length[5]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    )
                       
                ),
                array(
                    'field'=>'repassword',
                    'label'=>'repassword',
                    'rules'=>'required|min_length[4]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'min_length[5]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    )
                       
                ),
                array(
                    'field'=>'nama_com',
                    'label'=>'nama_com',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'alamat_com',
                    'label'=>'alamat_com',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'tlp_com',
                    'label'=>'tlp_com',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    public function insert_register(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array("username" => "",
                            "email" => "",
                            "nama_user" => "",
                            "password" => "",
                            "repassword" => "",
                            "nama_com" => "",
                            "alamat_com" => "",
                            "tlp_com" => "",
                            "token" => "");

        if($this->val_form_reg()){
            $username     = $this->input->post('username');
    		$email        = $this->input->post('email');
            $nama_user    = $this->input->post('nama_user');
            $password     = $this->input->post('password');
            $repassword   = $this->input->post('repassword');
            $nama_com     = $this->input->post('nama_com');
            $alamat_com   = $this->input->post('alamat_com');
            $tlp_com      = $this->input->post('tlp_com');
            $token        = $this->input->post('token');

            $id_admin     = "0";
            $time_update  = date("Y-m-d h:i:s");
            $sts_active   = "0";

            if($token == "FC094X"){
                if($password == $repassword){
                    $insert = $this->um->insert_user($username, $email, $nama_user, $password, $nama_com, $alamat_com, $time_update, $id_admin, $tlp_com, $sts_active);
                    if($insert){
                        $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("REG_SUC"));
                        
                        $link = $this->get_link($email);
                        $this->sendemail->send_email_vert($email,$link,"Verifikasi Registrasi");
                    }
                }else{
                    $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                }
            }else{
                $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("ACCESS_FAIL"));
            }
        }else{
            $msg_detail["username"]     = strip_tags(form_error("username"));
            $msg_detail["email"]        = strip_tags(form_error("email"));
            $msg_detail["nama_user"]    = strip_tags(form_error("nama_user"));
            $msg_detail["password"]     = strip_tags(form_error("password"));
            $msg_detail["repassword"]   = strip_tags(form_error("repassword"));
            $msg_detail["nama_com"]     = strip_tags(form_error("nama_com"));
            $msg_detail["alamat_com"]   = strip_tags(form_error("alamat_com"));
            $msg_detail["tlp_com"]      = strip_tags(form_error("tlp_com"));
            $msg_detail["token"]        = strip_tags(form_error("token"));
        }
        
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }


    private function get_link($email){
        $id = $this->um->get_id_from_email(array("email"=>$email));
        $code = $this->generate_token->generate_link_email();

        if($this->mm->get_data_each("user_vert", array("id_user"=>$id["id_user"]))){
            $data = array(
                    "time"=>date("Y-m-d H:i:s"),
                    "param"=>$code["param"],
                    "code"=>$code["code"]);
        
            if($this->mm->update_data("user_vert", $data, array("id_user"=>$id["id_user"]))){
                return base_url()."user/verification/active/".hash("sha256", $id["id_user"])."/?".$code["param"]."=".$code["code"];    
            }
        }else{
            $data = array(
                    "id_user"=>$id["id_user"],
                    "time"=>date("Y-m-d H:i:s"),
                    "param"=>$code["param"],
                    "code"=>$code["code"]);
        
            if($this->um->insert_user_vert($data)){
                return base_url()."user/verification/active/".hash("sha256", $id["id_user"])."/?".$code["param"]."=".$code["code"];    
            }
        }

        return null; 
    }
#=============================================================================#
#-------------------------------------------register_user---------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------verifikasi_user-------------------#
#=============================================================================#
    public function vert_user($id_user){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = null;

        $param_code = $this->mm->get_data_each("user_vert", array("sha2(id_user, '256')="=>$id_user));
        if($param_code != null){
            if(isset($_GET[$param_code["param"]])){
                if($_GET[$param_code["param"]] == $param_code["code"]){
                    if($this->mm->update_data("user", array("sts_active"=>"1"), array("sha2(id_user, '256')="=>$id_user))){

                        if($this->mm->delete_data("user_vert", array("sha2(id_user, '256')="=>$id_user))){
                          $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("ACTIVATION_SUC"));
                        }
                    }
                }    
            }
        }

        $msg_array = $this->response_message->default_mgs($msg_main,null);

        print_r(json_encode($msg_array));
        redirect("https://www.google.com/");
    }

    public function vert_user_fg_pass($id_user){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = null;

        $param_code = $this->mm->get_data_each("user_vert_ch_pass", array("sha2(id_user, '256')="=>$id_user));
        if($param_code != null){
            if(isset($_GET[$param_code["param"]])){
                if($_GET[$param_code["param"]] == $param_code["code"]){
                    if($this->mm->update_data("user", array("sts_active"=>"1"), array("sha2(id_user, '256')="=>$id_user))){

                        if($this->mm->delete_data("user_vert_ch_pass", array("sha2(id_user, '256')="=>$id_user))){

                            $data_user = $this->mm->get_data_each("user", array("sha2(id_user, '256')="=>$id_user));
                            // print_r($user);

                            $email_send     = $data_user["email"];
                            $username_send  = $data_user["username"];

                            $id_user        = $data_user["id_user"];
                            $new_password   = $this->generate_token->generate_forget_password();

                            $update_data = $this->mm->update_data("user", array("password"=>hash("sha256", $new_password)), array("id_user"=>$id_user));
                            if($update_data){
                                $sendemail = $this->sendemail->send_email_forget_password($email_send, $username_send, $new_password, "permintaan pemberitahuan password");
                                // print_r($new_password);
                            }
                        }
                    }
                }    
            }
        }

        // $msg_array = $this->response_message->default_mgs($msg_main,null);

        // print_r(json_encode($msg_array));
        redirect("https://www.google.com/");
    }
#=============================================================================#
#-------------------------------------------verifikasi_user-------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------forget_password-------------------#
#=============================================================================#
    private function val_forget_password(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function forget_password(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array("email" => "");

        if($this->val_forget_password()){
            $email = $this->input->post("email");
            $token = $this->input->post('token');

            if($token == "FC094X"){
                $link = $this->get_link_chpass($email);
                $this->sendemail->send_email_vert_ch_pass($email,$link,"Verifikasi Perubahan Password");

                $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("CHANGE_PASS_SUC"));
            }
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }

    private function get_link_chpass($email){
        $id = $this->um->get_id_from_email(array("email"=>$email));
        $code = $this->generate_token->generate_link_email();

        if($this->mm->get_data_each("user_vert_ch_pass", array("id_user"=>$id["id_user"]))){
            $data = array(
                    "time"=>date("Y-m-d H:i:s"),
                    "param"=>$code["param"],
                    "code"=>$code["code"]);
        
            if($this->mm->update_data("user_vert_ch_pass", $data, array("id_user"=>$id["id_user"]))){
                return base_url()."user/verification/chpass/".hash("sha256", $id["id_user"])."/?".$code["param"]."=".$code["code"];    
            }
        }else{
            $data = array(
                    "id_user"=>$id["id_user"],
                    "time"=>date("Y-m-d H:i:s"),
                    "param"=>$code["param"],
                    "code"=>$code["code"]);
        
            if($this->um->insert_user_vert_ch_pass($data)){
                return base_url()."user/verification/chpass/".hash("sha256", $id["id_user"])."/?".$code["param"]."=".$code["code"];    
            }
        }

        return null; 
    }
#=============================================================================#
#-------------------------------------------forget_password-------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------change_password-------------------#
#=============================================================================#
    private function val_change_password(){
        $config_val_input = array(
                array(
                    'field'=>'password_old',
                    'label'=>'password_old',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'password_new',
                    'label'=>'password_new',
                    'rules'=>'required|min_length[4]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'min_length[5]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    )
                       
                ),
                array(
                    'field'=>'re_password_new',
                    'label'=>'re_password_new',
                    'rules'=>'required|min_length[4]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'min_length[5]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    )
                       
                ),
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_password(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("CHANGE_PASS_FAIL"));
        $msg_detail = array(
                            "password_old" => "",
                            "password_new" => "",
                            "re_password_new" => "",
                            "token" => "");

        if($this->val_change_password()){
            $id_user = $this->input->post('id_user');
            $password_old   = $this->input->post('password_old');
            $password_new   = $this->input->post('password_new');
            $re_password_new = $this->input->post('re_password_new');
            $token          = $this->input->post('token');

            $where = array(
                    'id_user' => $id_user
                );

            $get_data = $this->mm->get_data_each("user", $where);
            // print_r($get_data);
            if($token == "FC094X"){
                if(hash("sha256", $password_old) == $get_data["password"]){
                    if($password_new == $re_password_new){
                        $update_data = $this->mm->update_data("user", array("password"=>hash("sha256", $password_new)), $where);

                        $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("CHANGE_PASS_SUC"));
                    }else{
                        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                    }
                }else{
                    $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("MEMBER_NOT_FOUND"));
                }
            }else{
                $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("ACCESS_FAIL"));
            }
        }else{
            $msg_detail["password_old"] = strip_tags(form_error("password_old"));
            $msg_detail["password_new"] = strip_tags(form_error("password_new"));
            $msg_detail["re_password_new"] = strip_tags(form_error("re_password_new"));
            $msg_detail["token"]        = strip_tags(form_error("token"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=============================================================================#
#-------------------------------------------change_password-------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------edit_user-------------------------#
#=============================================================================#
    private function val_edit_user(){
        $config_val_input = array(
                array(
                    'field'=>'nama_user',
                    'label'=>'nama_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'nama_com',
                    'label'=>'nama_com',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'alamat_com',
                    'label'=>'alamat_com',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'tlp_com',
                    'label'=>'tlp_com',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_user(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array("nama_user" => "",
                            "nama_com" => "",
                            "alamat_com" => "",
                            "tlp_com" => "",
                            "token" => "");

        if($this->val_edit_user()){
            $id_user =$this->input->post('id_user');
            $nama_user    = $this->input->post('nama_user');
            $nama_com     = $this->input->post('nama_com');
            $alamat_com   = $this->input->post('alamat_com');
            $tlp_com      = $this->input->post('tlp_com');
            $token        = $this->input->post('token');

            $time_update  = date("Y-m-d h:i:s");

            $set = array("nama_user"=>$nama_user,
                            "nama_com"=>$nama_com,
                            "alamat_com"=>$alamat_com,
                            "tlp_com"=>$tlp_com);

            $where = array("id_user"=>$id_user);

            if($token == "FC094X"){
                $update_user = $this->mm->update_data("user", $set, $where);
                if($update_user){
                    $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }else{
                $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("ACCESS_FAIL"));
            }
        }else{
            $msg_detail["nama_user"]    = strip_tags(form_error("nama_user"));
            $msg_detail["nama_com"]     = strip_tags(form_error("nama_com"));
            $msg_detail["alamat_com"]   = strip_tags(form_error("alamat_com"));
            $msg_detail["tlp_com"]      = strip_tags(form_error("tlp_com"));
            $msg_detail["token"]        = strip_tags(form_error("token"));
        }
        
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=============================================================================#
#-------------------------------------------edit_user-------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------change_email----------------------#
#=============================================================================#
    private function val_change_email(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_email',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_email(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("CHANGE_EMAIL_FAIL"));
        $msg_detail = array("email" => "");

        if($this->val_change_email()){
            $email = $this->input->post('email');
            $token = $this->input->post('token');

            $id_user = $this->input->post('id_user');
            $get_data_user = $this->mm->get_data_each("user", array("id_user"=> $id_user));


            if($token == "FC094X"){
                $where = array("id_user != "=> $id_user, "email"=> $email);
                $get_email = $this->mm->get_data_each("user", $where);

                if($email != $get_data_user["email"]){
                    if($get_email == false){
                        $set = array("email"=>$email, "sts_active"=>"0");
                        $where_update = array("id_user"=>$id_user);
                        $update_email = $this->mm->update_data("user", $set, $where_update);
                        
                        if($update_email){
                            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("CHANGE_EMAIL_SUC"));

                            $link = $this->get_link($email);
                            $this->sendemail->send_email_vert($email,$link,"Verifikasi Perubahan Email");
                        }
                    }
                }
                
            }else{
                $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("ACCESS_FAIL"));
            }
        }else{
            $msg_detail["email"]    = strip_tags(form_error("email"));
            $msg_detail["token"]    = strip_tags(form_error("token"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=============================================================================#
#-------------------------------------------change_email----------------------#
#=============================================================================#


#=============================================================================#
#-------------------------------------------verifikasi_manual-----------------#
#=============================================================================#
    private function val_cek_vert(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'email',
                    'rules'=>'required|valid_email',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function cek_vert(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array("email" => "", "token"=> "");

        if($this->val_cek_vert()){
            $email = $this->input->post("email");
            $token = $this->input->post('token');

            if($token == "FC094X"){
                $link_email = $this->get_link($email);
                if($link_email!=null){
                    $this->sendemail->send_email_vert($email,$link_email,"Verifikasi Registrasi");
                    $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("VERT_SUC"));
                }else{
                    $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("EMAIL_NOT_AVAIL"));
                }
            }else{
                $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("ACCESS_FAIL"));
            }

        }else{
            $msg_detail["email"]     = strip_tags(form_error("email"));
            $msg_detail["token"]     = strip_tags(form_error("token"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
        // print_r();
    }
#=============================================================================#
#-------------------------------------------verifikasi_manual-----------------#
#=============================================================================#

}
?>