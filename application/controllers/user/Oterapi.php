<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Oterapi extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");

        $this->load->library("encrypt");
		
		$this->load->library("get_identity");
		$this->load->library("response_message");

	}

#=============================================================================#
#-------------------------------------------Oterapi_tipe----------------------#
#=============================================================================#
    public function general_api(){
        $config_val_input = array(
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_cara_bayar_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->general_api()){
            $token = $this->input->post("token");
            if($token == "FC094X"){
                $data = $this->mm->get_data_all_where("tipe_bayar", array("is_delete"=>"0", "is_delete"=> "0"));
                if($data){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["item"] = $data;
                }
            }
        }else{
            $msg_detail["token"] = strip_tags(form_error('token'));
        }
        
    	$res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_tipe_biaya_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->general_api()){
            $token = $this->input->post("token");
            if($token == "FC094X"){

                $data = $this->mm->get_data_all_where("tipe_biaya", array("is_delete"=>"0", "is_delete"=> "0"));
                if($data){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["item"] = $data;
                }
                
            }
        }else{
            $msg_detail["token"] = strip_tags(form_error('token'));
        }
        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


#=============================================================================#
#-------------------------------------------Oterapi_tipe----------------------#
#=============================================================================#


}
?>