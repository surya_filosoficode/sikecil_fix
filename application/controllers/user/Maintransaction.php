<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintransaction extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
        $this->load->model("user/transaction_main", "pm");

        // $this->load->library("encrypt");
		
		$this->load->library("get_identity");
		$this->load->library("response_message");
	}

#=============================================================================#
#-------------------------------------------main_data_master------------------#
#=============================================================================#
    // input
    // id_user, id_vendor, jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_pinjaman,
    // tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_pembelian, id_produk, jml_produk, disc, total_bayar, time_update
    public function val_get_standart_token(){
        $config_val_input = array(
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )      
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_vendor_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );

        if($this->val_get_standart_token()){
            $token      = $this->input->post("token");
            $id_user    = $this->input->post("id_user");

            if($token == "FC094X"){
                $data = $this->mm->get_data_all_where("kontak", array("id_user"=>$id_user));
                $msg_main           = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail["item"] = $data;
            }
        }else{
            $msg_detail["token"] = strip_tags(form_error('token'));
        }

        $array_msg = array($msg_main, $msg_detail);
    	print_r(json_encode($array_msg));
    }

    public function get_product_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );

        if($this->val_get_standart_token()){
            $token      = $this->input->post("token");
            $id_user    = $this->input->post("id_user");

            if($token == "FC094X"){
                $data = $this->mm->get_data_all_where("produk", array("id_user"=>$id_user));
                $msg_main           = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail["item"] = $data;
            }
        }else{
            $msg_detail["token"] = strip_tags(form_error('token'));
        }
        
        $array_msg = array($msg_main, $msg_detail);
        print_r(json_encode($array_msg));
    }

    public function get_tipe_biaya_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );

        if($this->val_get_standart_token()){
            $token      = $this->input->post("token");

            if($token == "FC094X"){
                $data = $this->mm->get_data_all_where("tipe_biaya", array("is_delete"=>"0"));
                $msg_main           = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail["item"] = $data;
            }
        }else{
            $msg_detail["token"] = strip_tags(form_error('token'));
        }
        
        $array_msg = array($msg_main, $msg_detail);
        print_r(json_encode($array_msg));
    }

    public function get_tipe_bayar_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );

        if($this->val_get_standart_token()){
            $token      = $this->input->post("token");
            $id_user    = $this->input->post("id_user");

            if($token == "FC094X"){
                $data = $this->mm->get_data_all_where("tipe_bayar", array("is_delete"=>"0"));
                $msg_main           = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail["item"] = $data;
            }
        }else{
            $msg_detail["token"] = strip_tags(form_error('token'));
        }
        
        $array_msg = array($msg_main, $msg_detail);
        print_r(json_encode($array_msg));
    }
#=============================================================================#
#-------------------------------------------main_data_master------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------pembelian_main--------------------#
#=============================================================================#
    public function val_form_pembelian(){
        $config_val_input = array(
                array(
                    'field'=>'id_user',
                    'label'=>'id_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_vdr',
                    'label'=>'id_vdr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'jenis_bayar',
                    'label'=>'jenis_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_tempo_start',
                    'label'=>'tgl_tempo_start',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_tempo_finish',
                    'label'=>'tgl_tempo_finish',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'sisa_pinjaman',
                    'label'=>'sisa_pinjaman',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'sts_pinjaman',
                    'label'=>'sts_pinjaman',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tipe_bayar',
                    'label'=>'tipe_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'keterangan_bayar',
                    'label'=>'keterangan_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'deskripsi',
                    'label'=>'deskripsi',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'no_faktur',
                    'label'=>'no_faktur',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_pembelian',
                    'label'=>'tgl_pembelian',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_produk',
                    'label'=>'id_produk',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'jml_produk',
                    'label'=>'jml_produk',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'disc',
                    'label'=>'disc',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'harga_satuan',
                    'label'=>'harga_satuan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'total_bayar',
                    'label'=>'total_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )      
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_pembelian(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                    "id_vdr"=>"",
                    "jenis_bayar"=>"",
                    "tgl_tempo_start"=>"",
                    "tgl_tempo_finish"=>"",
                    "sisa_pinjaman"=>"",
                    "sts_pinjaman"=>"",
                    "tipe_bayar"=>"",
                    "keterangan_bayar"=>"",
                    "deskripsi"=>"",
                    "no_faktur"=>"",
                    "tgl_pembelian"=>"",
                    "id_produk"=>"",
                    "jml_produk"=>"",

                    "disc"=>"",
                    "harga_satuan"=>"",
                    "total_bayar"=>"",
                    "token"=>""
                );

        if($this->val_form_pembelian()){
            $id_user            = $this->input->post("id_user");
            $id_vdr             = $this->input->post("id_vdr");
            $jenis_bayar        = $this->input->post("jenis_bayar");
            $tgl_tempo_start    = $this->input->post("tgl_tempo_start");
            $tgl_tempo_finish   = $this->input->post("tgl_tempo_finish");
            $sisa_pinjaman      = $this->input->post("sisa_pinjaman");
            $sts_pinjaman       = $this->input->post("sts_pinjaman");
            $tipe_bayar         = $this->input->post("tipe_bayar");
            $keterangan_bayar   = $this->input->post("keterangan_bayar");
            $deskripsi          = $this->input->post("deskripsi");
            $no_faktur          = $this->input->post("no_faktur");
            $tgl_pembelian      = $this->input->post("tgl_pembelian");
            $id_produk          = $this->input->post("id_produk");
            $jml_produk         = $this->input->post("jml_produk");

            $disc               = $this->input->post("disc");
            $harga_satuan       = $this->input->post("harga_satuan");
            $total_bayar        = $this->input->post("total_bayar");
            $token              = $this->input->post("token");

            $id_admin = "0";
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $t_bayar_send = $harga_satuan*$jml_produk;
                $disc_send = 0;
                if($disc != 0){
                    $disc_send = ($disc/100)*$t_bayar_send;
                }

                $t_bayar = $t_bayar_send-$disc_send;

                $data_stok = $this->mm->get_data_each("produk", array("id_prd"=>$id_produk));
                $stok_real = $data_stok["stok"]+$jml_produk;

                // print_r($jml_produk);

                $insert = $this->pm->insert_pembelian($id_user, $id_vdr, $jenis_bayar, $tgl_tempo_start, $tgl_tempo_finish, $sisa_pinjaman, $sts_pinjaman, $tipe_bayar, $keterangan_bayar, $deskripsi, $no_faktur, $tgl_pembelian, $id_produk, $jml_produk, $disc, $harga_satuan, $t_bayar, $time_update);
                if($insert){
                    $set_up_stok = array("stok"=>$stok_real);
                    $where_up_stok = array("id_prd"=>$id_produk);

                    $update_stok = $this->mm->update_data("produk", $set_up_stok, $where_up_stok);
                    if($update_stok){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
                    $msg_detail["jenis_bayar"]      = strip_tags(form_error('jenis_bayar'));
                    $msg_detail["tgl_tempo_start"]  = strip_tags(form_error('tgl_tempo_start'));
                    $msg_detail["tgl_tempo_finish"] = strip_tags(form_error('tgl_tempo_finish'));
                    $msg_detail["sisa_pinjaman"]    = strip_tags(form_error('sisa_pinjaman'));
                    $msg_detail["sts_pinjaman"]     = strip_tags(form_error('sts_pinjaman'));
                    $msg_detail["tipe_bayar"]       = strip_tags(form_error('tipe_bayar'));
                    $msg_detail["keterangan_bayar"] = strip_tags(form_error('keterangan_bayar'));
                    $msg_detail["deskripsi"]        = strip_tags(form_error('deskripsi'));
                    $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur'));
                    $msg_detail["tgl_pembelian"]    = strip_tags(form_error('tgl_pembelian'));
                    $msg_detail["id_produk"]        = strip_tags(form_error('id_produk'));
                    $msg_detail["jml_produk"]       = strip_tags(form_error('jml_produk'));

                    $msg_detail["disc"]             = strip_tags(form_error('disc'));
                    $msg_detail["total_bayar"]      = strip_tags(form_error('total_bayar'));
                    $msg_detail["harga_satuan"]     = strip_tags(form_error('harga_satuan'));
                    $msg_detail["token"]            = strip_tags(form_error('token'));            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_pembelian(){
        $id_pembelian   = $this->input->post("id_pembelian");
        $data           = $this->pm->get_pembelian(array("id_pembelian"=>$id_pembelian));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }

    public function update_pembelian(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                    "id_vdr"=>"",
                    "jenis_bayar"=>"",
                    "tgl_tempo_start"=>"",
                    "tgl_tempo_finish"=>"",
                    "sisa_pinjaman"=>"",
                    "sts_pinjaman"=>"",
                    "tipe_bayar"=>"",
                    "keterangan_bayar"=>"",
                    "deskripsi"=>"",
                    "no_faktur"=>"",
                    "tgl_pembelian"=>"",
                    "id_produk"=>"",
                    "jml_produk"=>"",

                    "disc"=>"",
                    "harga_satuan"=>"",
                    "total_bayar"=>"",
                    "token"=>""
                );

        if($this->val_form_pembelian()){
            $id_user            = $this->input->post("id_user");
            $id_vdr             = $this->input->post("id_vdr");
            $jenis_bayar        = $this->input->post("jenis_bayar");
            $tgl_tempo_start    = $this->input->post("tgl_tempo_start");
            $tgl_tempo_finish   = $this->input->post("tgl_tempo_finish");
            $sisa_pinjaman      = $this->input->post("sisa_pinjaman");
            $sts_pinjaman       = $this->input->post("sts_pinjaman");
            $tipe_bayar         = $this->input->post("tipe_bayar");
            $keterangan_bayar   = $this->input->post("keterangan_bayar");
            $deskripsi          = $this->input->post("deskripsi");
            $no_faktur          = $this->input->post("no_faktur");
            $tgl_pembelian      = $this->input->post("tgl_pembelian");
            $id_produk          = $this->input->post("id_produk");
            $jml_produk         = $this->input->post("jml_produk");

            $disc               = $this->input->post("disc");
            $harga_satuan       = $this->input->post("harga_satuan");
            $total_bayar        = $this->input->post("total_bayar");
            $token              = $this->input->post("token");

            $id_pembelian       = $this->input->post("id_pembelian");

            $id_admin = "0";
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $t_bayar_send = $harga_satuan*$jml_produk;
                $disc_send = 0;
                if($disc != 0){
                    $disc_send = ($disc/100)*$t_bayar_send;
                }

                $t_bayar = $t_bayar_send-$disc_send;

                $set = array(
                        "id_vdr"=>$id_vdr,
                        "jenis_bayar"=>$jenis_bayar,
                        "tgl_tempo_start"=>$tgl_tempo_start,
                        "tgl_tempo_finish"=>$tgl_tempo_finish,
                        "sisa_pinjaman"=>$sisa_pinjaman,
                        "sts_pinjaman"=>$sts_pinjaman,
                        "tipe_bayar"=>$tipe_bayar,
                        "keterangan_bayar"=>$keterangan_bayar,

                        "deskripsi"=>$deskripsi,
                        "no_faktur"=>$no_faktur,
                        "tgl_pembelian"=>$tgl_pembelian,
                        "id_prd"=>$id_produk,
                        "jml_produk"=>$jml_produk,
                        "disc"=>$disc,
                        "harga_satuan"=>$harga_satuan,
                        "total_bayar"=>$t_bayar
                    );

                $where = array(
                            "id_pembelian"=>$id_pembelian
                        );

                $data_pembelian_before = $this->mm->get_data_each("pembelian", $where);
                $jml_prd_before_update = $data_pembelian_before["jml_produk"];
                $id_produk_before      = $data_pembelian_before["id_prd"];

                $update = $this->mm->update_data("pembelian", $set, $where);
                if($update){
                    $data_stok_before = $this->mm->get_data_each("produk", array("id_prd"=>$id_produk_before));
                    $stok_real_before = $data_stok_before["stok"] - $jml_prd_before_update;

                    $set_up_stok_before = array("stok"=>$stok_real_before);
                    $where_up_stok_before = array("id_prd"=>$id_produk_before);

                    $update_stok_before = $this->mm->update_data("produk", $set_up_stok_before, $where_up_stok_before);
                    if($update_stok_before){
                        $data_stok_after    = $this->mm->get_data_each("produk", array("id_prd"=>$id_produk));
                        $stok_real_after    = $data_stok_after["stok"] + $jml_produk;

                        $set_up_stok_after = array("stok"=>$stok_real_after);
                        $where_up_stok_after = array("id_prd"=>$id_produk);

                        $update_stok_after = $this->mm->update_data("produk", $set_up_stok_after, $where_up_stok_after);
                        if($update_stok_after){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                        }
                    }
                }else {
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                }
            }
        }else{    
            $msg_detail["id_vdr"]           = strip_tags(form_error('id_vdr')); 
            $msg_detail["jenis_bayar"]      = strip_tags(form_error('id_user')); 
            $msg_detail["tgl_tempo_start"]  = strip_tags(form_error('tgl_tempo_start')); 
            $msg_detail["tgl_tempo_finish"] = strip_tags(form_error('tgl_tempo_finish')); 
            $msg_detail["sisa_pinjaman"]    = strip_tags(form_error('sisa_pinjaman')); 
            $msg_detail["sts_pinjaman"]     = strip_tags(form_error('sts_pinjaman'));             
            $msg_detail["tipe_bayar"]       = strip_tags(form_error('tipe_bayar')); 
            $msg_detail["keterangan_bayar"] = strip_tags(form_error('keterangan_bayar')); 
            $msg_detail["deskripsi"]        = strip_tags(form_error('deskripsi')); 
            $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur')); 
            $msg_detail["tgl_pembelian"]    = strip_tags(form_error('tgl_pembelian')); 
            $msg_detail["id_produk"]        = strip_tags(form_error('id_produk')); 
            $msg_detail["jml_produk"]       = strip_tags(form_error('jml_produk')); 
            $msg_detail["disc"]             = strip_tags(form_error('disc')); 
            $msg_detail["harga_satuan"]     = strip_tags(form_error('harga_satuan')); 
            $msg_detail["total_bayar"]      = strip_tags(form_error('total_bayar')); 
            $msg_detail["token"]            = strip_tags(form_error('token')); 
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete_pembelian(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_get_standart_token()){
            $id_pembelian     = $this->input->post("id_pembelian");
            $token      = $this->input->post("token");
            if($token == "FC094X"){

                $where = array(
                            "id_pembelian"=>$id_pembelian
                        );

                $data_pembelian_before = $this->mm->get_data_each("pembelian", $where);
                $jml_prd_before_update = $data_pembelian_before["jml_produk"];
                $id_produk_before      = $data_pembelian_before["id_prd"];

                $delete_pembelian = $this->mm->delete_data("pembelian", array("id_pembelian"=>$id_pembelian));
                if($delete_pembelian){
                    $delete_hutang = $this->mm->delete_data("hutang", array("no_faktur"=>$id_pembelian));
                    
                    if($delete_hutang){
                        $data_stok_before = $this->mm->get_data_each("produk", array("id_prd"=>$id_produk_before));
                        $stok_real_before = $data_stok_before["stok"] - $jml_prd_before_update;

                        $set_up_stok_before = array("stok"=>$stok_real_before);
                        $where_up_stok_before = array("id_prd"=>$id_produk_before);

                        $update_stok_before = $this->mm->update_data("produk", $set_up_stok_before, $where_up_stok_before);
                        if($update_stok_before){
                           $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                        }
                    }
                    
                }else {
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                }
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }


    public function get_pembelian_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_standart_token()){
            $token = $this->input->post("token");
            $id_user = $this->input->post("id_user");

            if($token == "FC094X"){
                // print_r($_POST);
                $data = $this->pm->get_pembelian(array("pm.id_user"=>$id_user, "pm.is_delete"=>"0"));
                // print_r($data);
                if($data){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["item"] = $data;
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------pembelian_main--------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------penjualan_main--------------------#
#=============================================================================#
    public function val_form_penjualan(){
        $config_val_input = array(
                array(
                    'field'=>'id_user',
                    'label'=>'id_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_vdr',
                    'label'=>'id_vdr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'jenis_bayar',
                    'label'=>'jenis_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_tempo_start',
                    'label'=>'tgl_tempo_start',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_tempo_finish',
                    'label'=>'tgl_tempo_finish',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'sisa_pinjaman',
                    'label'=>'sisa_pinjaman',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'sts_pinjaman',
                    'label'=>'sts_pinjaman',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tipe_bayar',
                    'label'=>'tipe_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'keterangan_bayar',
                    'label'=>'keterangan_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'deskripsi',
                    'label'=>'deskripsi',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'no_faktur',
                    'label'=>'no_faktur',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_penjualan',
                    'label'=>'tgl_penjualan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_produk',
                    'label'=>'id_produk',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'jml_produk',
                    'label'=>'jml_produk',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'disc',
                    'label'=>'disc',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'harga_satuan',
                    'label'=>'harga_satuan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'total_bayar',
                    'label'=>'total_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )      
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_penjualan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                    "id_vdr"=>"",
                    "jenis_bayar"=>"",
                    "tgl_tempo_start"=>"",
                    "tgl_tempo_finish"=>"",
                    "sisa_pinjaman"=>"",
                    "sts_pinjaman"=>"",
                    "tipe_bayar"=>"",
                    "keterangan_bayar"=>"",
                    "deskripsi"=>"",
                    "no_faktur"=>"",
                    "tgl_penjualan"=>"",
                    "id_produk"=>"",
                    "jml_produk"=>"",

                    "disc"=>"",
                    "harga_satuan"=>"",
                    "total_bayar"=>"",
                    "token"=>""
                );

        if($this->val_form_penjualan()){
            $id_user            = $this->input->post("id_user");
            $id_vdr             = $this->input->post("id_vdr");
            $jenis_bayar        = $this->input->post("jenis_bayar");
            $tgl_tempo_start    = $this->input->post("tgl_tempo_start");
            $tgl_tempo_finish   = $this->input->post("tgl_tempo_finish");
            $sisa_pinjaman      = $this->input->post("sisa_pinjaman");
            $sts_pinjaman       = $this->input->post("sts_pinjaman");
            $tipe_bayar         = $this->input->post("tipe_bayar");
            $keterangan_bayar   = $this->input->post("keterangan_bayar");
            $deskripsi          = $this->input->post("deskripsi");
            $no_faktur          = $this->input->post("no_faktur");
            $tgl_penjualan      = $this->input->post("tgl_penjualan");
            $id_produk          = $this->input->post("id_produk");
            $jml_produk         = $this->input->post("jml_produk");

            $disc               = $this->input->post("disc");
            $harga_satuan       = $this->input->post("harga_satuan");
            $total_bayar        = $this->input->post("total_bayar");
            $token              = $this->input->post("token");

            $id_admin = "0";
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $t_bayar_send = $harga_satuan*$jml_produk;
                $disc_send = 0;
                if($disc != 0){
                    $disc_send = ($disc/100)*$t_bayar_send;
                }

                $t_bayar = $t_bayar_send-$disc_send;

                $data_stok = $this->mm->get_data_each("produk", array("id_prd"=>$id_produk));
                $stok_real = $data_stok["stok"]-$jml_produk;

                $insert = $this->pm->insert_penjualan($id_user, $id_vdr, $jenis_bayar, $tgl_tempo_start, $tgl_tempo_finish, $sisa_pinjaman, $sts_pinjaman, $tipe_bayar, $keterangan_bayar, $deskripsi, $no_faktur, $tgl_penjualan, $id_produk, $jml_produk, $disc, $harga_satuan, $t_bayar, $time_update);
                if($insert){
                    $set_up_stok = array("stok"=>$stok_real);
                    $where_up_stok = array("id_prd"=>$id_produk);

                    $update_stok = $this->mm->update_data("produk", $set_up_stok, $where_up_stok);
                    if($update_stok){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
                    $msg_detail["jenis_bayar"]      = strip_tags(form_error('jenis_bayar'));
                    $msg_detail["tgl_tempo_start"]  = strip_tags(form_error('tgl_tempo_start'));
                    $msg_detail["tgl_tempo_finish"] = strip_tags(form_error('tgl_tempo_finish'));
                    $msg_detail["sisa_pinjaman"]    = strip_tags(form_error('sisa_pinjaman'));
                    $msg_detail["sts_pinjaman"]     = strip_tags(form_error('sts_pinjaman'));
                    $msg_detail["tipe_bayar"]       = strip_tags(form_error('tipe_bayar'));
                    $msg_detail["keterangan_bayar"] = strip_tags(form_error('keterangan_bayar'));
                    $msg_detail["deskripsi"]        = strip_tags(form_error('deskripsi'));
                    $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur'));
                    $msg_detail["tgl_penjualan"]    = strip_tags(form_error('tgl_penjualan'));
                    $msg_detail["id_produk"]        = strip_tags(form_error('id_produk'));
                    $msg_detail["jml_produk"]       = strip_tags(form_error('jml_produk'));

                    $msg_detail["disc"]             = strip_tags(form_error('disc'));
                    $msg_detail["total_bayar"]      = strip_tags(form_error('total_bayar'));
                    $msg_detail["harga_satuan"]     = strip_tags(form_error('harga_satuan'));
                    $msg_detail["token"]            = strip_tags(form_error('token'));            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_penjualan(){
        $id_penjualan   = $this->input->post("id_penjualan");
        $data           = $this->pm->get_penjualan(array("id_penjualan"=>$id_penjualan));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }

    public function update_penjualan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                    "id_vdr"=>"",
                    "jenis_bayar"=>"",
                    "tgl_tempo_start"=>"",
                    "tgl_tempo_finish"=>"",
                    "sisa_pinjaman"=>"",
                    "sts_pinjaman"=>"",
                    "tipe_bayar"=>"",
                    "keterangan_bayar"=>"",
                    "deskripsi"=>"",
                    "no_faktur"=>"",
                    "tgl_penjualan"=>"",
                    "id_produk"=>"",
                    "jml_produk"=>"",

                    "disc"=>"",
                    "harga_satuan"=>"",
                    "total_bayar"=>"",
                    "token"=>""
                );

        if($this->val_form_penjualan()){
            $id_user            = $this->input->post("id_user");
            $id_vdr             = $this->input->post("id_vdr");
            $jenis_bayar        = $this->input->post("jenis_bayar");
            $tgl_tempo_start    = $this->input->post("tgl_tempo_start");
            $tgl_tempo_finish   = $this->input->post("tgl_tempo_finish");
            $sisa_pinjaman      = $this->input->post("sisa_pinjaman");
            $sts_pinjaman       = $this->input->post("sts_pinjaman");
            $tipe_bayar         = $this->input->post("tipe_bayar");
            $keterangan_bayar   = $this->input->post("keterangan_bayar");
            $deskripsi          = $this->input->post("deskripsi");
            $no_faktur          = $this->input->post("no_faktur");
            $tgl_penjualan      = $this->input->post("tgl_penjualan");
            $id_produk          = $this->input->post("id_produk");
            $jml_produk         = $this->input->post("jml_produk");

            $disc               = $this->input->post("disc");
            $harga_satuan       = $this->input->post("harga_satuan");
            $total_bayar        = $this->input->post("total_bayar");
            $token              = $this->input->post("token");

            $id_penjualan       = $this->input->post("id_penjualan");

            $id_admin = "0";
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $t_bayar_send = $harga_satuan*$jml_produk;
                $disc_send = 0;
                if($disc != 0){
                    $disc_send = ($disc/100)*$t_bayar_send;
                }

                $t_bayar = $t_bayar_send-$disc_send;
                $set = array(
                        "id_vdr"=>$id_vdr,
                        "jenis_bayar"=>$jenis_bayar,
                        "tgl_tempo_start"=>$tgl_tempo_start,
                        "tgl_tempo_finish"=>$tgl_tempo_finish,
                        "sisa_pinjaman"=>$sisa_pinjaman,
                        "sts_pinjaman"=>$sts_pinjaman,
                        "tipe_bayar"=>$tipe_bayar,
                        "keterangan_bayar"=>$keterangan_bayar,

                        "deskripsi"=>$deskripsi,
                        "no_faktur"=>$no_faktur,
                        "tgl_penjualan"=>$tgl_penjualan,
                        "id_prd"=>$id_produk,
                        "jml_produk"=>$jml_produk,
                        "disc"=>$disc,
                        "harga_satuan"=>$harga_satuan,
                        "total_bayar"=>$t_bayar
                    );

                $where = array(
                            "id_penjualan"=>$id_penjualan
                        );

                $data_pembelian_before = $this->mm->get_data_each("penjualan", $where);
                $jml_prd_before_update = $data_pembelian_before["jml_produk"];
                $id_produk_before      = $data_pembelian_before["id_prd"];

                $update = $this->mm->update_data("penjualan", $set, $where);
                if($update){
                    $data_stok_before = $this->mm->get_data_each("produk", array("id_prd"=>$id_produk_before));
                    $stok_real_before = $data_stok_before["stok"] + $jml_prd_before_update;

                    $set_up_stok_before = array("stok"=>$stok_real_before);
                    $where_up_stok_before = array("id_prd"=>$id_produk_before);

                    $update_stok_before = $this->mm->update_data("produk", $set_up_stok_before, $where_up_stok_before);
                    if($update_stok_before){
                        $data_stok_after    = $this->mm->get_data_each("produk", array("id_prd"=>$id_produk));
                        $stok_real_after    = $data_stok_after["stok"] - $jml_produk;

                        $set_up_stok_after = array("stok"=>$stok_real_after);
                        $where_up_stok_after = array("id_prd"=>$id_produk);

                        $update_stok_after = $this->mm->update_data("produk", $set_up_stok_after, $where_up_stok_after);
                        if($update_stok_after){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                        }
                    }
                }else {
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                }
            }
        }else{    
            $msg_detail["id_vdr"]           = strip_tags(form_error('id_vdr')); 
            $msg_detail["jenis_bayar"]      = strip_tags(form_error('id_user')); 
            $msg_detail["tgl_tempo_start"]  = strip_tags(form_error('tgl_tempo_start')); 
            $msg_detail["tgl_tempo_finish"] = strip_tags(form_error('tgl_tempo_finish')); 
            $msg_detail["sisa_pinjaman"]    = strip_tags(form_error('sisa_pinjaman')); 
            $msg_detail["sts_pinjaman"]     = strip_tags(form_error('sts_pinjaman'));             
            $msg_detail["tipe_bayar"]       = strip_tags(form_error('tipe_bayar')); 
            $msg_detail["keterangan_bayar"] = strip_tags(form_error('keterangan_bayar')); 
            $msg_detail["deskripsi"]        = strip_tags(form_error('deskripsi')); 
            $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur')); 
            $msg_detail["tgl_penjualan"]    = strip_tags(form_error('tgl_penjualan')); 
            $msg_detail["id_produk"]        = strip_tags(form_error('id_produk')); 
            $msg_detail["jml_produk"]       = strip_tags(form_error('jml_produk')); 
            $msg_detail["disc"]             = strip_tags(form_error('disc')); 
            $msg_detail["harga_satuan"]     = strip_tags(form_error('harga_satuan'));
            $msg_detail["total_bayar"]      = strip_tags(form_error('total_bayar')); 
            $msg_detail["token"]            = strip_tags(form_error('token')); 
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete_penjualan(){
        $msg_main   = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );

        if($this->val_get_standart_token()){
            $token          = $this->input->post("token");
            $id_penjualan   = $this->input->post("id_penjualan");

            if($token == "FC094X"){
                $data_penjualan = $this->mm->get_data_each("penjualan", array("id_penjualan"=>$id_penjualan));
                $id_prd = "";
                $jml_penjualan = 0;

                $jml_stok = 0;
                $jml_stok_fix = 0;

                if($data_penjualan){
                    $id_prd         = $data_penjualan["id_prd"];
                    $jml_penjualan  = $data_penjualan["jml_produk"];

                    $data_stok = $this->mm->get_data_each("produk", array("id_prd"=>$id_prd));
                    if($data_stok){
                        $jml_stok = $data_stok["stok"];
                        $jml_stok_fix = $jml_stok + $jml_penjualan;
                    }
                }


                $delete_piutang = $this->mm->delete_data("piutang", array("no_faktur"=>$id_penjualan));
                if($delete_piutang){
                    $delete_penjualan = $this->mm->delete_data("penjualan", array("id_penjualan"=>$id_penjualan));
                    if($delete_penjualan){
                        $update_produk = $this->mm->update_data("produk", array("stok"=>$jml_stok_fix), array("id_prd"=>$id_prd));
                        if($update_produk){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                        }
                    }
                }
            }
        }else{
            $msg_detail["token"] =strip_tags(form_error('token'));
        }

        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_penjualan_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_standart_token()){
            $token = $this->input->post("token");
            $id_user = $this->input->post("id_user");

            if($token == "FC094X"){
                // print_r($_POST);
                $data = $this->pm->get_penjualan(array("pm.id_user"=>$id_user, "pm.is_delete"=>"0"));
                // print_r($data);
                if($data){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["item"] = $data;
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
        $array_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($array_msg));
    }
#=============================================================================#
#-------------------------------------------penjualan_main--------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------pengeluaran_main------------------#
#=============================================================================#
    public function val_form_pengeluaran(){
        $config_val_input = array(
                array(
                    'field'=>'id_user',
                    'label'=>'id_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_vdr',
                    'label'=>'id_vdr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'jenis_bayar',
                    'label'=>'jenis_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_tempo_start',
                    'label'=>'tgl_tempo_start',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_tempo_finish',
                    'label'=>'tgl_tempo_finish',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'sisa_pinjaman',
                    'label'=>'sisa_pinjaman',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'sts_pinjaman',
                    'label'=>'sts_pinjaman',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tipe_bayar',
                    'label'=>'tipe_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'keterangan_bayar',
                    'label'=>'keterangan_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'deskripsi',
                    'label'=>'deskripsi',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'no_faktur',
                    'label'=>'no_faktur',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_pengeluaran',
                    'label'=>'tgl_pengeluaran',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_biaya',
                    'label'=>'id_biaya',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'keterangan_biaya',
                    'label'=>'keterangan_biaya',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'total_biaya',
                    'label'=>'total_biaya',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )      
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_pengeluaran(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                    "id_vdr"=>"",
                    "jenis_bayar"=>"",
                    "tgl_tempo_start"=>"",
                    "tgl_tempo_finish"=>"",
                    "sisa_pinjaman"=>"",
                    "sts_pinjaman"=>"",
                    "tipe_bayar"=>"",
                    "keterangan_bayar"=>"",
                    "deskripsi"=>"",
                    "no_faktur"=>"",
                    "tgl_pengeluaran"=>"",
                    "id_biaya"=>"",
                    "keterangan_biaya"=>"",
                    "total_biaya"=>"",
                    "token"=>""
                );

        if($this->val_form_pengeluaran()){
            $id_user            = $this->input->post("id_user");
            $id_vdr             = $this->input->post("id_vdr");
            $jenis_bayar        = $this->input->post("jenis_bayar");
            $tgl_tempo_start    = $this->input->post("tgl_tempo_start");
            $tgl_tempo_finish   = $this->input->post("tgl_tempo_finish");
            $sisa_pinjaman      = $this->input->post("sisa_pinjaman");
            $sts_pinjaman       = $this->input->post("sts_pinjaman");
            $tipe_bayar         = $this->input->post("tipe_bayar");
            $keterangan_bayar   = $this->input->post("keterangan_bayar");
            $deskripsi          = $this->input->post("deskripsi");
            $no_faktur          = $this->input->post("no_faktur");
            $tgl_pengeluaran    = $this->input->post("tgl_pengeluaran");
            $id_biaya           = $this->input->post("id_biaya");
            $keterangan_biaya   = $this->input->post("keterangan_biaya");

            $total_biaya        = $this->input->post("total_biaya");
            $token              = $this->input->post("token");

            $id_admin = "0";
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){

                $insert = $this->pm->insert_pengeluaran($id_user, $id_vdr, $jenis_bayar, $tgl_tempo_start, $tgl_tempo_finish, $sisa_pinjaman, $sts_pinjaman, $tipe_bayar, $keterangan_bayar, $deskripsi, $no_faktur, $tgl_pengeluaran, $id_biaya, $keterangan_biaya, $total_biaya, $time_update);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
                    $msg_detail["jenis_bayar"]      = strip_tags(form_error('jenis_bayar'));
                    $msg_detail["tgl_tempo_start"]  = strip_tags(form_error('tgl_tempo_start'));
                    $msg_detail["tgl_tempo_finish"] = strip_tags(form_error('tgl_tempo_finish'));
                    $msg_detail["sisa_pinjaman"]    = strip_tags(form_error('sisa_pinjaman'));
                    $msg_detail["sts_pinjaman"]     = strip_tags(form_error('sts_pinjaman'));
                    $msg_detail["tipe_bayar"]       = strip_tags(form_error('tipe_bayar'));
                    $msg_detail["keterangan_bayar"] = strip_tags(form_error('keterangan_bayar'));
                    $msg_detail["deskripsi"]        = strip_tags(form_error('deskripsi'));
                    $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur'));
                    $msg_detail["tgl_pengeluaran"]  = strip_tags(form_error('tgl_pengeluaran'));
                    $msg_detail["id_biaya"]         = strip_tags(form_error('id_biaya'));
                    $msg_detail["keterangan_biaya"] = strip_tags(form_error('keterangan_biaya'));

                    $msg_detail["total_biaya"]      = strip_tags(form_error('total_biaya'));
                    $msg_detail["token"]            = strip_tags(form_error('token'));            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_pengeluaran(){
        $id_pengeluaran   = $this->input->post("id_pengeluaran");
        $data           = $this->pm->get_pengeluaran(array("id_pengeluaran"=>$id_pengeluaran));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }


    public function update_pengeluaran(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                    "id_vdr"=>"",
                    "jenis_bayar"=>"",
                    "tgl_tempo_start"=>"",
                    "tgl_tempo_finish"=>"",
                    "sisa_pinjaman"=>"",
                    "sts_pinjaman"=>"",
                    "tipe_bayar"=>"",
                    "keterangan_bayar"=>"",
                    "deskripsi"=>"",
                    "no_faktur"=>"",
                    "tgl_pengeluaran"=>"",
                    "id_biaya"=>"",
                    "keterangan_biaya"=>"",
                    "total_biaya"=>"",
                    "token"=>""
                );

        if($this->val_form_pengeluaran()){
            $id_user            = $this->input->post("id_user");
            $id_vdr             = $this->input->post("id_vdr");
            $jenis_bayar        = $this->input->post("jenis_bayar");
            $tgl_tempo_start    = $this->input->post("tgl_tempo_start");
            $tgl_tempo_finish   = $this->input->post("tgl_tempo_finish");
            $sisa_pinjaman      = $this->input->post("sisa_pinjaman");
            $sts_pinjaman       = $this->input->post("sts_pinjaman");
            $tipe_bayar         = $this->input->post("tipe_bayar");
            $keterangan_bayar   = $this->input->post("keterangan_bayar");
            $deskripsi          = $this->input->post("deskripsi");
            $no_faktur          = $this->input->post("no_faktur");
            $tgl_pengeluaran    = $this->input->post("tgl_pengeluaran");
            $id_biaya           = $this->input->post("id_biaya");
            $keterangan_biaya   = $this->input->post("keterangan_biaya");

            $total_biaya        = $this->input->post("total_biaya");
            $token              = $this->input->post("token");

            $id_pengeluaran       = $this->input->post("id_pengeluaran");

            $id_admin = "0";
            $time_update = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $set = array(
                        "id_vdr"=>$id_vdr,
                        "jenis_bayar"=>$jenis_bayar,
                        "tgl_tempo_start"=>$tgl_tempo_start,
                        "tgl_tempo_finish"=>$tgl_tempo_finish,
                        "sisa_pinjaman"=>$sisa_pinjaman,
                        "sts_pinjaman"=>$sts_pinjaman,
                        "tipe_bayar"=>$tipe_bayar,
                        "keterangan_bayar"=>$keterangan_bayar,

                        "deskripsi"=>$deskripsi,
                        "no_faktur"=>$no_faktur,
                        "tgl_pengeluaran"=>$tgl_pengeluaran,
                        "id_biaya"=>$id_biaya,
                        "keterangan_biaya"=>$keterangan_biaya,
                        "total_biaya"=>$total_biaya,
                    );

                $where = array(
                            "id_pengeluaran"=>$id_pengeluaran
                        );

                $update = $this->mm->update_data("pengeluaran", $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }else {
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                }
            }
        }else{    
                    $msg_detail["jenis_bayar"]      = strip_tags(form_error('jenis_bayar'));
                    $msg_detail["tgl_tempo_start"]  = strip_tags(form_error('tgl_tempo_start'));
                    $msg_detail["tgl_tempo_finish"] = strip_tags(form_error('tgl_tempo_finish'));
                    $msg_detail["sisa_pinjaman"]    = strip_tags(form_error('sisa_pinjaman'));
                    $msg_detail["sts_pinjaman"]     = strip_tags(form_error('sts_pinjaman'));
                    $msg_detail["tipe_bayar"]       = strip_tags(form_error('tipe_bayar'));
                    $msg_detail["keterangan_bayar"] = strip_tags(form_error('keterangan_bayar'));
                    $msg_detail["deskripsi"]        = strip_tags(form_error('deskripsi'));
                    $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur'));
                    $msg_detail["tgl_pengeluaran"]  = strip_tags(form_error('tgl_pengeluaran'));
                    $msg_detail["id_biaya"]         = strip_tags(form_error('id_biaya'));
                    $msg_detail["keterangan_biaya"] = trip_tags(form_error('keterangan_biaya'));

                    $msg_detail["total_biaya"]      = strip_tags(form_error('total_biaya'));
                    $msg_detail["token"]            = strip_tags(form_error('token'));       
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete_pengeluaran(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_get_standart_token()){
            $id_pengeluaran = $this->input->post("id_pengeluaran");
            $token          = $this->input->post("token");
            if($token == "FC094X"){
                if($this->mm->delete_data("hutang", array("no_faktur"=>$id_pengeluaran))){
                    if($this->mm->delete_data("pengeluaran", array("id_pengeluaran"=>$id_pengeluaran))){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                    }
                }
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }


    public function get_pengeluaran_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_standart_token()){
            $token = $this->input->post("token");
            $id_user = $this->input->post("id_user");

            if($token == "FC094X"){
                // print_r($_POST);
                $data = $this->pm->get_pengeluaran(array("pm.id_user"=>$id_user, "pm.is_delete"=>"0"));
                // print_r($data);
                if($data){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["item"] = $data;
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
        $array_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($array_msg));
    }
#=============================================================================#
#-------------------------------------------pengeluaran_main------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------hutang_main-----------------------#
#=============================================================================#
    public function val_form_hutang(){
        $config_val_input = array(
                array(
                    'field'=>'tgl_bayar',
                    'label'=>'tgl_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'cara_bayar',
                    'label'=>'cara_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'total_bayar',
                    'label'=>'total_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'no_faktur',
                    'label'=>'no_faktur',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'ket_hutang',
                    'label'=>'ket_hutang',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )  
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_hutang(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tgl_bayar"=>"",
                    "cara_bayar"=>"",
                    "total_bayar"=>"",
                    "no_faktur"=>"",
                    "ket_hutang"=>""
                );

        if($this->val_form_hutang()){
            $id_user            = $this->input->post("id_user");
            $tgl_bayar          = $this->input->post("tgl_bayar");
            $cara_bayar         = $this->input->post("cara_bayar");
            $total_bayar        = $this->input->post("total_bayar");
            $no_faktur          = $this->input->post("no_faktur");
            $ket_hutang         = $this->input->post("ket_hutang");
            $token              = $this->input->post("token");

            $id_admin           = "0";
            $tgl_input          = date("Y-m-d");
            $time_update        = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $kode_hutang = explode("-", $no_faktur)[1];
                if($kode_hutang == "PB"){
                    $data_pembelian = $this->mm->get_data_each("pembelian", array("id_pembelian"=>$no_faktur));
                    if($data_pembelian){
                        $t_pembelian = $data_pembelian["total_bayar"];
                        $sisa_pinjaman = $data_pembelian["sisa_pinjaman"];

                        $sisa_pinjaman_fix = $sisa_pinjaman - $total_bayar;

                        $insert = $this->pm->insert_hutang($id_user, $no_faktur, $tgl_bayar, $cara_bayar, $total_bayar, $ket_hutang, $tgl_input, $time_update);
                        if($insert){
                            $set_up_sisa = array("sisa_pinjaman"=>$sisa_pinjaman_fix);
                            $where_up_sisa = array("id_pembelian"=>$no_faktur);
                            if($sisa_pinjaman_fix <= 0 ){
                                $set_up_sisa["sts_pinjaman"] = "1";
                            }else {
                                $set_up_sisa["sts_pinjaman"] = "0";
                            }

                            $update_sisa = $this->mm->update_data("pembelian", $set_up_sisa, $where_up_sisa);
                            if($update_sisa){
                                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                            }
                        }
                    }
                }elseif ($kode_hutang == "PG") {
                    $data_pengeluaran = $this->mm->get_data_each("pengeluaran", array("id_pengeluaran"=>$no_faktur));
                    if($data_pengeluaran){
                        $t_pengeluaran = $data_pengeluaran["total_biaya"];
                        $sisa_pinjaman = $data_pengeluaran["sisa_pinjaman"];

                        $sisa_pinjaman_fix = $sisa_pinjaman - $total_bayar;

                        $insert = $this->pm->insert_hutang($id_user, $no_faktur, $tgl_bayar, $cara_bayar, $total_bayar, $ket_hutang, $tgl_input, $time_update);
                        if($insert){
                            $set_up_sisa = array("sisa_pinjaman"=>$sisa_pinjaman_fix);
                            $where_up_sisa = array("id_pengeluaran"=>$no_faktur);
                            if($sisa_pinjaman_fix <= 0 ){
                                $set_up_sisa["sts_pinjaman"] = "1";
                            }else {
                                $set_up_sisa["sts_pinjaman"] = "0";
                            }

                            $update_sisa = $this->mm->update_data("pengeluaran", $set_up_sisa, $where_up_sisa);
                            if($update_sisa){
                                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                            }
                        }
                    }
                }
            }
        }else{
            $msg_detail["tgl_bayar"]        = strip_tags(form_error('tgl_bayar'));
            $msg_detail["cara_bayar"]       = strip_tags(form_error('cara_bayar'));
            $msg_detail["total_bayar"]      = strip_tags(form_error('total_bayar'));
            $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur'));
            $msg_detail["ket_hutang"]       = strip_tags(form_error('ket_hutang'));

            $msg_detail["token"]            = strip_tags(form_error('token'));       
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_hutang(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tgl_bayar"=>"",
                    "cara_bayar"=>"",
                    "total_bayar"=>"",
                    "no_faktur"=>"",
                    "ket_hutang"=>""
                );

        if($this->val_form_hutang()){
            $tgl_bayar          = $this->input->post("tgl_bayar");
            $cara_bayar         = $this->input->post("cara_bayar");
            $total_bayar        = $this->input->post("total_bayar");
            $no_faktur          = $this->input->post("no_faktur");
            $ket_hutang         = $this->input->post("ket_hutang");
            $token              = $this->input->post("token");

            $id_hutang          = $this->input->post("id_hutang");

            $id_admin           = "0";
            $tgl_input          = date("Y-m-d");
            $time_update        = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $set = array(
                        "tgl_bayar"=>$tgl_bayar,
                        "cara_bayar"=>$cara_bayar,
                        "total_bayar"=>$total_bayar,
                        "ket_hutang"=>$ket_hutang,
                        "time_update"=>$time_update
                    );

                $where = array(
                            "id_hutang"=>$id_hutang
                        );

                $kode_hutang = explode("-", $no_faktur)[1];
                if($kode_hutang == "PB"){
                    $data_pembelian = $this->mm->get_data_each("pembelian", array("id_pembelian"=>$no_faktur));
                    if($data_pembelian){
                        $id_pembelian_before = $data_pembelian["id_pembelian"];
                        $t_pembelian_real    = $data_pembelian["total_bayar"];
                        $sisa_pinjaman_real  = $data_pembelian["sisa_pinjaman"];

                        $data_hutang_before = $this->mm->get_data_each("hutang", $where);
                        $t_bayar_hutang_before = $data_hutang_before["total_bayar"];

                        $t_sisa_pinjaman_before_fix = $sisa_pinjaman_real + $t_bayar_hutang_before;

                        $update_pembelian_before = $this->mm->update_data("pembelian",
                            array("sisa_pinjaman"=> $t_sisa_pinjaman_before_fix), array("id_pembelian"=>$id_pembelian_before));
                        if($update_pembelian_before){
                            $data_pembelian_after = $this->mm->get_data_each("pembelian", array("id_pembelian"=>$no_faktur));

                            $id_pembelian_after   = $data_pembelian_after["id_pembelian"];
                            $t_pembelian_after    = $data_pembelian_after["total_bayar"];
                            $sisa_pinjaman_after  = $data_pembelian_after["sisa_pinjaman"];

                            $t_sisa_pinjaman_after_fix = $sisa_pinjaman_after - $total_bayar;

                            $set_up_pembelian_fater = array("sisa_pinjaman"=> $t_sisa_pinjaman_after_fix);
                            if($t_sisa_pinjaman_after_fix <= 0 ){
                                $set_up_pembelian_fater["sts_pinjaman"] = "1";
                            }else {
                                $set_up_pembelian_fater["sts_pinjaman"] = "0";
                            }

                            $update_pembelian_fater = $this->mm->update_data("pembelian",
                            $set_up_pembelian_fater, array("id_pembelian"=>$id_pembelian_after));

                            if($update_pembelian_fater){
                                $update_hutang = $this->mm->update_data("hutang", $set, $where);
                                if($update_hutang){
                                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                                }
                            }
                        }
                    }
                }elseif ($kode_hutang == "PG") {
                    $data_pengeluaran = $this->mm->get_data_each("pengeluaran", array("id_pengeluaran"=>$no_faktur));
                    if($data_pengeluaran){
                        $id_pengeluaran_before = $data_pengeluaran["id_pengeluaran"];
                        $t_pengeluaran_real    = $data_pengeluaran["total_biaya"];
                        $sisa_pinjaman_real  = $data_pengeluaran["sisa_pinjaman"];

                        $data_hutang_before = $this->mm->get_data_each("hutang", $where);
                        $t_bayar_hutang_before = $data_hutang_before["total_bayar"];

                        $t_sisa_pinjaman_before_fix = $sisa_pinjaman_real + $t_bayar_hutang_before;

                        $set_up_pengeluaran_before = array("sisa_pinjaman"=> $t_sisa_pinjaman_before_fix);
                        $update_pengeluaran_before = $this->mm->update_data("pengeluaran",
                            $set_up_pengeluaran_before, array("id_pengeluaran"=>$id_pengeluaran_before));
                        if($update_pengeluaran_before){
                            $data_pengeluaran_after = $this->mm->get_data_each("pengeluaran", array("id_pengeluaran"=>$no_faktur));

                            $id_pengeluaran_after   = $data_pengeluaran_after["id_pengeluaran"];
                            $t_pengeluaran_after    = $data_pengeluaran_after["total_biaya"];
                            $sisa_pinjaman_after  = $data_pengeluaran_after["sisa_pinjaman"];

                            $t_sisa_pinjaman_after_fix = $sisa_pinjaman_after - $total_bayar;

                            $set_up_pengeluaran_after = array("sisa_pinjaman"=> $t_sisa_pinjaman_after_fix);
                            if($t_sisa_pinjaman_after_fix <= 0){
                                $set_up_pengeluaran_after["sts_pinjaman"] = "1";
                            }else {
                                $set_up_pengeluaran_after["sts_pinjaman"] = "0";
                            }
                            $update_pengeluaran_fater = $this->mm->update_data("pengeluaran",
                            $set_up_pengeluaran_after, array("id_pengeluaran"=>$id_pengeluaran_after));

                            if($update_pengeluaran_fater){
                                $update_hutang = $this->mm->update_data("hutang", $set, $where);
                                if($update_hutang){
                                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                                }
                            }
                        }
                    }
                }
            }
        }else{
            $msg_detail["tgl_bayar"]        = strip_tags(form_error('tgl_bayar'));
            $msg_detail["cara_bayar"]       = strip_tags(form_error('cara_bayar'));
            $msg_detail["total_bayar"]      = strip_tags(form_error('total_bayar'));
            $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur'));
            $msg_detail["ket_hutang"]       = strip_tags(form_error('ket_hutang'));

            $msg_detail["token"]            = strip_tags(form_error('token'));       
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_hutang(){
        $id_hutang   = $this->input->post("id_hutang");
        $data_hutang = $this->mm->get_data_each("hutang", array("id_hutang"=>$id_hutang));
        $data = array();

        $kode_hutang = explode("-", $data_hutang["no_faktur"])[1];
        if($kode_hutang == "PB"){
            $data        = $this->pm->get_hutang_pembelian(array("id_hutang"=>$id_hutang));
        }elseif ($kode_hutang == "PG") {
            $data        = $this->pm->get_hutang_pengeluaran(array("id_hutang"=>$id_hutang));
        }
    
        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }

    public function delete_hutang(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_standart_token()){
            $token = $this->input->post("token");
            $id_hutang = $this->input->post("id_hutang");

            if($token == "FC094X"){
                $data_hutang = $this->mm->get_data_each("hutang", array("id_hutang"=>$id_hutang));
                $kode_hutang = "";
                if($data_hutang){
                    $kode_hutang = explode("-", $data_hutang["no_faktur"])[1];
                    $no_faktur   = $data_hutang["no_faktur"];
                    $total_bayar_before = $data_hutang["total_bayar"];
                }
                

                $sts_delete = false;
                if($kode_hutang == "PB"){
                    $data_pembelian = $this->mm->get_data_each("pembelian", array("id_pembelian"=>$no_faktur));
                    if($data_pembelian){
                        $t_pembelian = $data_pembelian["total_bayar"];
                        $sisa_pinjaman = $data_pembelian["sisa_pinjaman"];

                        $sisa_pinjaman_fix = $sisa_pinjaman + $total_bayar_before;

                        $set_up_sisa = array("sisa_pinjaman"=>$sisa_pinjaman_fix);
                        $where_up_sisa = array("id_pembelian"=>$no_faktur);
                        if($sisa_pinjaman_fix <= 0 ){
                            $set_up_sisa["sts_pinjaman"] = "1";
                        }else {
                            $set_up_sisa["sts_pinjaman"] = "0";
                        }

                        $update_sisa = $this->mm->update_data("pembelian", $set_up_sisa, $where_up_sisa);
                        if($update_sisa){
                            $sts_delete = true;
                        }
                    }
                }elseif ($kode_hutang == "PG") {
                    $data_pengeluaran = $this->mm->get_data_each("pengeluaran", array("id_pengeluaran"=>$no_faktur));
                    if($data_pengeluaran){
                        $t_pengeluaran = $data_pengeluaran["total_biaya"];
                        $sisa_pinjaman = $data_pengeluaran["sisa_pinjaman"];

                        $sisa_pinjaman_fix = $sisa_pinjaman + $total_bayar_before;

                        $set_up_sisa = array("sisa_pinjaman"=>$sisa_pinjaman_fix);
                        $where_up_sisa = array("id_pengeluaran"=>$no_faktur);
                        if($sisa_pinjaman_fix <= 0 ){
                            $set_up_sisa["sts_pinjaman"] = "1";
                        }else {
                            $set_up_sisa["sts_pinjaman"] = "0";
                        }

                        $update_sisa = $this->mm->update_data("pengeluaran", $set_up_sisa, $where_up_sisa);
                        if($update_sisa){
                            $sts_delete = true;
                        }
                    }
                }

                if($sts_delete){
                    $delete = $this->mm->delete_data("hutang", array("id_hutang"=>$id_hutang));
                    if($delete){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                    }
                }
            }
        }else{
            $msg_detail["token"] =strip_tags(form_error('token'));
        }
        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_hutang_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_standart_token()){
            $token = $this->input->post("token");
            $id_user = $this->input->post("id_user");

            if($token == "FC094X"){
                // print_r($_POST);
                $data_hutang_pembelian = $this->pm->get_hutang_pembelian(array("pm.id_user"=>$id_user, "pm.is_delete"=>"0"));
                $data_hutang_pengeluaran = $this->pm->get_hutang_pengeluaran(array("pm.id_user"=>$id_user, "pm.is_delete"=>"0"));
                
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail["item_hutang_pembalian"]    = $data_hutang_pembelian;
                $msg_detail["item_hutang_pengeluaran"]  = $data_hutang_pengeluaran;
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_hutang_pengeluaran_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_standart_token()){
            $token = $this->input->post("token");
            $id_user = $this->input->post("id_user");

            if($token == "FC094X"){
                // print_r($_POST);
                $data_hutang_pembelian = $this->pm->get_pembelian(array("pm.id_user"=>$id_user, "pm.is_delete"=>"0", "pm.jenis_bayar"=>"1", "pm.sts_pinjaman"=>"0"));
                $data_hutang_pengeluaran = $this->pm->get_pengeluaran(array("pm.id_user"=>$id_user, "pm.is_delete"=>"0", "pm.jenis_bayar"=>"1", "pm.sts_pinjaman"=>"0"));
                // print_r($data);
                if($data_hutang_pengeluaran || $data_hutang_pembelian){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["item_hutang_pembelian"]    = $data_hutang_pembelian;
                    $msg_detail["item_hutang_pengeluaran"]  = $data_hutang_pengeluaran;
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------hutang_main-----------------------#
#=============================================================================#


#=============================================================================#
#-------------------------------------------piutang_main----------------------#
#=============================================================================#

    public function val_form_piutang(){
        $config_val_input = array(
                array(
                    'field'=>'tgl_bayar',
                    'label'=>'tgl_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'cara_bayar',
                    'label'=>'cara_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'total_bayar',
                    'label'=>'total_bayar',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'no_faktur',
                    'label'=>'no_faktur',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'ket_piutang',
                    'label'=>'ket_piutang',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )  
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_piutang(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tgl_bayar"=>"",
                    "cara_bayar"=>"",
                    "total_bayar"=>"",
                    "no_faktur"=>"",
                    "ket_piutang"=>"",
                    "token"=>""
                );

        if($this->val_form_piutang()){
            $id_user            = $this->input->post("id_user");
            $tgl_bayar          = $this->input->post("tgl_bayar");
            $cara_bayar         = $this->input->post("cara_bayar");
            $total_bayar        = $this->input->post("total_bayar");
            $no_faktur          = $this->input->post("no_faktur");
            $ket_piutang         = $this->input->post("ket_piutang");
            $token              = $this->input->post("token");

            $id_admin           = "0";
            $tgl_input          = date("Y-m-d");
            $time_update        = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $data_penjualan = $this->mm->get_data_each("penjualan", array("id_penjualan"=>$no_faktur));
                if($data_penjualan){
                    $t_penjualan    = $data_penjualan["total_bayar"];
                    $sisa_pinjaman  = $data_penjualan["sisa_pinjaman"];

                    $sisa_pinjaman_fix = $sisa_pinjaman - $total_bayar;

                    $insert = $this->pm->insert_piutang($id_user, $no_faktur, $tgl_bayar, $cara_bayar, $total_bayar, $ket_piutang, $tgl_input, $time_update);
                    if($insert){
                        $set_up_sisa = array("sisa_pinjaman"=>$sisa_pinjaman_fix);
                        $set_up_sisa["sts_pinjaman"] = "0";
                        if($sisa_pinjaman_fix <= 0){
                            $set_up_sisa["sts_pinjaman"] = "1";
                        }
                        $where_up_sisa = array("id_penjualan"=>$no_faktur);

                        $update_sisa = $this->mm->update_data("penjualan", $set_up_sisa, $where_up_sisa);
                        if($update_sisa){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                        }
                    }else {
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                    }
                }
            }
        }else{
            $msg_detail["tgl_bayar"]        = strip_tags(form_error('tgl_bayar'));
            $msg_detail["cara_bayar"]       = strip_tags(form_error('cara_bayar'));
            $msg_detail["total_bayar"]      = strip_tags(form_error('total_bayar'));
            $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur'));
            $msg_detail["ket_piutang"]       = strip_tags(form_error('ket_piutang'));

            $msg_detail["token"]            = strip_tags(form_error('token'));       
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_piutang(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tgl_bayar"=>"",
                    "cara_bayar"=>"",
                    "total_bayar"=>"",
                    "no_faktur"=>"",
                    "ket_piutang"=>"",
                    "token"=>""
                );

        if($this->val_form_piutang()){
            $tgl_bayar          = $this->input->post("tgl_bayar");
            $cara_bayar         = $this->input->post("cara_bayar");
            $total_bayar        = $this->input->post("total_bayar");
            $no_faktur          = $this->input->post("no_faktur");
            $ket_piutang         = $this->input->post("ket_piutang");
            $token              = $this->input->post("token");

            $id_piutang          = $this->input->post("id_piutang");

            $id_admin           = "0";
            $tgl_input          = date("Y-m-d");
            $time_update        = date("Y-m-d h:i:s");

            if($token == "FC094X"){
                $set = array(
                        "tgl_bayar"=>$tgl_bayar,
                        "cara_bayar"=>$cara_bayar,
                        "total_bayar"=>$total_bayar,
                        "no_faktur"=>$no_faktur,
                        "ket_piutang"=>$ket_piutang,
                        "time_update"=>$time_update
                    );

                $where = array(
                            "id_piutang"=>$id_piutang
                        );

                $data_penjualan = $this->mm->get_data_each("penjualan", array("id_penjualan"=>$no_faktur));
                if($data_penjualan){
                    $data_pitang_before     = $this->mm->get_data_each("piutang", array("id_piutang"=>$id_piutang));
                    $t_bayat_pitang_before  = $data_pitang_before["total_bayar"];    

                    $t_penjualan    = $data_penjualan["total_bayar"];
                    $sisa_pinjaman  = $data_penjualan["sisa_pinjaman"];

                    $sisa_pinjaman_fix = ($sisa_pinjaman + $t_bayat_pitang_before) - $total_bayar;

                    $update = $this->mm->update_data("piutang", $set, $where);
                    if($update){
                        $set_up_sisa = array("sisa_pinjaman"=>$sisa_pinjaman_fix);
                        $set_up_sisa["sts_pinjaman"] = "0";
                        if($sisa_pinjaman_fix <= 0){
                            $set_up_sisa["sts_pinjaman"] = "1";
                        }
                        $where_up_sisa = array("id_penjualan"=>$no_faktur);

                        $update_sisa = $this->mm->update_data("penjualan", $set_up_sisa, $where_up_sisa);
                        if($update_sisa){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                        }
                    }else {
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
                    }
                }
            }
        }else{
            $msg_detail["tgl_bayar"]        = strip_tags(form_error('tgl_bayar'));
            $msg_detail["cara_bayar"]       = strip_tags(form_error('cara_bayar'));
            $msg_detail["total_bayar"]      = strip_tags(form_error('total_bayar'));
            $msg_detail["no_faktur"]        = strip_tags(form_error('no_faktur'));
            $msg_detail["ket_piutang"]       = strip_tags(form_error('ket_piutang'));

            $msg_detail["token"]            = strip_tags(form_error('token'));       
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_piutang(){
        $id_piutang   = $this->input->post("id_piutang");
        $data        = $this->pm->get_piutang(array("id_piutang"=>$id_piutang));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        print_r(json_encode($data_json));
    }

    public function delete_piutang(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_standart_token()){
            $token = $this->input->post("token");
            $id_piutang = $this->input->post("id_piutang");

            if($token == "FC094X"){
                $data_piutang = $this->mm->get_data_each("piutang", array("id_piutang"=>$id_piutang));
                    $no_faktur   = $data_piutang["no_faktur"];
                    $total_bayar_before = $data_piutang["total_bayar"];

                $delete = $this->mm->delete_data("piutang", array("id_piutang"=>$id_piutang));
                if($delete){

                    $data_penjualan = $this->mm->get_data_each("penjualan", array("id_penjualan"=>$no_faktur));
                    $sisa_pinjaman_penjualan = $data_penjualan["sisa_pinjaman"];

                    $sisa_pinjaman_penjualan_fix = $sisa_pinjaman_penjualan + $total_bayar_before;

                    $set_up_sisa = array("sisa_pinjaman"=>$sisa_pinjaman_penjualan_fix);
                    $set_up_sisa["sts_pinjaman"] = "0";
                        if($sisa_pinjaman_penjualan_fix <= 0){
                            $set_up_sisa["sts_pinjaman"] = "1";
                        }
                    $where_up_sisa = array("id_penjualan"=>$no_faktur);

                    $update_data_penjualan = $this->mm->update_data("penjualan", $set_up_sisa, $where_up_sisa);
                    if($update_data_penjualan){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                    }
                }
            }
        }else{
            $msg_detail["token"] =strip_tags(form_error('token'));
        }
        
        $array_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($array_msg));
    }

    public function get_piutang_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_standart_token()){
            $token = $this->input->post("token");
            $id_user = $this->input->post("id_user");

            if($token == "FC094X"){
                // print_r($_POST);
                $data = $this->pm->get_piutang(array("pm.id_user"=>$id_user, "pm.is_delete"=>"0"));
                // print_r($data);
                if($data){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["item"] = $data;
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
        $array_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($array_msg));
    }

    public function get_penjualan_piutang_api(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(
                    "token"=>""
                );
        if($this->val_get_standart_token()){
            $token = $this->input->post("token");
            $id_user = $this->input->post("id_user");

            if($token == "FC094X"){
                // print_r($_POST);
                $data = $this->pm->get_penjualan(array("pm.id_user"=>$id_user,
                                                    "pm.jenis_bayar"=>"1",
                                                    "pm.sts_pinjaman"=>"0", 
                                                    "pm.is_delete"=>"0"));
                // print_r($data);
                if($data){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["item"] = $data;
                }
            }
        }else{
            $msg_detail = array(
                            "token"     =>strip_tags(form_error('token'))
                        );
        }
        
        $array_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($array_msg));
    }
#=============================================================================#
#-------------------------------------------piutang_main----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------get_api_form_date-----------------#
#=============================================================================#
    #-------------------val_from_from_date----------------
        public function val_from_from_date(){
            $config_val_input = array(
               array(
                    'field'=>'id_user',
                    'label'=>'id_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_start',
                    'label'=>'tgl_start',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_finish',
                    'label'=>'tgl_finish',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ) 
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
        }
    #-------------------val_from_from_date----------------
    
    #-------------------penjualan-------------------------
        public function get_penjualan_from_date(){
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                    "id_user"=>"",
                    "tgl_start"=>"",
                    "tgl_finish"=>"",
                    "token"=>""
                );
            if($this->val_from_from_date()){
                $id_user    = $this->input->post("id_user");
                $tgl_start  = $this->input->post("tgl_start");
                $tgl_finish = $this->input->post("tgl_finish");
                $token      = $this->input->post("token");

                if($token == "FC094X"){
                    if(strtotime($tgl_start) < strtotime($tgl_finish)){
                        $where = array("pm.tgl_penjualan>="=>$tgl_start, "pm.tgl_penjualan<="=>$tgl_finish, "pm.id_user"=>$id_user, "pm.is_delete"=>"0");
                        $data_item = $this->pm->get_penjualan($where);
                        if($data_item){
                            $msg_detail["item"] = $data_item;

                            $data_item_sum = $this->pm->get_penjualan_sum($where);
                            $msg_detail["sum"] = $data_item_sum;

                            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                        }
                    }
                }
            }else {
                $msg_detail["id_user"]      = strip_tags(form_error('id_user')); 
                $msg_detail["tgl_start"]    = strip_tags(form_error('tgl_start')); 
                $msg_detail["tgl_finish"]   = strip_tags(form_error('tgl_finish')); 
                $msg_detail["token"]        = strip_tags(form_error('token')); 
            }

            $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
            print_r(json_encode($res_msg));

        }
    #-------------------penjualan-------------------------

    #-------------------pembelian-------------------------
        public function get_pembelian_from_date(){
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                    "id_user"=>"",
                    "tgl_start"=>"",
                    "tgl_finish"=>"",
                    "token"=>""
                );
            if($this->val_from_from_date()){
                $id_user    = $this->input->post("id_user");
                $tgl_start  = $this->input->post("tgl_start");
                $tgl_finish = $this->input->post("tgl_finish");
                $token      = $this->input->post("token");

                if($token == "FC094X"){
                    if(strtotime($tgl_start) < strtotime($tgl_finish)){
                        $where = array("pm.tgl_pembelian>="=>$tgl_start, "pm.tgl_pembelian<="=>$tgl_finish, "pm.id_user"=>$id_user, "pm.is_delete"=>"0");
                        $data_item = $this->pm->get_pembelian($where);
                        if($data_item){
                            $msg_detail["item"] = $data_item;

                            $data_item_sum = $this->pm->get_pembelian_sum($where);
                            $msg_detail["sum"] = $data_item_sum;

                            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                        }
                    }
                }else{

                }
            }else {
                $msg_detail["id_user"]      = strip_tags(form_error('id_user')); 
                $msg_detail["tgl_start"]    = strip_tags(form_error('tgl_start')); 
                $msg_detail["tgl_finish"]   = strip_tags(form_error('tgl_finish')); 
                $msg_detail["token"]        = strip_tags(form_error('token')); 
            }

            $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
            print_r(json_encode($res_msg));
        }
    #-------------------pembelian-------------------------

    #-------------------pengeluaran-------------------------
        public function get_pengeluaran_from_date(){
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                    "id_user"=>"",
                    "tgl_start"=>"",
                    "tgl_finish"=>"",
                    "token"=>""
                );
            if($this->val_from_from_date()){
                $id_user    = $this->input->post("id_user");
                $tgl_start  = $this->input->post("tgl_start");
                $tgl_finish = $this->input->post("tgl_finish");
                $token      = $this->input->post("token");

                if($token == "FC094X"){
                    if(strtotime($tgl_start) < strtotime($tgl_finish)){
                        $where = array("pm.tgl_pengeluaran>="=>$tgl_start, "pm.tgl_pengeluaran<="=>$tgl_finish, "pm.id_user"=>$id_user, "pm.is_delete"=>"0");
                        $data_item = $this->pm->get_pengeluaran($where);
                        if($data_item){
                            $msg_detail["item"] = $data_item;

                            $data_item_sum = $this->pm->get_pengeluaran_sum($where);
                            $msg_detail["sum"] = $data_item_sum;

                            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                        }
                    }
                }else{

                }
            }else {
                $msg_detail["id_user"]      = strip_tags(form_error('id_user')); 
                $msg_detail["tgl_start"]    = strip_tags(form_error('tgl_start')); 
                $msg_detail["tgl_finish"]   = strip_tags(form_error('tgl_finish')); 
                $msg_detail["token"]        = strip_tags(form_error('token')); 
            }

            $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
            print_r(json_encode($res_msg));
        }
    #-------------------pengeluaran-------------------------


    #-------------------all_home_page-------------------------

         public function all_home_page(){
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                    "id_user"=>"",
                    "tgl_start"=>"",
                    "tgl_finish"=>"",
                    "token"=>""
                );
            if($this->val_from_from_date()){
                $id_user    = $this->input->post("id_user");
                $tgl_start  = $this->input->post("tgl_start");
                $tgl_finish = $this->input->post("tgl_finish");
                $token      = $this->input->post("token");

                if($token == "FC094X"){
                    if(strtotime($tgl_start) < strtotime($tgl_finish)){
                        // -------pengeluaran
                        $where_pengeluaran = array("pm.tgl_pengeluaran>="=>$tgl_start, "pm.tgl_pengeluaran<="=>$tgl_finish, "pm.id_user"=>$id_user, "pm.is_delete"=>"0");

                        $data_item_pengeluaran = $this->pm->get_pengeluaran($where_pengeluaran);
                        
                        $msg_detail["item_pengeluaran"] = $data_item_pengeluaran;

                        $data_item_sum_pengeluaran = $this->pm->get_pengeluaran_sum($where_pengeluaran);
                        $msg_detail["sum_pengeluaran"] = $data_item_sum_pengeluaran;

                            
                        // -------pembelian
                        $where_pembelian = array("pm.tgl_pembelian>="=>$tgl_start, "pm.tgl_pembelian<="=>$tgl_finish, "pm.id_user"=>$id_user, "pm.is_delete"=>"0");
                        $data_item_pembelian = $this->pm->get_pembelian($where_pembelian);
                        $msg_detail["item_pembelian"] = $data_item_pembelian;

                        $data_item_sum_pembelian = $this->pm->get_pembelian_sum($where_pembelian);
                        $msg_detail["sum_pembelian"] = $data_item_sum_pembelian;


                        // -------penjualan
                        $where_penjualan = array("pm.tgl_penjualan>="=>$tgl_start, "pm.tgl_penjualan<="=>$tgl_finish, "pm.id_user"=>$id_user, "pm.is_delete"=>"0");
                        $data_item_penjualan = $this->pm->get_penjualan($where_penjualan);

                        $msg_detail["item_penjualan"] = $data_item_penjualan;

                        $data_item_sum_penjualan = $this->pm->get_penjualan_sum($where_penjualan);
                        $msg_detail["sum_penjualan"] = $data_item_sum_penjualan;


                        // -------penjualan_tempo
                        $where_penjualan_tempo = array("pm.tgl_tempo_finish>="=>$tgl_start, "pm.tgl_tempo_finish<="=>$tgl_finish
                            , "sts_pinjaman="=>'0'
                            , "pm.id_user"=>$id_user, "pm.is_delete"=>"0");
                        $data_item_penjualan_tempo = $this->db->query("select * from penjualan where jenis_bayar=\"1\" and tgl_tempo_finish >= '".$tgl_start."' and tgl_tempo_finish <= '".$tgl_finish."' and id_user = '".$id_user."' and is_delete = \"0\" and sts_pinjaman = \"0\"")->result();

                        $msg_detail["item_penjualan_tempo"] = $data_item_penjualan_tempo;

                        $data_item_sum_penjualan_tempo = $this->db->query("select sum(sisa_pinjaman) as t_jatuh_tempo from penjualan where jenis_bayar=\"1\" and tgl_tempo_finish >= '".$tgl_start."' and tgl_tempo_finish <= '".$tgl_finish."' and id_user = '".$id_user."' and is_delete = \"0\" and sts_pinjaman = \"0\"")->result();

                        $msg_detail["sum_penjualan_tempo"] = $data_item_sum_penjualan_tempo;
                        $msg_detail["sum_item_penjualan_tempo"][0]["t_item"] = count($data_item_penjualan_tempo);


                        // -------pembelian_tempo
                        $where_pembelian_tempo = array("pm.tgl_tempo_finish>="=>$tgl_start, "pm.tgl_tempo_finish<="=>$tgl_finish, "jenis_bayar"=> "1", "sts_pinjaman"=> "0", "pm.id_user"=>$id_user, "pm.is_delete"=>"0");

                        $data_item_pembelian_tempo = $this->pm->get_pembelian($where_pembelian_tempo);
                        $msg_detail["item_pembelian_tempo"] = $data_item_pembelian_tempo;

                        $data_item_sum_pembelian_tempo = $this->pm->get_pembelian_sum($where_pembelian_tempo);

                        $msg_detail["sum_pembelian_tempo"] = "0";
                        if($data_item_sum_pembelian_tempo != null){

                        }
                        $msg_detail["sum_pembelian_tempo"] = $data_item_sum_pembelian_tempo;
                        $msg_detail["sum_item_pembelian_tempo"][0]["t_item"] = count($data_item_pembelian_tempo);


                        // -------pengeluaran_tempo
                        $where_pengeluaran_tempo = array("pm.tgl_tempo_finish>="=>$tgl_start, "pm.tgl_tempo_finish<="=>$tgl_finish, "jenis_bayar"=> "1", "sts_pinjaman"=> "0", "pm.id_user"=>$id_user, "pm.is_delete"=>"0");

                        $data_item_pengeluaran_tempo = $this->pm->get_pengeluaran($where_pengeluaran_tempo);
                        $msg_detail["item_pengeluaran_tempo"] = $data_item_pengeluaran_tempo;

                        $data_item_sum_pengeluaran_tempo = $this->pm->get_pengeluaran_sum($where_pengeluaran_tempo);
                        $msg_detail["sum_pengeluaran_tempo"] = "0";
                        if($data_item_sum_pengeluaran_tempo != null){
                            $msg_detail["sum_pengeluaran_tempo"] = $data_item_sum_pengeluaran_tempo;
                        }
                        $msg_detail["sum_item_pengeluaran_tempo"][0]["t_item"] = count($data_item_pengeluaran_tempo);


                        // -------vendor
                        $data_vendor = $this->mm->get_data_all_where("kontak", array("id_user"=>$id_user));
                        $msg_detail["item_vendor"] = $data_vendor;


                        // -------produk
                        $data_produk = $this->mm->get_data_all_where("produk", array("id_user"=>$id_user));
                        $msg_detail["item_produk"] = $data_produk;                       
                           


                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                        
                    }
                }else{

                }
            }else {
                $msg_detail["id_user"]      = strip_tags(form_error('id_user')); 
                $msg_detail["tgl_start"]    = strip_tags(form_error('tgl_start')); 
                $msg_detail["tgl_finish"]   = strip_tags(form_error('tgl_finish')); 
                $msg_detail["token"]        = strip_tags(form_error('token')); 
            }

            $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
            print_r(json_encode($res_msg));
        }

    #-------------------all_home_page-------------------------
#=============================================================================#
#-------------------------------------------get_api_form_date-----------------#
#=============================================================================#

#
}
?>