<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Userlogin extends CI_Controller{

	public function __construct(){
		parent::__construct();	
		$this->load->model('user/user_main', 'um');

        $this->load->library("response_message");
        $this->load->library("encrypt");
        // $this->encrypt->set_cipher(blowfish);
	}
    
    private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'token',
                    'label'=>'token',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
    
    public function auth(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
        $msg_detail = array("email" => "",
                            "password" => "");

        if($this->val_form_log()){
            $email = $this->input->post('email');
    		$password = $this->input->post('password');
            $token = $this->input->post('token');

    		$where = array(
        			'password' => hash("sha256", $password)
    			);

            $where_or = array(
                    'email' => $email,
                    'username' => $email
                );
    		
            $cek = $this->um->user_login($where, $where_or);
            // print_r($cek);
            if($token == "FC094X"){
                if($cek["sts_delete"] == "0"){
                    if($cek["sts_active"] == "1"){
                        $data_session = array(
                            "id_user"       => $cek["id_user"],
                            "username"      => $cek["username"],
                            "email"         => $cek["email"],
                            "nama_user"     => $cek["nama_user"],
                            "nama_com"      => $cek["nama_com"],
                            "alamat_com"    => $cek["alamat_com"],
                            "tlp_com"       => $cek["tlp_com"],
                            "is_log"        => "1"
                        );

                        $msg_detail["data_session"] = $data_session;
                        $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC"));
                        // print_r($data_session);
                    }else{
                        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("USER_UNACTIVE"));
                    }
                }else{
                    $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("USER_DELETE"));
                }
            }else{
                $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("ACCESS_FAIL"));
            }
        }else{
            $msg_detail["email"]    = strip_tags(form_error("email"));
            $msg_detail["password"] = strip_tags(form_error("password"));
            $msg_detail["token"]    = strip_tags(form_error("token"));

            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        }
        
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
}
?>