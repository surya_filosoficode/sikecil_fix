<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>assets/core_img/icon_sikecil_white.png">
    <title>sikecil_admin</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?=base_url()?>admin_template/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="<?=base_url()?>admin_template/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/morrisjs/morris.css" rel="stylesheet">
    <!-- Vector CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="<?=base_url()?>admin_template/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?=base_url()?>admin_template/main/css/colors/purple.css" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/skins/all.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/js/jquery-3.2.1.js"></script>

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<?php
    if(isset($_SESSION["admin_lv_1"])){
        $name = $_SESSION["admin_lv_1"]["nama"];
        $username = $_SESSION["admin_lv_1"]["username"];
        $email = $_SESSION["admin_lv_1"]["email"];
    }
?>
<body class="fix-sidebar fix-header card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img width="40px" height="40px" src="<?=base_url()?>assets/core_img/icon_sikecil_white.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img width="40px" height="40px" src="<?=base_url()?>assets/core_img/icon_sikecil_white.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img width="120px" height="35px" src="<?=base_url()?>assets/core_img/sikecil_font.jpg" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img width="120px" height="35px" src="<?=base_url()?>assets/core_img/sikecil_font.jpg" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php print_r("Admin , ".$username)?></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-text">
                                                <h4><?php print_r($name)?></h4>
                                                <p class="text-muted"><?php print_r($username)?></p>
                                                <p class="text-muted"><?php print_r($email)?></p>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="<?php print_r(base_url());?>back-admin/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile text-->
                    <div class="profile-text"> 
                            <h5><?php print_r($name)?></h5>
                            <h6><?php print_r($email)?></h6>
                            <br>
                            <center><a href="<?= base_url();?>back-admin/logout" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a></center>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">MENU</li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Beranda</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/home">Beranda</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account-network"></i><span class="hide-menu">Admin</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/main">Data Admin</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-human-male-female"></i><span class="hide-menu">Data User</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/user">Data User</a></li>
                                
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-clipboard-text"></i><span class="hide-menu">Data Master</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?=base_url()?>admin/tipe_produk">Data Jenis Produk</a></li>
                                <li><a href="<?=base_url()?>admin/tipe_kontak">Data Jenis Kontak</a></li>
                                <li><a href="<?=base_url()?>admin/tipe_bayar">Data Cara Pembayaran</a></li>
                                <li><a href="<?=base_url()?>admin/tipe_biaya">Data Jenis Biaya</a></li>
                                <li><a href="<?=base_url()?>admin/satuan">Data Jenis Satuan</a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?php
            if(isset($page)){
                switch ($page) {
                    case 'home' : include "admin/super_admin_home.php"; 
                        break;
                #=================================================================================================#
                #-------------------------------------------main_admin--------------------------------------------#
                #=================================================================================================#
                    case 'page_admin'       : include "admin/super_admin_main.php"; 
                        break;

                #=================================================================================================#
                #-------------------------------------------main_user---------------------------------------------#
                #=================================================================================================#
                    case 'page_user'        : include "admin/user_main.php"; 
                        break;

                #=================================================================================================#
                #-------------------------------------------main_tipe---------------------------------------------#
                #=================================================================================================#
                    case 'product_tipe'     : include "admin/product_tipe.php"; 
                        break; 

                    case 'kontak_tipe'     : include "admin/kontak_tipe.php"; 
                        break;  

                    case 'bayar_tipe'     : include "admin/bayar_tipe.php"; 
                        break;  

                    case 'biaya_tipe'     : include "admin/biaya_tipe.php"; 
                        break;  

                    case 'satuan'     : include "admin/satuan.php"; 
                        break;   

                #=================================================================================================#
                #-------------------------------------------main_penilaian----------------------------------------#
                #=================================================================================================#
                    case 'page_penilaian'   : include "page_penilaian/penilaian_dsn_main.php"; 
                        break;

                    case 'page_analisa'     : include "page_penilaian/perhitungan_page.php"; 
                        break;

                    case 'page_hasil'       : include "page_penilaian/hasil_page.php"; 
                        break;
                    
                    default:
                        break;
                }
            }
            ?>
            
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer"> ©2019 Create by folosofi_code & design by themedesigner.in </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?=base_url()?>admin_template/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url()?>admin_template/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url()?>admin_template/main/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url()?>admin_template/main/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url()?>admin_template/main/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?=base_url()?>admin_template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url()?>admin_template/main/js/custom.min.js"></script>

    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <script src="<?=base_url()?>admin_template/assets/plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!--morris JavaScript -->
    <script src="<?=base_url()?>admin_template/assets/plugins/raphael/raphael-min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/morrisjs/morris.min.js"></script>
    <!-- Vector map JavaScript -->
    <script src="<?=base_url()?>admin_template/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?=base_url()?>admin_template/main/js/dashboard2.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- icheck -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/icheck.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/icheck.init.js"></script>

    <!-- This is data table -->
    <script src="<?=base_url()?>admin_template/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?=base_url()?>admin_template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>


    
    
    

</body>

</html>
