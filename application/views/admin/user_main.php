<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data User</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data User</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="15%">Username</th>
                                            <th width="15%">Email</th>
                                            <th width="15%">Nama Company</th>
                                            <th width="15%">Alamat Company</th>
                                            <th width="15%">Telephon Company</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(!empty($user)){
                                                // print_r($user);
                                                $no =   1;
                                                foreach ($user as $r_user => $v_user) {
                                                    $str_active = "<span class=\"label label-warning\">belum diaktifkan</span>";
                                                            if($v_user["user"]->sts_active == "1"){
                                                                $str_active = "<span class=\"label label-info\">aktif</span>";
                                                            }elseif ($v_user["user"]->sts_active == "2") {
                                                                $str_active = "<span class=\"label label-danger\">di blockir</span>";
                                                            }

                                                            $t_pembelian_user   = $v_user["count"]["pembelian"];
                                                            $t_pengeluaran_user = $v_user["count"]["pengeluaran"];
                                                            $t_penjualan_user   = $v_user["count"]["penjualan"]; 
                                                            $t_hutang_user      = $v_user["count"]["hutang"];
                                                            $t_piutang_user     = $v_user["count"]["piutang"];

                                                            $t_produk_user      = $v_user["count"]["produk"];
                                                            $t_kontak_user      = $v_user["count"]["kontak"];

                                                            echo "<tr>
                                                                    <td>".($no++)."</td>
                                                                    <td>".$v_user["user"]->username." - ".$str_active."</td>
                                                                    <td>".$v_user["user"]->email."</td>
                                                                    <td>".$v_user["user"]->nama_com."</td>
                                                                    <td>".$v_user["user"]->alamat_com."</td>
                                                                    <td>".$v_user["user"]->tlp_com."</td>
                                                                    <td>
                                                                        <center>
                                                                            <button class=\"btn btn-success\" id=\"up_user\" onclick=\"active_user('".$this->encrypt->encode($v_user["user"]->id_user)."')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>
                                                                                &nbsp;&nbsp;

                                                                            <button class=\"btn btn-info\" id=\"up_user\" onclick=\"get_detail('".$this->encrypt->encode($v_user["user"]->id_user)."', '".$t_produk_user."','".$t_kontak_user."', '".$t_pembelian_user."', '".$t_pengeluaran_user."', '".$t_penjualan_user."', '".$t_hutang_user."', '".$t_piutang_user."')\" style=\"width: 40px;\"><i class=\"fa fa-list\" ></i></button>
                                                                                &nbsp;&nbsp;
                                                                            <button class=\"btn btn-primary\" id=\"up_user\" onclick=\"dasabled_user('".$this->encrypt->encode($v_user["user"]->id_user)."')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>
                                                                                &nbsp;&nbsp;
                                                                            <button class=\"btn btn-danger\" id=\"del_user\" onclick=\"delete_user('".$this->encrypt->encode($v_user["user"]->id_user)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                        </center>
                                                                    </td>
                                                                </tr>";
                                                            }
                                                        }
                                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>
                        <a class="btn btn-success" style="width: 40px;"><i class="fa fa-check" style="color: white;"></i></a>
                        <label class="form-label text-success">Aktifkan User</label>,&nbsp;
                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-list" style="color: white;"></i></a>
                        <label class="form-label text-info">Detail User</label>,&nbsp;
                        <a class="btn btn-primary" style="width: 40px;"><i class="fa fa fa-window-close" style="color: white;"></i></a>
                        <label class="form-label text-primary">Non Aktifkan User</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete User</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

                                <div class="modal fade bs-example-modal-lg" id="modal_detail_user" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Detail Data User</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/produk_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Produk</b></center></div>
                                                                <div class="col-lg-12" id="count_produk"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/kontak_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Kontak</b></center></div>
                                                                <div class="col-lg-12" id="count_kontak"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/hutang_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Hutang</b></center></div>
                                                                <div class="col-lg-12" id="count_hutang"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/piutang_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Piutang</b></center></div>
                                                                <div class="col-lg-12" id="count_piutang"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br><br>
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/sales_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Penjualan</b></center></div>
                                                                <div class="col-lg-12" id="count_penjualan"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/pembelian_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Pembelian</b></center></div>
                                                                <div class="col-lg-12" id="count_pembelian"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/pengeluaran_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Pengeluaran</b></center></div>
                                                                <div class="col-lg-12" id="count_pengeluaran"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->





<script type="text/javascript">
    
    //=========================================================================//
    //-----------------------------------user_delete--------------------------//
    //=========================================================================//
        function delete_user(id_user) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Hapus",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_user', id_user);

                            $.ajax({
                                url: "<?php echo base_url()."admin/adminmain/delete_user/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    console.log(res);
                                    response_delete(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data user berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: true
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/user";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data user gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");
            }
        }
    //=========================================================================//
    //-----------------------------------user_update--------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------dasabled_user--------------------------//
    //=========================================================================//
        function dasabled_user(id_user) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelem di nonaktifkan, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di nonaktifkan",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Blockir",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_user', id_user);

                            $.ajax({
                                url: "<?php echo base_url()."admin/adminmain/unactivate_user/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    console.log(res);
                                    response_disabled(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_disabled(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data user berhasil nonaktifkan ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/user";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data user gagal nonaktifkan, coba periksa jaringan dan koneksi anda", "warning");
            }
        }   
    //=========================================================================//
    //-----------------------------------dasabled_user--------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------activate_user--------------------------//
    //=========================================================================//

        function active_user(id_user) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelum di aktifkan, jika anda sudah yakin maka user berhak untuk menggunakan fitur aplikasi ini..",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Aktifkan",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_user', id_user);

                            $.ajax({
                                url: "<?php echo base_url()."admin/adminmain/activate_user/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    console.log(res);
                                    response_active_user(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_active_user(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data user berhasil nonaktifkan ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/user";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data user gagal aktifkan, coba periksa jaringan dan koneksi anda", "warning");
            }
        }

    //=========================================================================//
    //-----------------------------------activate_user--------------------------//
    //=========================================================================//

    
    function get_detail(id, t_produk_user, t_kontak_user, t_pembelian_user, t_pengeluaran_user, t_penjualan_user, t_hutang_user, t_piutang_user){
        $("#modal_detail_user").modal("show");
        console.log(id);

        $("#count_produk").html("<center>"+t_produk_user + " Data"+"</center>");
        $("#count_kontak").html("<center>"+t_kontak_user+ " Data"+"</center>");
        $("#count_pembelian").html("<center>"+t_pembelian_user+ " Data"+"</center>");
        $("#count_penjualan").html("<center>"+t_penjualan_user+ " Data"+"</center>");
        $("#count_pengeluaran").html("<center>"+t_pengeluaran_user+ " Data"+"</center>");
        $("#count_hutang").html("<center>"+t_hutang_user+ " Data"+"</center>");
        $("#count_piutang").html("<center>"+t_piutang_user+ " Data"+"</center>");
    }
</script>