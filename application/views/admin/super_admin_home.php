<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Beranda Admin Super</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <?php

    $t_user = 0;
    $t_pembelian = 0;
    $t_pengeluaran = 0;
    $t_penjualan = 0; 
    $t_hutang = 0;
    $t_piutang = 0;    

    if(isset($data_count_user)){
        $t_user = $data_count_user;
    }

    if(isset($data_count_pembelian)){
        $t_pembelian = $data_count_pembelian;
    }

    if(isset($data_count_pengeluaran)){
        $t_pengeluaran = $data_count_pengeluaran;
    }

    if(isset($data_count_penjualan)){
        $t_penjualan = $data_count_penjualan;
    }

    if(isset($data_count_hutang)){
        $t_hutang = $data_count_hutang;
    }

    if(isset($data_count_piutang)){
        $t_piutang = $data_count_piutang;
    }

    ?>
    <div class="row">
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-8">
                            <h2><?=$t_user?> record</h2>
                            <h6>Data User</h6></div>
                        <div class="col-4 align-self-center text-right  p-l-0">
                            <div id="sparklinedash3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-8">
                            <h2 class=""><?=$t_penjualan?> record</h2>
                            <h6>Data Penjualan</h6></div>
                        <div class="col-4 align-self-center text-right p-l-0">
                            <div id="sparklinedash"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-8">
                            <h2><?=($t_pengeluaran+$t_pembelian)?> record</h2>
                            <h6>Data Pengeluaran</h6></div>
                        <div class="col-4 align-self-center text-right p-l-0">
                            <div id="sparklinedash2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-8">
                            <h2><?=($t_hutang+$t_piutang)?> record</h2>
                            <h6>Data Hutang dan Piutang</h6></div>
                        <div class="col-4 align-self-center text-right p-l-0">
                            <div id="sparklinedash4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                        <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Product Overview</h4>
                </div>
                <div class="card-body collapse show">
                    <div class="table-responsive">
                        <table class="table product-overview">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="20%">Username</th>
                                    <th width="15%">Email</th>
                                    <th width="15%">Nama Company</th>
                                    <th width="15%">Alamat Company</th>
                                    <th width="15%">Telephon Company</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                                    if(!empty($data_all)){
                                                        $no = 1;
                                                        foreach ($data_all as $r_user => $v_user) {
                                                            $str_active = "<span class=\"label label-warning\">belum diaktifkan</span>";
                                                            if($v_user["user"]->sts_active == "1"){
                                                                $str_active = "<span class=\"label label-info\">aktif</span>";
                                                            }elseif ($v_user["user"]->sts_active == "2") {
                                                                $str_active = "<span class=\"label label-danger\">di blockir</span>";
                                                            }

                                                            $t_pembelian_user   = $v_user["count"]["pembelian"];
                                                            $t_pengeluaran_user = $v_user["count"]["pengeluaran"];
                                                            $t_penjualan_user   = $v_user["count"]["penjualan"]; 
                                                            $t_hutang_user      = $v_user["count"]["hutang"];
                                                            $t_piutang_user     = $v_user["count"]["piutang"];

                                                            $t_produk_user      = $v_user["count"]["produk"];
                                                            $t_kontak_user      = $v_user["count"]["kontak"];

                                                            echo "<tr>
                                                                <td>".($no++)."</td>
                                                                <td>".$v_user["user"]->username." - ".$str_active."</td>
                                                                <td>".$v_user["user"]->email."</td>
                                                                <td>".$v_user["user"]->nama_com."</td>
                                                                <td>".$v_user["user"]->alamat_com."</td>
                                                                <td>".$v_user["user"]->tlp_com."</td>
                                                                <td>
                                                                    <center>
                                                                    <button class=\"btn btn-info\" id=\"up_user\" onclick=\"get_detail('".$this->encrypt->encode($v_user["user"]->id_user)."', '".$t_produk_user."','".$t_kontak_user."', '".$t_pembelian_user."', '".$t_pengeluaran_user."', '".$t_penjualan_user."', '".$t_hutang_user."', '".$t_piutang_user."')\" style=\"width: 40px;\"><i class=\"fa fa-list\" ></i></button>
                                                                    </center>
                                                                </td>
                                                                </tr>";
                                                            }
                                                        }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->

<!-- sample modal content -->
                                <div class="modal fade bs-example-modal-lg" id="modal_detail_user" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myLargeModalLabel">Detail Data User</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/produk_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Produk</b></center></div>
                                                                <div class="col-lg-12" id="count_produk"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/kontak_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Kontak</b></center></div>
                                                                <div class="col-lg-12" id="count_kontak"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/hutang_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Hutang</b></center></div>
                                                                <div class="col-lg-12" id="count_hutang"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/piutang_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Piutang</b></center></div>
                                                                <div class="col-lg-12" id="count_piutang"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <br><br>
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/sales_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Penjualan</b></center></div>
                                                                <div class="col-lg-12" id="count_penjualan"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/pembelian_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Pembelian</b></center></div>
                                                                <div class="col-lg-12" id="count_pembelian"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="row">
                                                                <div class="col-lg-12"><center><img width="60px" height="60px" src="<?=base_url()?>assets/core_img/pengeluaran_blue.png"></center></div>
                                                                <div class="col-lg-12"><center><b>Data Pengeluaran</b></center></div>
                                                                <div class="col-lg-12" id="count_pengeluaran"><center><b>10 record</b></center></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->


    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

<script type="text/javascript">
    $(document).ready(function(){
        console.log("ok");
    });

    function get_detail(id, t_produk_user, t_kontak_user, t_pembelian_user, t_pengeluaran_user, t_penjualan_user, t_hutang_user, t_piutang_user){
        $("#modal_detail_user").modal("show");
        console.log(id);

        $("#count_produk").html("<center>"+t_produk_user + " Data"+"</center>");
        $("#count_kontak").html("<center>"+t_kontak_user+ " Data"+"</center>");
        $("#count_pembelian").html("<center>"+t_pembelian_user+ " Data"+"</center>");
        $("#count_penjualan").html("<center>"+t_penjualan_user+ " Data"+"</center>");
        $("#count_pengeluaran").html("<center>"+t_pengeluaran_user+ " Data"+"</center>");
        $("#count_hutang").html("<center>"+t_hutang_user+ " Data"+"</center>");
        $("#count_piutang").html("<center>"+t_piutang_user+ " Data"+"</center>");
    }
</script>