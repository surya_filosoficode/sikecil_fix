<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Admin</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data Admin</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn btn-rounded btn-primary" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data Admin</button>

                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="20%">Email</th>
                                            <th width="15%">User Name</th>
                                            <th width="20%">Nama</th>
                                            <th width="15%">Password</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(!empty($admin)){
                                                foreach ($admin as $r_admin => $v_admin) {
                                                    $str_active = "<span class=\"label label-warning\">belum diaktifkan</span>";
                                                        if($v_admin->status_active == "1"){
                                                            $str_active = "<span class=\"label label-info\">aktif</span>";
                                                        }elseif ($v_admin->status_active == "2") {
                                                            $str_active = "<span class=\"label label-danger\">di blockir</span>";
                                                        }
                                                    echo "<tr>
                                                        <td>".($r_admin+1)."</td>
                                                        <td>".$v_admin->email."</td>
                                                        <td>".$v_admin->username."</td>
                                                        <td>".$v_admin->nama." ".$str_active."</td>
                                                        <td><button class=\"btn btn-primary\" id=\"up_pass\" onclick=\"change_pass('".$this->encrypt->encode($v_admin->id_admin)."')\">Ubah Password</button></td>
                                                        
                                                        <td>
                                                            <center>
                                                            <button class=\"btn btn-success\" id=\"ac_admin\" onclick=\"active_admin('".$this->encrypt->encode($v_admin->id_admin)."')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-primary\" id=\"un_ac_admin\" onclick=\"dasabled_admin('".$this->encrypt->encode($v_admin->id_admin)."')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-info\" id=\"up_admin\" onclick=\"update_admin('".$this->encrypt->encode($v_admin->id_admin)."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-danger\" id=\"del_admin\" onclick=\"delete_admin('".$this->encrypt->encode($v_admin->id_admin)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                            </center>
                                                        </td>
                                                        </tr>";
                                                            }
                                                        }
                                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" id="change_pass_modal" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Ubah Password Admin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password :</label>
                                <input type="Password" class="form-control" id="_pass" name="pass" required="">
                                <p id="_msg_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password :</label>
                                <input type="Password" class="form-control" id="_repass" name="repass" required="">
                                <p id="_msg_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="add_pass_new" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_admin";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Admin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Username :</label>
                                <input type="text" class="form-control" id="username" name="username" required="">
                                <p id="msg_username" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama :</label>
                                <input type="text" class="form-control" id="nama" name="nama" required="">
                                <p id="msg_nama" style="color: red;"></p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email :</label>
                                <input type="text" class="form-control" id="email" name="email" required="">
                                <p id="msg_email" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password :</label>
                                <input type="Password" class="form-control" id="pass" name="pass" required="">
                                <p id="msg_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password :</label>
                                <input type="Password" class="form-control" id="repass" name="repass" required="">
                                <p id="msg_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_admin" class="btn btn-primary">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Admin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_admin";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Username :</label>
                                <input type="text" class="form-control" id="_username" name="username" required="" readonly="">
                                <p id="_msg_username" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama :</label>
                                <input type="text" class="form-control" id="_nama" name="nama" required="">
                                <p id="_msg_nama" style="color: red;"></p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email :</label>
                                <input type="text" class="form-control" id="_email" name="email" required="">
                                <p id="_msg_email" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_admin" class="btn btn-primary">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_admin_glob = "";
    var key_ex, id_admin_op;

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_admin").click(function() {
            var data_main = new FormData();
            data_main.append('nama', $("#nama").val());
            data_main.append('email', $("#email").val());
            data_main.append('username', $("#username").val());
            data_main.append('pass', $("#pass").val());
            data_main.append('repass', $("#repass").val());

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/insert_admin";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_insert(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data admin berhasil disimpan ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/main";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#msg_email").html(detail_msg.email);
                            $("#msg_nama").html(detail_msg.nama);
                            $("#msg_username").html(detail_msg.username);
                            $("#msg_pass").html(detail_msg.pass);
                            $("#msg_repass").html(detail_msg.repass);

                            swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function update_admin(id_admin) {
            var id_admin_chahce = "";
            clear_from_update();

            var data_main = new FormData();
            data_main.append('id_admin', id_admin);

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/get_admin_update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    set_val_update(res, id_admin);
                    $("#update_admin").modal('show');
                }
            });
        }

        function set_val_update(res, id_admin) {
            var res_pemohon = JSON.parse(res);
            console.log(res_pemohon);
            if (res_pemohon.status == true) {
                id_admin_chahce = res_pemohon.val_response.id_admin;
                $("#_email").val(res_pemohon.val_response.email);
                $("#_username").val(res_pemohon.val_response.username);
                $("#_nama").val(res_pemohon.val_response.nama);
               
                id_admin_glob = id_admin;
            } else {
                clear_from_update();
            }
        }

        function clear_from_update() {
            $("#_email").val("");
            $("#_username").val("");
            $("#_nama").val("");

            id_admin_glob = "";
            $("#update_admin").modal('hide');
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#btn_update_admin").click(function() {
            var data_main = new FormData();
            data_main.append('id_admin', id_admin_glob);
            data_main.append('email', $("#_email").val());
            data_main.append('username', $("#_username").val());
            data_main.append('nama', $("#_nama").val());

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/update_admin";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data admin berhasil diubah ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."super/admin";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#_msg_email").html(detail_msg.email);
                            $("#_msg_nama").html(detail_msg.nama);
                            $("#_msg_username").html(detail_msg.username);
                            

                            swal("Proses Gagal.!!", "Data admin gagal diubah, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//
        function delete_admin(id_admin) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Hapus",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_admin', id_admin);

                            $.ajax({
                                url: "<?php echo base_url()."admin/adminmain/delete_admin/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    // console.log(id_admin);
                                    response_delete(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data admin berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/main";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data admin gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//
        $("#add_pass_new").click(function() {
            var data_main = new FormData();
            data_main.append('id_admin', id_admin_op);
            data_main.append('key', key_ex);

            data_main.append('pass', $("#_pass").val());
            data_main.append('repass', $("#_repass").val());

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/change_pass";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_pass_new(res);
                    // response_change_pass(res, id_admin_op);
                }
            });
        });

        function response_pass_new(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Password admin berhasil diubah ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/main";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message                        
                            swal("Proses Gagal.!!", "Password admin gagal diubah, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }

        function change_pass(id_admin) {
            var data_main = new FormData();
            data_main.append('id_admin', id_admin);

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/get_password";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_change_pass(res, id_admin);
                }
            });
        }

        function response_change_pass(res, id_admin) {
            var data_json = JSON.parse(res);
            if (data_json.status) {
                key_ex = data_json.val_key;
                id_admin_op = id_admin;

                console.log(key_ex.length);
                $("#change_pass_modal").modal("show");
            }
        }
    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//

     //=========================================================================//
    //-----------------------------------dasabled_admin--------------------------//
    //=========================================================================//
        function dasabled_admin(id_admin) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelem di nonaktifkan, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di nonaktifkan",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Blockir",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_admin', id_admin);

                            $.ajax({
                                url: "<?php echo base_url()."admin/adminmain/unactivate_admin/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    console.log(res);
                                    response_disabled(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_disabled(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data admin berhasil nonaktifkan ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/main";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data admin gagal nonaktifkan, coba periksa jaringan dan koneksi anda", "warning");
            }
        }   
    //=========================================================================//
    //-----------------------------------dasabled_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------activate_admin--------------------------//
    //=========================================================================//

        function active_admin(id_admin) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelum di aktifkan, jika anda sudah yakin maka admin berhak untuk menggunakan fitur aplikasi ini..",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Aktifkan",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_admin', id_admin);

                            $.ajax({
                                url: "<?php echo base_url()."admin/adminmain/activate_admin/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    console.log(res);
                                    response_active_admin(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_active_admin(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data admin berhasil nonaktifkan ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/main";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data admin gagal aktifkan, coba periksa jaringan dan koneksi anda", "warning");
            }
        }

    //=========================================================================//
    //-----------------------------------activate_admin--------------------------//
    //=========================================================================//
</script>