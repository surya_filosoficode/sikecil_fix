<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Tipe Bayar</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data Tipe Bayar</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn btn-rounded btn-primary" data-toggle="modal" data-target="#insert_data"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data Tipe Bayar</button>

                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="*">Tipe Bayar</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(!empty($list_bayar_tipe)){
                                                foreach ($list_bayar_tipe as $r_tipe => $v_tipe) {
                                                    echo "<tr>
                                                        <td>".($r_tipe+1)."</td>
                                                        <td>".$v_tipe->cara_bayar."</td>
                                                        
                                                        <td>
                                                            <center>
                                                            <button class=\"btn btn-info\" id=\"up_tipe\" onclick=\"update_data('".$this->encrypt->encode($v_tipe->id_bayar)."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>
                                                                &nbsp;&nbsp;
                                                            <button class=\"btn btn-danger\" id=\"del_tipe\" onclick=\"delete_data('".$this->encrypt->encode($v_tipe->id_bayar)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                            </center>
                                                        </td>
                                                        </tr>";
                                                            }
                                                        }
                                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->



<!-- ============================================================== -->
<!-- --------------------------insert_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Tipe Bayar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Keterangan Tipe Bayar :</label>
                                <input type="text" class="form-control" id="cara_bayar" name="cara_bayar" required="">
                                <p id="msg_cara_bayar" style="color: red;"></p>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_data" class="btn btn-primary">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_data------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Tipe Bayar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Keterangan Tipe Bayar :</label>
                                <input type="text" class="form-control" id="_cara_bayar" name="cara_bayar" required="">
                                <p id="_msg_cara_bayar" style="color: red;"></p>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_data" class="btn btn-primary">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_bayar_glob = "";
    var key_ex, id_bayar_op;

    //=========================================================================//
    //-----------------------------------insert_data--------------------------//
    //=========================================================================//
        $("#add_data").click(function() {
            var data_main = new FormData();
            data_main.append('cara_bayar', $("#cara_bayar").val());
            

            $.ajax({
                url: "<?php echo base_url()."admin/Mainmaster/insert_tipe_bayar";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_insert(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Tipe Bayar berhasil disimpan ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/tipe_bayar";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#msg_cara_bayar").html(detail_msg.cara_bayar);
                            

                            swal("Proses Gagal.!!", "Data Tipe Bayar gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //=========================================================================//
    //-----------------------------------insert_data--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_tipe_update----------------------//
    //=========================================================================//
        function update_data(id_bayar) {
            var id_bayar_chahce = "";
            clear_from_update();

            var data_main = new FormData();
            data_main.append('id_bayar', id_bayar);

            $.ajax({
                url: "<?php echo base_url()."admin/Mainmaster/get_tipe_update_bayar";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    set_val_update(res, id_bayar);
                    $("#update_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_bayar) {
            var res_pemohon = JSON.parse(res);
            console.log(res_pemohon);
            if (res_pemohon.status == true) {
                id_bayar_chahce = res_pemohon.val_response.id_bayar;
                $("#_cara_bayar").val(res_pemohon.val_response.cara_bayar);
                
               
                id_bayar_glob = id_bayar_chahce;
            } else {
                clear_from_update();
            }
        }

        function clear_from_update() {
            $("#_cara_bayar").val("");
            
            id_bayar_glob = "";
            $("#update_data").modal('hide');
        }
    //=========================================================================//
    //-----------------------------------get_tipe_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_bayar', id_bayar_glob);
            data_main.append('cara_bayar', $("#_cara_bayar").val());
            

            $.ajax({
                url: "<?php echo base_url()."admin/Mainmaster/update_tipe_bayar";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Tipe Bayar berhasil diubah ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/tipe_bayar";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#_msg_cara_bayar").html(detail_msg.cara_bayar);
                            
                            swal("Proses Gagal.!!", "Data Tipe Bayar gagal diubah, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }
    //=========================================================================//
    //-----------------------------------update_data--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//
        function delete_data(id_bayar) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Hapus",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_bayar', id_bayar);

                            $.ajax({
                                url: "<?php echo base_url()."admin/Mainmaster/delete_tipe_bayar/";?>",
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    console.log(res);
                                    response_delete(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");

                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data Tipe Bayar berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/tipe_bayar";?>";
                });

            } else {
                // console.log("false");
                swal("Proses Gagal.!!", "Data Tipe Bayar gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

</script>