<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Halaman Admin</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="card-title">Daftar Admin Pengelola</h4>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" class="btn btn-success btn-rounded" data-toggle="modal" data-target="#data_lv_admin">
                                <i class="fa fa-plus-circle"></i> Tambah Lv. Admin
                            </button>
                            &nbsp;&nbsp;
                            <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_admin">
                                <i class="fa fa-plus-circle"></i> Tambah Admin
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive m-t-100">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="15%">User Name</th>
                                    <th width="15%">Nama</th>
                                    <th width="15%">Level Admin</th>
                                    <th width="10%">Password</th>
                                    <th width="15%">Jabatan</th>
                                    <th width="15%">Bidang</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(!empty($admin)){
                                        foreach ($admin as $r_admin => $v_admin) {
                                            echo "<tr>
                                                    <td>".($r_admin+1)."</td>
                                                    <td>".$v_admin->email."</td>
                                                    <td>".$v_admin->nama."</td>
                                                    <td>".$v_admin->ket."</td>
                                                    <td><button class=\"btn btn-primary\" id=\"up_pass\" onclick=\"change_pass('".$this->encrypt->encode($v_admin->id_admin)."')\">Ubah Password</button></td>
                                                    <td>".$v_admin->nama_dinas."</td>
                                                    <td>".$v_admin->jabatan."</td>
                                                    <td>
                                                        <button class=\"btn btn-success\" id=\"up_user\" onclick=\"active_admin('".$this->encrypt->encode($v_admin->id_admin)."')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;
                                                        <button type=\"button\" class=\"btn btn-info\" id=\"up_dinas\" onclick=\"func_update_admin('".$this->encrypt->encode($v_admin->id_admin)."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\"></i></button>&nbsp;&nbsp;
                                                        <button class=\"btn btn-danger\" id=\"del_dinas\" onclick=\"delete_admin('".$this->encrypt->encode($v_admin->id_admin)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                    </td>
                                                </tr>";
                                        }
                                    }
                                ?>
                            </tbody>                  
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ============================================================== -->
<!-- --------------------------Modal insert Lv. Admin-------------- -->
<!-- ============================================================== -->
    <div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" id="data_lv_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Data Level Admin</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div id="slimtest2">
                    <div class="table">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="75%">Keterangan Admin</th>
                                    <th width="20%">Action</th>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>
                                        <input type="input" class="form-control" name="ket_lv" id="ket_lv" placeholder="Keterangan">
                                        <p style="color: red;"></p>
                                    </td>
                                    <td>
                                        <center>
                                        <button name="add_lv" id="add_lv" class="btn btn-info"><i class="fa fa-plus"></i></button>
                                        </center>
                                    </td>
                                </tr>
                            </thead>
                            <tbody id="out_act_lv">
                                <?php include "admin_main_lv.php";?>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ============================================================== -->
<!-- --------------------------Modal insert Lv. Admin-------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->
    <div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" id="change_pass_modal" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Ubah Password Admin</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Password :</label>
                                <input type="Password" class="form-control" id="pass_new" name="pass_new" required="">
                                <p id="msg_pass_new" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password :</label>
                                <input type="Password" class="form-control" id="repass_new" name="repass_new" required="">
                                <p id="msg_repass_new" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="add_pass_new" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>
<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"  id="insert_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_admin";?>"> -->
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Admin</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                                            
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Email :</label>
                                    <input type="text" class="form-control" id="email" name="email" required="">
                                    <p id="msg_email" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nama :</label>
                                    <input type="text" class="form-control" id="nama" name="nama" required="">
                                    <p id="msg_nama" style="color: red;"></p>
                                </div>
                            </div>
                            
                        </div>    
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">NIP :</label>
                                    <input type="number" class="form-control" id="nip" name="nip" required="">
                                    <p id="msg_nip" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Jabatan :</label>
                                    <input type="text" class="form-control" id="jbtn" name="jbtn" required="">
                                    <p id="msg_jbtn" style="color: red;"></p>
                                </div>
                            </div>
                            
                        </div>    
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Bidang/Dinas :</label>
                                    <select class="form-control" id="dinas" name="dinas">
                                    <?php
                                        if(!empty($dinas)){
                                            foreach ($dinas as $r_dinas => $v_dinas) {
                                                echo "<option value=\"".$v_dinas->id_dinas."\">".$v_dinas->nama_dinas."</option>";
                                            }
                                        }
                                    ?>

                                    </select>
                                    <p id="msg_dinas" style="color: red;"></p>
                                </div>
                            </div>
                        </div>
                    </div>                      
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Lv. Admin :</label>
                                    <select class="form-control" id="lv" name="lv">
                                        <?php
                                            if(!empty($lv)){
                                                foreach ($lv as $r_lv => $v_lv) {
                                                    echo "<option value=\"".$v_lv->id_lv."\">".$v_lv->ket."</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                    <p id="msg_lv" style="color: red;"></p>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Password :</label>
                                    <input type="Password" class="form-control" id="pass" name="pass" required="">
                                    <p id="msg_pass" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Ulangi Password :</label>
                                    <input type="Password" class="form-control" id="repass" name="repass" required="">
                                    <p id="msg_repass" style="color: red;"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="add_admin" class="btn btn-primary">Simpan</button>
                </div>
                <!-- </form> -->
            </div>
        </div>
    </div>
<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"  id="update_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Admin</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <!-- <form action="<?= base_url()."admin_super/superadmin/update_admin";?>" method="post"> -->
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Email :</label>
                                    <input type="text" class="form-control" id="_email" name="email" required="">
                                    <p id="_msg_email" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nama :</label>
                                    <input type="text" class="form-control" id="_nama" name="nama" required="">
                                    <p id="_msg_nama" style="color: red;"></p>
                                </div>
                            </div>
                        </div>    
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">NIP :</label>
                                    <input type="number" class="form-control" id="_nip" name="nip" required="">
                                    <p id="_msg_nip" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Jabatan :</label>
                                    <input type="text" class="form-control" id="_jbtn" name="jbtn" required="">
                                    <p id="_msg_jbtn" style="color: red;"></p>
                                </div>
                            </div>
                            
                        </div>    
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Bidang/Dinas :</label>
                                    <select class="form-control" id="_dinas" name="dinas">
                                    <?php
                                        if(!empty($dinas)){
                                            foreach ($dinas as $r_dinas => $v_dinas) {
                                                echo "<option value=\"".$v_dinas->id_dinas."\">".$v_dinas->nama_dinas."</option>";
                                            }
                                        }
                                    ?>
                                    </select>
                                    <p id="_msg_dinas" style="color: red;"></p>
                                </div>
                            </div>
                        </div>
                    </div>     
                    
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Lv. Admin :</label>
                                    <select class="form-control" id="_lv" name="lv">
                                    <?php
                                        if(!empty($lv)){
                                            foreach ($lv as $r_lv => $v_lv) {
                                                echo "<option value=\"".$v_lv->id_lv."\">".$v_lv->ket."</option>";
                                            }
                                        }
                                    ?>
                                    </select>
                                    <p id="_msg_lv" style="color: red;"></p>
                                </div>
                            </div>
                            
                        </div>    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn_update_admin" class="btn btn-primary">Ubah Data</button>
                </div>
                <!-- </form> -->
            </div>
        </div>
    </div>
<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    //========================================================================
    //--------------------------------Get_Lokasi------------------------------
    //========================================================================
        $(document).ready(function(){
            if(navigator.geolocation){
                navigator.geolocation.getCurrentPosition(showLocation);
            }else{ 
                console.log('Geolocation is not supported by this browser.');
            }
        });

        function showLocation(position){
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            // console.log(position);
        }
    //========================================================================
    //--------------------------------Get_Lokasi------------------------------
    //========================================================================

    var id_admin_glob = "";
    var key_ex, id_admin_op;
    
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_admin").click(function(){
            var data_main =  new FormData();
            data_main.append('nama' , $("#nama").val());
            data_main.append('email', $("#email").val());
            data_main.append('nip'  , $("#nip").val());
            data_main.append('jbtn' , $("#jbtn").val());
            data_main.append('dinas', $("#dinas").val());
            data_main.append('lv'   , $("#lv").val());
            data_main.append('pass' , $("#pass").val());
            data_main.append('repass', $("#repass").val());
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/add/admin_main";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    response_insert(res);
                }
            });
        });

        function response_insert(res){
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data admin berhasil disimpan ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."admin/super/add_admin";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_email").html(detail_msg.email);
                        $("#msg_nama").html(detail_msg.nama);
                        $("#msg_nip").html(detail_msg.nip);
                        $("#msg_jbtn").html(detail_msg.jabatan);
                        $("#msg_lv").html(detail_msg.lv);
                        $("#msg_id_bidang").html(detail_msg.id_bidang);
                        $("#msg_pass").html(detail_msg.pass);
                        $("#msg_repass").html(detail_msg.repass);

                        swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

                
            }
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function func_update_admin(id_admin){
            var id_admin_chahce = "";
            clear_from_update();

            var data_main =  new FormData();
            data_main.append('id_admin', id_admin);
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/get/admin_main";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    set_val_update(res, id_admin);
                    $("#update_admin").modal('show');
                }
            });
        }

        function set_val_update(res, id_admin){
            var res_pemohon = JSON.parse(res.toString());
            // console.log(res_pemohon.val_response.id_dinas);
            if(res_pemohon.status == true){
                id_admin_chahce = res_pemohon.val_response.id_admin;
                $("#_email").val(res_pemohon.val_response.email);
                $("#_nama").val(res_pemohon.val_response.nama);
                $("#_nip").val(res_pemohon.val_response.nip);
                $("#_jbtn").val(res_pemohon.val_response.jabatan);
                $("#_lv").val(res_pemohon.val_response.id_lv);
                $("#_dinas").val(res_pemohon.val_response.id_dinas);

                id_admin_glob = id_admin;
            }else {
                clear_from_update();
            }
        }

        function clear_from_update(){
            $("#_email").val("");
            $("#_nama").val("");
            $("#_nip").val("");
            $("#_jbtn").val("");
            $("#_lv").val("1");
            $("#_dinas").val("1");
            id_admin_glob = "";
            $("#update_admin").modal('hide');
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#btn_update_admin").click(function(){
            var data_main =  new FormData();
            data_main.append('id_admin' , id_admin_glob);
            data_main.append('nama' , $("#_nama").val());
            data_main.append('email', $("#_email").val());
            data_main.append('nip'  , $("#_nip").val());
            data_main.append('jbtn' , $("#_jbtn").val());
            data_main.append('dinas', $("#_dinas").val());
            data_main.append('lv'   , $("#_lv").val());
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/up/admin_main";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res){
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data admin berhasil diubah ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."admin/super/add_admin";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#_msg_email").html(detail_msg.email);
                        $("#_msg_nama").html(detail_msg.nama);
                        $("#_msg_nip").html(detail_msg.nip);
                        $("#_msg_jbtn").html(detail_msg.jabatan);
                        $("#_msg_lv").html(detail_msg.lv);
                        $("#_msg_id_bidang").html(detail_msg.id_bidang);
                        
                        swal("Proses Gagal.!!", "Data admin gagal diubah, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//
        function delete_admin(id_admin){
            !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: false 
                    }, function(){
                        var data_main =  new FormData();
                        data_main.append('id_admin', id_admin);
                                                    
                        $.ajax({
                            url: "<?php echo base_url()."super/act/del/admin_main";?>",
                            dataType: 'html',  // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,                         
                            type: 'post',
                            success: function(res){
                                // console.log(id_admin);
                                response_delete(res);
                            }
                        });
                    });                              
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),
            
            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res){
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if(main_msg.status){
                // console.log("true");
                
                swal({   
                    title: "Proses Berhasil.!!",   
                    text: "Data admin berhasil dihapus ..!",   
                    type: "success",   
                    showCancelButton: false,   
                    confirmButtonColor: "#28a745",   
                    confirmButtonText: "Lanjutkan",   
                    closeOnConfirm: false 
                }, function(){
                    window.location.href = "<?php echo base_url()."admin/super/add_admin";?>";
                });                              
                    

            }else{
                // console.log("false");
                swal("Proses Gagal.!!", "Data admin gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");                          
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//
                                   
    //=========================================================================//
    //-----------------------------------insert_lv_admin-----------------------//
    //=========================================================================//
        $("#add_lv").click(function(){
            console.log("#add_lv");
            var data_main =  new FormData();
            data_main.append('ket_lv', $("#ket_lv").val().toLowerCase());
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/add/lv_admin";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    // console.log(res);
                    $("#ket_lv").val("")
                    $("#out_act_lv").html(res);
                }
            });
        });
    //=========================================================================//
    //-----------------------------------insert_lv_admin-----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------delete_lv_admin-----------------------//
    //=========================================================================//
        function del_admin_lv(id_lv){
            !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus permanen, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: true 
                    }, function(){
                        
                        var data_main =  new FormData();
                        data_main.append('id_lv', id_lv);
                                                    
                        $.ajax({
                            url: "<?php echo base_url()."super/act/del/lv_admin";?>",
                            dataType: 'html',  // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,                         
                            type: 'post',
                            success: function(res){
                                console.log(res);
                                swal("Proses Berhasil.!!", "Penghapusan Data Berhasil", "success");
                                $("#ket_lv").val("");
                                $("#out_act_lv").html(res);

                            }
                        });   
                    });                                     
                },
                                          
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }
    //=========================================================================//
    //-----------------------------------delete_lv_admin-----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//
        $("#add_pass_new").click(function(){
            var data_main =  new FormData();
            data_main.append('id_admin', id_admin_op);

            data_main.append('key', key_ex);

            data_main.append('pass', $("#pass_new").val());
            data_main.append('repass', $("#repass_new").val());

            $.ajax({
                url: "<?php echo base_url()."super/act/repass/admin_main";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    response_pass_new(res);
                }
            });
        });

        function response_pass_new(res){
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Password admin berhasil diubah ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."admin/super/add_admin";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message                        
                        swal("Proses Gagal.!!", "Password admin gagal diubah, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }
        }

        function change_pass(id_admin){
            var data_main =  new FormData();
            data_main.append('id_admin', id_admin);
                                                    
            $.ajax({
                url: "<?php echo base_url()."super/act/chpass/admin_main";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    response_change_pass(res, id_admin);
                }
            });
        }

        function response_change_pass(res, id_admin){
            var data_json = JSON.parse(res);
            if(data_json.status){
                key_ex = data_json.val_key;
                id_admin_op = id_admin;

                $("#change_pass_modal").modal("show");
            }
        }
    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//
        
</script>