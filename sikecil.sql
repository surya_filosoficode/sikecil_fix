-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24 Jun 2019 pada 02.26
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sikecil`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin`(`nama` VARCHAR(64), `email` TEXT, `pass` VARCHAR(64), `time_update` DATETIME, `id_admin_in` VARCHAR(12), `username` VARCHAR(64)) RETURNS varchar(14) CHARSET latin1
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
   
  select count(*) into count_row_user from admin where substr(id_admin,3,6) = left(NOW()+0, 6);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, email, username,  pass, '0', nama,  '0', id_admin_in, '0000-00-00 00:00:00');
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_hutang`(`id_tipe` INT, `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kontak`(`id_user_in` VARCHAR(17), `id_tipe_vdr` INT(3), `nama_vdr` TEXT, `email_vdr` TEXT, `tlp_vdr` VARCHAR(13), `alamat_ktr_vdr` TEXT, `alamat_krm_vdr` TEXT, `website` TEXT, `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from kontak where substr(id_vdr,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_vdr into last_key_user from kontak
  	where substr(id_vdr,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_vdr desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into kontak values(fix_key_user, id_user_in, id_tipe_vdr,  nama_vdr, email_vdr, tlp_vdr, alamat_ktr_vdr, alamat_krm_vdr, website, "0", time_update, id_admin);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pembelian`(`id_tipe` INT, `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pengeluaran`(`id_tipe` INT, `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_penjualan`(`id_tipe` INT, `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_piutang`(`id_tipe` INT, `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_produk`(`id_tipe` INT, `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_user`(`username` VARCHAR(64), `email` TEXT, `password` VARCHAR(64), `nama_com` VARCHAR(64), `alamat_com` TEXT, `time_update` DATETIME, `id_admin` VARCHAR(12), `tlp_com` VARCHAR(13), `sts_active` ENUM('0','1','2'), `nama_user` TEXT) RETURNS varchar(17) CHARSET latin1
    NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(17);
  
  select count(*) into count_row_user from user 
  	where substr(id_user,4,8) = left(NOW()+0, 8);
        
  select id_user into last_key_user from user
  	where substr(id_user,4,8) = left(NOW()+0, 8)
  	order by id_user desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("USR",left(NOW()+0, 8),"000001");
  else
      	set fix_key_user = concat("USR", right(last_key_user, 14)+1);
      
  END IF;
  
  insert into user values(fix_key_user, username, email, nama_user, password, nama_com, alamat_com, tlp_com, sts_active, "0", time_update, id_admin);
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` char(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `username`, `password`, `status_active`, `nama`, `is_delete`, `admin_del`, `time_update`) VALUES
('AD2019050001', 'suryahanggarass@gmail.com', 'suryahanggara', '21232f297a57a5a743894a0e4a801fc3', '1', 'surya hanggaras', '0', '0', '2019-05-05 00:00:00'),
('AD2019050002', 'donisiregar@gmail.com', 'donisiregar', '21232f297a57a5a743894a0e4a801fc3', '0', 'doni siregar', '0', '0', '0000-00-00 00:00:00'),
('AD2019050003', 'roberthanggara19@gmail.com', 'suryahanggaras', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya', '1', 'qV/9yPjTM9Dd', '2019-05-08 10:50:08'),
('AD2019050004', 'suryahanggasra@gmail.com', 'suryadi', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya', '1', 'qV/9yPjTM9Dd', '2019-05-08 10:50:03'),
('AD2019050005', 'suryahanggaras@gmail.com', 'suryahanggarass', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya hanggara', '1', 'YVuqb09LxHU9', '2019-05-08 10:49:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak`
--

CREATE TABLE IF NOT EXISTS `kontak` (
  `id_vdr` varchar(28) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_tipe_vdr` int(3) NOT NULL,
  `nama_vdr` text NOT NULL,
  `email_vdr` text NOT NULL,
  `tlp_vdr` varchar(13) NOT NULL,
  `alamat_ktr_vdr` text NOT NULL,
  `alamat_krm_vdr` text NOT NULL,
  `website` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_vdr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kontak`
--

INSERT INTO `kontak` (`id_vdr`, `id_user`, `id_tipe_vdr`, `nama_vdr`, `email_vdr`, `tlp_vdr`, `alamat_ktr_vdr`, `alamat_krm_vdr`, `website`, `is_delete`, `time_update`, `id_admin`) VALUES
('USR20190512000011-2019060001', 'USR20190512000011', 1, 'surya', 'suryahanggara@gmail.com', '081230695774', 'malang', 'malang', 'https://www.google.com/', '0', '0000-00-00 00:00:00', '1'),
('USR20190512000011-2019060002', 'USR20190512000011', 1, 'surya', 'suryahanggara@gmail.com', '081230695774', 'malang', 'malang', 'https://www.google.com/', '0', '0000-00-00 00:00:00', '1'),
('USR20190512000012-2019060001', 'USR20190512000012', 1, 'surya', 'suryahanggara@gmail.com', '081230695774', 'malang', 'malang', 'https://www.google.com/', '0', '0000-00-00 00:00:00', '1'),
('USR20190512000015-2019060001', 'USR20190512000015', 1, 'surya', 'suryahanggara@gmail.com', '081230695774', 'malang', 'malang', 'https://www.google.com/', '0', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak_tipe`
--

CREATE TABLE IF NOT EXISTS `kontak_tipe` (
  `id_tipe_vdr` varchar(28) NOT NULL,
  `id_user` varchar(12) NOT NULL,
  `nama_tipe_vdr` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_tipe_vdr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE IF NOT EXISTS `pembelian` (
  `id_pembelian` varchar(28) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_vendor` varchar(28) NOT NULL,
  `jenis_bayar` enum('0','1') NOT NULL,
  `tgl_tempo_start` date NOT NULL,
  `tgl_tempo_finish` date NOT NULL,
  `tipe_bayar` varchar(50) NOT NULL,
  `keterangan_bayar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `no_faktur` text NOT NULL,
  `tgl_pembelian` datetime NOT NULL,
  `id_produk` varchar(28) NOT NULL,
  `jml_produk` int(11) NOT NULL,
  `disc` int(3) NOT NULL,
  `total_bayar` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_pembelian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengeluaran`
--

CREATE TABLE IF NOT EXISTS `pengeluaran` (
  `id_pengeluaran` varchar(28) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_vendor` varchar(28) NOT NULL,
  `jenis_bayar` enum('0','1') NOT NULL,
  `tgl_tempo_start` date NOT NULL,
  `tgl_tempo_finish` date NOT NULL,
  `tipe_bayar` varchar(50) NOT NULL,
  `keterangan_bayar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `no_faktur` text NOT NULL,
  `tgl_pengeluaran` datetime NOT NULL,
  `id_biaya` varchar(28) NOT NULL,
  `keterangan_biaya` text NOT NULL,
  `total_biaya` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_pengeluaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE IF NOT EXISTS `penjualan` (
  `id_penjualan` varchar(28) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_vendor` varchar(28) NOT NULL,
  `jenis_bayar` enum('0','1') NOT NULL,
  `tgl_tempo_start` date NOT NULL,
  `tgl_tempo_finish` date NOT NULL,
  `tipe_bayar` varchar(50) NOT NULL,
  `keterangan_bayar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `no_faktur` text NOT NULL,
  `tgl_penjualan` datetime NOT NULL,
  `id_produk` varchar(28) NOT NULL,
  `jml_produk` int(11) NOT NULL,
  `disc` int(3) NOT NULL,
  `total_bayar` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_penjualan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `id_prd` varchar(28) NOT NULL,
  `id_tipe` int(11) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `satuan_prd` varchar(32) NOT NULL,
  `harga_prd` varchar(32) NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_opname` int(11) NOT NULL,
  `nama_prd` varchar(64) NOT NULL,
  `desk_prd` text NOT NULL,
  `sts_jual` enum('0','1') NOT NULL,
  `sts_beli` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_prd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id_prd`, `id_tipe`, `id_user`, `satuan_prd`, `harga_prd`, `stok`, `stok_opname`, `nama_prd`, `desk_prd`, `sts_jual`, `sts_beli`, `is_delete`, `time_update`, `id_admin`) VALUES
('USR20190512000011-2019060001', 1, 'USR20190512000011', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000011-2019060002', 1, 'USR20190512000011', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000011-2019060003', 1, 'USR20190512000011', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000011-2019060004', 1, 'USR20190512000011', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000011-2019060005', 1, 'USR20190512000011', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000011-2019060006', 1, 'USR20190512000011', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000012-2019060001', 1, 'USR20190512000012', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000016-2019060001', 1, 'USR20190512000016', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000016-2019060002', 1, 'USR20190512000016', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000016-2019060003', 1, 'USR20190512000016', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0'),
('USR20190512000017-2019060001', 1, 'USR20190512000017', '1', '1', 1, 1, 'dika', 'dika', '1', '1', '0', '0000-00-00 00:00:00', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_tipe`
--

CREATE TABLE IF NOT EXISTS `produk_tipe` (
  `id_tipe` int(11) NOT NULL AUTO_INCREMENT,
  `ket_tipe` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_tipe`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `produk_tipe`
--

INSERT INTO `produk_tipe` (`id_tipe`, `ket_tipe`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'Barang', '0', '2019-05-13 06:28:55', 'AD2019050001'),
(2, 'Jasa', '0', '2019-05-13 06:29:40', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_bayar`
--

CREATE TABLE IF NOT EXISTS `tipe_bayar` (
  `id_bayar` int(11) NOT NULL AUTO_INCREMENT,
  `cara_bayar` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_bayar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_biaya`
--

CREATE TABLE IF NOT EXISTS `tipe_biaya` (
  `id_biaya` int(11) NOT NULL AUTO_INCREMENT,
  `cara_biaya` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_biaya`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` varchar(17) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` text NOT NULL,
  `nama_user` text NOT NULL,
  `password` varchar(64) NOT NULL,
  `nama_com` text NOT NULL,
  `alamat_com` text NOT NULL,
  `tlp_com` varchar(13) NOT NULL,
  `sts_active` enum('0','1','2') NOT NULL,
  `sts_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `email`, `nama_user`, `password`, `nama_com`, `alamat_com`, `tlp_com`, `sts_active`, `sts_delete`, `time_update`, `id_admin`) VALUES
('USR20190512000011', 'suryahanggara', 'suryahanggarao@gmail.com', 'suryarobertson', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'filososfi kopi', 'surabaya', '081230695774', '1', '0', '2019-05-12 02:46:39', '0'),
('USR20190620000012', 'wahyu', 'wahyu@gmail.com', 'surya hanggara', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'kanca_lawas', 'malang', '081230695774', '0', '0', '2019-06-20 00:00:00', '0'),
('USR20190620000013', 'dimas', 'dimas@gmail.com', 'dimas', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'dimas com', 'malang', '0071203218', '0', '0', '0000-00-00 00:00:00', '0'),
('USR20190620000014', 'dimas', 'dimas@gmail.com', 'dimas', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'dimas com', 'malang', '0071203218', '0', '0', '0000-00-00 00:00:00', '0'),
('USR20190620000015', 'dimas', 'dimas@gmail.com', 'dimas', '549e398b72f8f9e06bf87ec83bae6d0abdcaeed1c4f2c00e8d2e44a3adc2e12e', 'dimas com', 'malang', '0071203218', '0', '0', '0000-00-00 00:00:00', '0'),
('USR20190620000016', 'suryahanggaraz', 'suryahanggaraz@gmail.com', 'surya hanggara', 'ea1b9d779a37fa378d87c40dd6a56fcd491a7c9bef3a1f6e40228031bf00ac68', 'surya hanggara', 'malang', '081230695774', '0', '0', '2019-06-20 08:46:40', '0'),
('USR20190623000017', 'surya', 'suryafilosoficode@gmail.com', 'surya hanggara', 'b1f42dffc2f1b7577ba304fe7d1a6f28c9440d129147c00370829db54cc132e1', 'filosofi_code', 'malang', '081230695774', '1', '0', '2019-06-23 08:56:21', '0'),
('USR20190623000018', 'suryah', 'roberthanggara19@gmail.com', 'surya hanggara', 'ea1b9d779a37fa378d87c40dd6a56fcd491a7c9bef3a1f6e40228031bf00ac68', 'filosofi_code', 'malang', '081230695774', '1', '0', '2019-06-23 05:20:26', '0'),
('USR20190623000019', 'suryax', 'suryahanggara@gmail.com', 'surya hanggara', '671b2e039185e0baf32003ad023bab8a32e415fe94960b8871fea1248fce4370', 'filcode', 'malang', '081230695774', '1', '0', '2019-06-23 06:01:49', '0');

--
-- Trigger `user`
--
DROP TRIGGER IF EXISTS `auto_id_user`;
DELIMITER //
CREATE TRIGGER `auto_id_user` BEFORE INSERT ON `user`
 FOR EACH ROW BEGIN
  INSERT INTO user_seq VALUES (NULL,left(NOW()+0, 8));
  SET NEW.id_user = CONCAT('USR', left(NOW()+0, 8), LPAD(LAST_INSERT_ID(), 6, '0'));
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_seq`
--

CREATE TABLE IF NOT EXISTS `user_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data untuk tabel `user_seq`
--

INSERT INTO `user_seq` (`id`, `tgl`) VALUES
(1, '20190509'),
(2, '20190512'),
(3, '20190512'),
(4, '20190512'),
(5, '20190512'),
(6, '20190512'),
(7, '20190512'),
(8, '20190512'),
(9, '20190512'),
(10, '20190512'),
(11, '20190512'),
(12, '20190620'),
(13, '20190620'),
(14, '20190620'),
(15, '20190620'),
(16, '20190620'),
(17, '20190623'),
(18, '20190623'),
(19, '20190623');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_vert`
--

CREATE TABLE IF NOT EXISTS `user_vert` (
  `id_user` varchar(17) NOT NULL,
  `time` datetime NOT NULL,
  `param` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_vert`
--

INSERT INTO `user_vert` (`id_user`, `time`, `param`, `code`) VALUES
('USR20190620000016', '2019-06-20 08:46:40', '9bfd608db3ac9b9c56908d935263f3e4', 'c4d447f6c8b5b0740b54804ae5d9cee4'),
('USR20190512000011', '2019-06-23 17:59:07', '5a75a99fe1eb4d8bb6633643905f11e4', 'fa1aa38f73851ddb50f3396fa064c157');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_vert_ch_pass`
--

CREATE TABLE IF NOT EXISTS `user_vert_ch_pass` (
  `id_user` varchar(17) NOT NULL,
  `time` datetime NOT NULL,
  `param` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_vert_ch_pass`
--

INSERT INTO `user_vert_ch_pass` (`id_user`, `time`, `param`, `code`) VALUES
('USR20190512000011', '2019-06-23 17:53:13', '0f5dfd552fe5a0b7c3f945444cdd898e', '3def6ca1fd712e5e07c00bd35b55b677');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
