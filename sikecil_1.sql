-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15 Jul 2019 pada 02.05
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sikecil`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin`(`nama` VARCHAR(64), `email` TEXT, `pass` VARCHAR(64), `time_update` DATETIME, `id_admin_in` VARCHAR(12), `username` VARCHAR(64)) RETURNS varchar(14) CHARSET latin1
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
   
  select count(*) into count_row_user from admin where substr(id_admin,3,6) = left(NOW()+0, 6);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, email, username,  pass, '0', nama,  '0', id_admin_in, '0000-00-00 00:00:00');
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_hutang`(`id_user_in` VARCHAR(17), `no_faktur` VARCHAR(34), `tgl_bayar` DATETIME, `cara_bayar` TEXT, `total_bayar` VARCHAR(50), `ket_hutang` TEXT, `tgl_input` DATE, `time_update` DATETIME) RETURNS varchar(34) CHARSET latin1
BEGIN
  declare last_key_user varchar(34);
  declare count_row_user int;
  declare fix_key_user varchar(34);
   
  select count(*) into count_row_user from hutang where substr(id_hutang,22,8) = left(NOW()+0, 8) and id_user = id_user_in;
        
  select id_hutang into last_key_user from hutang
  	where substr(id_hutang,22,8) = left(NOW()+0, 8)
    and id_user = id_user_in
  	order by id_hutang desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-","HT","-",left(NOW()+0, 8),"00001");
  else
      	set fix_key_user = concat(id_user_in,"-","HT","-",RIGHT(last_key_user, 13)+1);
      
  END IF;
  
  
  insert into hutang values(fix_key_user, id_user_in, no_faktur,  tgl_bayar, cara_bayar, total_bayar, ket_hutang, tgl_input, "0", time_update, "0");
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kontak`(`id_user_in` VARCHAR(17), `id_tipe_vdr` VARCHAR(64), `nama_vdr` TEXT, `email_vdr` TEXT, `tlp_vdr` VARCHAR(13), `alamat_ktr_vdr` TEXT, `alamat_krm_vdr` TEXT, `website` TEXT, `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from kontak where substr(id_vdr,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_vdr into last_key_user from kontak
  	where substr(id_vdr,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_vdr desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into kontak values(fix_key_user, id_user_in, id_tipe_vdr,  nama_vdr, email_vdr, tlp_vdr, alamat_ktr_vdr, alamat_krm_vdr, website, "0", time_update, id_admin);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pembelian`(`id_user_in` VARCHAR(17), `id_vendor` VARCHAR(28), `jenis_bayar` ENUM('0','1'), `tgl_tempo_start` DATE, `tgl_tempo_finish` DATE, `sisa_pinjaman` VARCHAR(50), `sts_piutang` ENUM('0','1'), `tipe_bayar` VARCHAR(50), `keterangan_bayar` TEXT, `deskripsi` TEXT, `no_faktur` TEXT, `tgl_pembelian` DATE, `id_produk` VARCHAR(28), `jml_produk` INT(11), `disc` INT(3), `total_bayar` VARCHAR(50), `time_update` DATETIME, `harga_satuan` VARCHAR(64)) RETURNS varchar(34) CHARSET latin1
BEGIN
  declare last_key_user varchar(34);
  declare count_row_user int;
  declare fix_key_user varchar(34);
   
  select count(*) into count_row_user from pembelian where substr(id_pembelian,22,8) = left(NOW()+0, 8) and id_user = id_user_in;
        
  select id_pembelian into last_key_user from pembelian
  	where substr(id_pembelian,22,8) = left(NOW()+0, 8)
    and id_user = id_user_in
  	order by id_pembelian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-","PB","-",left(NOW()+0, 8),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-","PB","-",RIGHT(last_key_user, 12)+1);
      
  END IF;
  
  
  insert into pembelian values(fix_key_user, id_user_in, id_vendor,  jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_piutang, tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_pembelian, id_produk, jml_produk, disc, harga_satuan, total_bayar, "0", "0", time_update);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pengeluaran`(`id_user_in` VARCHAR(17), `id_vendor` VARCHAR(28), `jenis_bayar` ENUM('0','1'), `tgl_tempo_start` DATE, `tgl_tempo_finish` DATE, `sisa_pinjaman` VARCHAR(50), `sts_pinjaman` ENUM('0','1'), `tipe_bayar` TEXT, `keterangan_bayar` TEXT, `deskripsi` TEXT, `no_faktur` TEXT, `tgl_pengeluaran` DATETIME, `id_biaya` TEXT, `keterangan_biaya` TEXT, `total_biaya` VARCHAR(50), `time_update` DATETIME) RETURNS varchar(34) CHARSET latin1
BEGIN
  declare last_key_user varchar(34);
  declare count_row_user int;
  declare fix_key_user varchar(34);
   
  select count(*) into count_row_user from pengeluaran where substr(id_pengeluaran,22,8) = left(NOW()+0, 8) and id_user = id_user_in;
        
  select id_pengeluaran into last_key_user from pengeluaran
  	where substr(id_pengeluaran,22,8) = left(NOW()+0, 8)
    and id_user = id_user_in
  	order by id_pengeluaran desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-","PG","-",left(NOW()+0, 8),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-","PG","-",RIGHT(last_key_user, 12)+1);
      
  END IF;
  
  insert into pengeluaran values(fix_key_user, id_user_in, id_vendor,  jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_pinjaman, tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_pengeluaran, id_biaya, keterangan_biaya, total_biaya, "0", "0", time_update);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_penjualan`(`id_user_in` VARCHAR(17), `id_vendor` VARCHAR(28), `jenis_bayar` ENUM('0','1'), `tgl_tempo_start` DATE, `tgl_tempo_finish` DATE, `sisa_pinjaman` VARCHAR(50), `sts_piutang` ENUM('0','1'), `tipe_bayar` VARCHAR(50), `keterangan_bayar` TEXT, `deskripsi` TEXT, `no_faktur` TEXT, `tgl_penjualan` DATE, `id_produk` VARCHAR(28), `jml_produk` INT(11), `disc` INT(3), `total_bayar` VARCHAR(50), `time_update` DATETIME, `harga_satuan` VARCHAR(64)) RETURNS varchar(34) CHARSET latin1
BEGIN
  declare last_key_user varchar(34);
  declare count_row_user int;
  declare fix_key_user varchar(34);
   
  select count(*) into count_row_user from penjualan where substr(id_penjualan,22,8) = left(NOW()+0, 8) and id_user = id_user_in;
        
  select id_penjualan into last_key_user from penjualan
  	where substr(id_penjualan,22,8) = left(NOW()+0, 8)
    and id_user = id_user_in
  	order by id_penjualan desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-","PJ","-",left(NOW()+0, 8),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-","PJ","-",RIGHT(last_key_user, 12)+1);
      
  END IF;
  
  
	insert into penjualan values(fix_key_user, id_user_in, id_vendor,  jenis_bayar, tgl_tempo_start, tgl_tempo_finish, sisa_pinjaman, sts_piutang, tipe_bayar, keterangan_bayar, deskripsi, no_faktur, tgl_penjualan, id_produk, jml_produk, disc, harga_satuan, total_bayar, "0", "0", time_update);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_piutang`(`id_user_in` VARCHAR(17), `no_faktur` VARCHAR(34), `tgl_bayar` DATETIME, `cara_bayar` TEXT, `total_bayar` VARCHAR(50), `ket_piutang` TEXT, `tgl_input` DATE, `time_update` DATETIME) RETURNS varchar(34) CHARSET latin1
BEGIN
  declare last_key_user varchar(34);
  declare count_row_user int;
  declare fix_key_user varchar(34);
   
  select count(*) into count_row_user from piutang where substr(id_piutang,22,8) = left(NOW()+0, 8) and id_user = id_user_in;
        
  select id_piutang into last_key_user from piutang
  	where substr(id_piutang,22,8) = left(NOW()+0, 8)
    and id_user = id_user_in
  	order by id_piutang desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-","PT","-",left(NOW()+0, 8),"00001");
  else
      	set fix_key_user = concat(id_user_in,"-","PT","-",RIGHT(last_key_user, 13)+1);
      
  END IF;
  
  
  insert into piutang values(fix_key_user, id_user_in, no_faktur,  tgl_bayar, cara_bayar, total_bayar, ket_piutang, tgl_input, "0", time_update, "0");
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_produk`(`id_tipe` VARCHAR(64), `id_user_in` VARCHAR(17), `satuan_prd` VARCHAR(32), `harga_prd` VARCHAR(32), `harga_beli_satuan` VARCHAR(64), `stok` INT, `stok_opname` INT, `nama_prd` VARCHAR(64), `desk_prd` TEXT, `sts_jual` ENUM('0','1'), `sts_beli` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS varchar(28) CHARSET latin1
BEGIN
  declare last_key_user varchar(28);
  declare count_row_user int;
  declare fix_key_user varchar(28);
   
  select count(*) into count_row_user from produk where substr(id_prd,19,6) = left(NOW()+0, 6) and id_user = id_user_in;
        
  select id_prd into last_key_user from produk
  	where substr(id_prd,19,6) = left(NOW()+0, 6)
    and id_user = id_user_in
  	order by id_prd desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(id_user_in,"-",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat(id_user_in,"-",RIGHT(last_key_user, 10)+1);
      
  END IF;
  
  
  insert into produk values(fix_key_user, id_tipe, id_user_in,  satuan_prd, harga_prd, harga_beli_satuan, stok, stok_opname, nama_prd, desk_prd, sts_jual, sts_beli, "0", time_update, id_admin);
    
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_user`(`username` VARCHAR(64), `email` TEXT, `password` VARCHAR(64), `nama_com` VARCHAR(64), `alamat_com` TEXT, `time_update` DATETIME, `id_admin` VARCHAR(12), `tlp_com` VARCHAR(13), `sts_active` ENUM('0','1','2'), `nama_user` TEXT) RETURNS varchar(17) CHARSET latin1
    NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(17);
  
  select count(*) into count_row_user from user 
  	where substr(id_user,4,8) = left(NOW()+0, 8);
        
  select id_user into last_key_user from user
  	where substr(id_user,4,8) = left(NOW()+0, 8)
  	order by id_user desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("USR",left(NOW()+0, 8),"000001");
  else
      	set fix_key_user = concat("USR", right(last_key_user, 14)+1);
      
  END IF;
  
  insert into user values(fix_key_user, username, email, nama_user, password, nama_com, alamat_com, tlp_com, sts_active, "0", time_update, id_admin);
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` char(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `email`, `username`, `password`, `status_active`, `nama`, `is_delete`, `admin_del`, `time_update`) VALUES
('AD2019050001', 'suryahanggarass@gmail.com', 'suryahanggara', '21232f297a57a5a743894a0e4a801fc3', '1', 'surya hanggaras', '0', '0', '2019-05-05 00:00:00'),
('AD2019050002', 'donisiregar@gmail.com', 'donisiregar', '21232f297a57a5a743894a0e4a801fc3', '0', 'doni siregar', '0', '0', '0000-00-00 00:00:00'),
('AD2019050003', 'roberthanggara19@gmail.com', 'suryahanggaras', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya', '1', 'qV/9yPjTM9Dd', '2019-05-08 10:50:08'),
('AD2019050004', 'suryahanggasra@gmail.com', 'suryadi', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya', '1', 'qV/9yPjTM9Dd', '2019-05-08 10:50:03'),
('AD2019050005', 'suryahanggaras@gmail.com', 'suryahanggarass', '21232f297a57a5a743894a0e4a801fc3', '0', 'surya hanggara', '1', 'YVuqb09LxHU9', '2019-05-08 10:49:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hutang`
--

CREATE TABLE IF NOT EXISTS `hutang` (
  `id_hutang` varchar(34) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `no_faktur` varchar(34) NOT NULL,
  `tgl_bayar` datetime NOT NULL,
  `cara_bayar` text NOT NULL,
  `total_bayar` varchar(50) NOT NULL,
  `ket_hutang` text NOT NULL,
  `tgl_input` date NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_hutang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak`
--

CREATE TABLE IF NOT EXISTS `kontak` (
  `id_vdr` varchar(28) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_tipe_vdr` varchar(64) NOT NULL,
  `nama_vdr` text NOT NULL,
  `email_vdr` text NOT NULL,
  `tlp_vdr` varchar(13) NOT NULL,
  `alamat_ktr_vdr` text NOT NULL,
  `alamat_krm_vdr` text NOT NULL,
  `website` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_vdr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kontak`
--

INSERT INTO `kontak` (`id_vdr`, `id_user`, `id_tipe_vdr`, `nama_vdr`, `email_vdr`, `tlp_vdr`, `alamat_ktr_vdr`, `alamat_krm_vdr`, `website`, `is_delete`, `time_update`, `id_admin`) VALUES
('USR20190706000001-2019070001', 'USR20190706000001', 'vendor', 'PT Sampoerna UI Tbk.', 'sampoernaui.gmail.com', '081230789121', 'malang utara', 'malang utara', 'sampoernaui.com', '0', '2019-07-06 08:04:09', '0'),
('USR20190706000001-2019070002', 'USR20190706000001', 'suplier', 'PT. Djaya Sentosa Bersama Tbk.', 'jaya@gmail.com', '081234567765', 'malang', 'malang', 'jaya.com', '0', '2019-07-11 12:51:25', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kontak_tipe`
--

CREATE TABLE IF NOT EXISTS `kontak_tipe` (
  `id_tipe_vdr` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tipe_vdr` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_tipe_vdr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `kontak_tipe`
--

INSERT INTO `kontak_tipe` (`id_tipe_vdr`, `nama_tipe_vdr`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'suplier', '0', '0000-00-00 00:00:00', '0'),
(2, 'vendor', '0', '2019-05-05 00:00:00', '0'),
(3, 'dimasxxx xasdas23432 sdsadas-gfsfasds', '1', '2019-07-03 06:38:35', 'AD2019050001'),
(4, 'dono', '1', '2019-07-03 06:38:40', 'AD2019050001'),
(5, 'johan aa', '1', '2019-07-03 06:52:14', 'AD2019050001'),
(6, 'nomadenff dsadsad', '1', '2019-07-03 06:52:17', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE IF NOT EXISTS `pembelian` (
  `id_pembelian` varchar(34) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_vdr` varchar(28) NOT NULL,
  `jenis_bayar` enum('0','1') NOT NULL,
  `tgl_tempo_start` date NOT NULL,
  `tgl_tempo_finish` date NOT NULL,
  `sisa_pinjaman` varchar(50) NOT NULL,
  `sts_pinjaman` enum('0','1') NOT NULL,
  `tipe_bayar` varchar(50) NOT NULL,
  `keterangan_bayar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `no_faktur` text NOT NULL,
  `tgl_pembelian` datetime NOT NULL,
  `id_prd` varchar(28) NOT NULL,
  `jml_produk` int(11) NOT NULL,
  `disc` int(3) NOT NULL,
  `harga_satuan` varchar(64) NOT NULL,
  `total_bayar` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_pembelian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `id_user`, `id_vdr`, `jenis_bayar`, `tgl_tempo_start`, `tgl_tempo_finish`, `sisa_pinjaman`, `sts_pinjaman`, `tipe_bayar`, `keterangan_bayar`, `deskripsi`, `no_faktur`, `tgl_pembelian`, `id_prd`, `jml_produk`, `disc`, `harga_satuan`, `total_bayar`, `sts_active`, `is_delete`, `time_update`) VALUES
('USR20190706000001-PB-201907150002', 'USR20190706000001', 'USR20190706000001-2019070001', '1', '2019-07-15', '2019-07-31', '4750000.0', '0', 'Lainnya', 'Pembelian Lilin Second', 'Pembelian Lilin Second', '-', '2019-07-15 00:00:00', 'USR20190706000001-2019070001', 50, 5, '100000', '4750000', '0', '0', '2019-07-15 01:09:23'),
('USR20190706000001-PB-201907150003', 'USR20190706000001', 'USR20190706000001-2019070001', '0', '0000-00-00', '0000-00-00', '0', '0', 'Uang Tunai', '-', 'Pembelian Lilin', '123', '2019-07-06 00:00:00', 'USR20190706000001-2019070001', 20, 5, '100000', '1900000', '0', '0', '2019-07-15 01:09:43'),
('USR20190706000001-PB-201907150004', 'USR20190706000001', 'USR20190706000001-2019070001', '1', '2019-07-05', '2019-07-30', '1900000', '1', '0', '0', 'Pembelian Lilin', '123', '2019-07-05 00:00:00', 'USR20190706000001-2019070001', 20, 5, '100000', '1900000', '0', '0', '2019-07-15 01:10:55'),
('USR20190706000001-PB-201907150005', 'USR20190706000001', 'USR20190706000001-2019070002', '1', '2019-07-15', '2019-07-31', '392000.0', '0', 'Lainnya', 'Pembelian kertas mili meter', 'Pembelian kertas mili meter', '-', '2019-07-15 00:00:00', 'USR20190706000001-2019070002', 20, 2, '20000', '392000', '0', '0', '2019-07-15 01:28:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengeluaran`
--

CREATE TABLE IF NOT EXISTS `pengeluaran` (
  `id_pengeluaran` varchar(34) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_vdr` varchar(28) NOT NULL,
  `jenis_bayar` enum('0','1') NOT NULL,
  `tgl_tempo_start` date NOT NULL,
  `tgl_tempo_finish` date NOT NULL,
  `sisa_pinjaman` varchar(50) NOT NULL,
  `sts_pinjaman` enum('0','1') NOT NULL,
  `tipe_bayar` varchar(50) NOT NULL,
  `keterangan_bayar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `no_faktur` text NOT NULL,
  `tgl_pengeluaran` datetime NOT NULL,
  `id_biaya` varchar(28) NOT NULL,
  `keterangan_biaya` text NOT NULL,
  `total_biaya` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_pengeluaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengeluaran`
--

INSERT INTO `pengeluaran` (`id_pengeluaran`, `id_user`, `id_vdr`, `jenis_bayar`, `tgl_tempo_start`, `tgl_tempo_finish`, `sisa_pinjaman`, `sts_pinjaman`, `tipe_bayar`, `keterangan_bayar`, `deskripsi`, `no_faktur`, `tgl_pengeluaran`, `id_biaya`, `keterangan_biaya`, `total_biaya`, `sts_active`, `is_delete`, `time_update`) VALUES
('USR20190706000001-PG-201907150001', 'USR20190706000001', 'USR20190706000001-2019070001', '1', '2019-07-05', '2019-07-30', '1900000', '1', '0', '0', 'Pembelian Lilin', '123', '2019-07-05 00:00:00', 'Biaya listrik', 'pembayaran beban listrik', '200000', '0', '0', '2019-07-15 01:49:58'),
('USR20190706000001-PG-201907150002', 'USR20190706000001', 'USR20190706000001-2019070001', '1', '2019-07-05', '2019-07-30', '1900000', '1', '0', '0', 'Pembelian Lilin', '123', '2019-07-05 00:00:00', 'Biaya listrik', 'pembayaran beban listrik', '200000', '0', '0', '2019-07-15 01:50:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE IF NOT EXISTS `penjualan` (
  `id_penjualan` varchar(34) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `id_vdr` varchar(28) NOT NULL,
  `jenis_bayar` enum('0','1') NOT NULL,
  `tgl_tempo_start` date NOT NULL,
  `tgl_tempo_finish` date NOT NULL,
  `sisa_pinjaman` varchar(50) NOT NULL,
  `sts_pinjaman` enum('0','1') NOT NULL,
  `tipe_bayar` varchar(50) NOT NULL,
  `keterangan_bayar` text NOT NULL,
  `deskripsi` text NOT NULL,
  `no_faktur` text NOT NULL,
  `tgl_penjualan` datetime NOT NULL,
  `id_prd` varchar(28) NOT NULL,
  `jml_produk` int(11) NOT NULL,
  `disc` int(3) NOT NULL,
  `harga_satuan` varchar(64) NOT NULL,
  `total_bayar` varchar(50) NOT NULL,
  `sts_active` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_penjualan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan`
--

INSERT INTO `penjualan` (`id_penjualan`, `id_user`, `id_vdr`, `jenis_bayar`, `tgl_tempo_start`, `tgl_tempo_finish`, `sisa_pinjaman`, `sts_pinjaman`, `tipe_bayar`, `keterangan_bayar`, `deskripsi`, `no_faktur`, `tgl_penjualan`, `id_prd`, `jml_produk`, `disc`, `harga_satuan`, `total_bayar`, `sts_active`, `is_delete`, `time_update`) VALUES
('USR20190706000001-PJ-201907070003', 'USR20190706000001', 'USR20190706000001-2019070001', '1', '2019-07-06', '2019-07-30', '0', '1', '0', '0', 'Pembelian rokok', '123', '2019-07-06 00:00:00', 'USR20190706000001-2019070001', 20, 5, '10000', '190000', '0', '0', '2019-07-07 11:57:54'),
('USR20190706000001-PJ-201907100001', 'USR20190706000001', 'USR20190706000001-2019070001', '0', '0000-00-00', '0000-00-00', '0', '0', 'Uang Tunai', '-', 'Pembelian rokok', '123', '2019-07-06 00:00:00', 'USR20190706000001-2019070001', 20, 5, '10000', '190000', '0', '0', '2019-07-10 01:36:05'),
('USR20190706000001-PJ-201907130001', 'USR20190706000001', 'USR20190706000001-2019070001', '0', '0000-00-00', '0000-00-00', '0', '0', 'Cash', 'Pembelian Obat Herbal', 'Pembelian Obat Herbal', '-', '2019-07-13 00:00:00', 'USR20190706000001-2019070002', 20, 2, '20000', '392000', '0', '0', '2019-07-13 12:13:49'),
('USR20190706000001-PJ-201907130002', 'USR20190706000001', 'USR20190706000001-2019070002', '0', '0000-00-00', '0000-00-00', '0', '1', 'Cash', 'Pembelian rumah KPR', 'Pembelian rumah KPR', '-', '2019-07-05 00:00:00', 'USR20190706000001-2019070001', 200, 10, '3000000', '540000000', '0', '0', '2019-07-13 12:16:29'),
('USR20190706000001-PJ-201907130003', 'USR20190706000001', 'USR20190706000001-2019070001', '0', '0000-00-00', '0000-00-00', '0', '1', 'Transfer Bank', 'Pembelian property rumah', 'Pembelian property rumah', '-', '2019-07-12 00:00:00', 'USR20190706000001-2019070001', 300, 3, '4000000', '1170000000', '0', '0', '2019-07-13 12:18:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `piutang`
--

CREATE TABLE IF NOT EXISTS `piutang` (
  `id_piutang` varchar(34) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `no_faktur` varchar(34) NOT NULL,
  `tgl_bayar` datetime NOT NULL,
  `cara_bayar` text NOT NULL,
  `total_bayar` varchar(50) NOT NULL,
  `ket_piutang` text NOT NULL,
  `tgl_input` date NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_piutang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `piutang`
--

INSERT INTO `piutang` (`id_piutang`, `id_user`, `no_faktur`, `tgl_bayar`, `cara_bayar`, `total_bayar`, `ket_piutang`, `tgl_input`, `is_delete`, `time_update`, `id_admin`) VALUES
('USR20190706000001-PT-2019071400001', 'USR20190706000001', 'USR20190706000001-PJ-201907070003', '2019-07-13 00:00:00', 'Cash', '40000', 'Pelunasan piutang kavling tanah', '2019-07-14', '0', '2019-07-14 01:20:51', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `id_prd` varchar(28) NOT NULL,
  `id_tipe` varchar(64) NOT NULL,
  `id_user` varchar(17) NOT NULL,
  `satuan_prd` varchar(32) NOT NULL,
  `harga_prd` varchar(32) NOT NULL,
  `harga_beli_satuan` varchar(64) NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_opname` int(11) NOT NULL,
  `nama_prd` varchar(64) NOT NULL,
  `desk_prd` text NOT NULL,
  `sts_jual` enum('0','1') NOT NULL,
  `sts_beli` enum('0','1') NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_prd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id_prd`, `id_tipe`, `id_user`, `satuan_prd`, `harga_prd`, `harga_beli_satuan`, `stok`, `stok_opname`, `nama_prd`, `desk_prd`, `sts_jual`, `sts_beli`, `is_delete`, `time_update`, `id_admin`) VALUES
('USR20190706000001-2019070001', 'Barang', 'USR20190706000001', 'm', '30000', '25000', -530, 0, 'Tanah Kavling', 'Tanah Kavling', '1', '1', '0', '2019-07-06 07:39:35', '0'),
('USR20190706000001-2019070002', 'Barang', 'USR20190706000001', 'pcs', '20000', '15000', 100, 0, 'Lampu neon', 'Lampu neon', '1', '1', '0', '2019-07-11 12:46:18', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_tipe`
--

CREATE TABLE IF NOT EXISTS `produk_tipe` (
  `id_tipe` int(11) NOT NULL AUTO_INCREMENT,
  `ket_tipe` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_tipe`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `produk_tipe`
--

INSERT INTO `produk_tipe` (`id_tipe`, `ket_tipe`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'Barang', '0', '2019-05-13 06:28:55', 'AD2019050001'),
(2, 'Jasa', '0', '2019-05-13 06:29:40', 'AD2019050001'),
(3, 'masis', '1', '2019-07-03 06:51:17', 'AD2019050001'),
(4, 'simad', '1', '2019-07-03 06:51:21', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE IF NOT EXISTS `satuan` (
  `id_satuan` int(11) NOT NULL AUTO_INCREMENT,
  `ket_satuan` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_satuan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `ket_satuan`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'cm', '0', '2019-07-03 07:05:57', 'AD2019050001'),
(2, 'jam', '0', '2019-07-03 07:07:21', 'AD2019050001'),
(3, 'menit', '0', '2019-07-03 07:07:27', 'AD2019050001'),
(4, 'detik', '0', '2019-07-03 07:07:34', 'AD2019050001'),
(5, 'asu kon', '1', '2019-07-03 07:08:16', 'AD2019050001'),
(6, 'temenan aa', '1', '2019-07-03 07:08:11', 'AD2019050001'),
(7, 'wih kon', '1', '2019-07-03 07:09:15', 'AD2019050001'),
(8, 'jamban jembarsut', '1', '2019-07-03 07:09:12', 'AD2019050001'),
(9, 'dafuk donat', '1', '2019-07-03 07:09:09', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_bayar`
--

CREATE TABLE IF NOT EXISTS `tipe_bayar` (
  `id_bayar` int(11) NOT NULL AUTO_INCREMENT,
  `cara_bayar` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_bayar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `tipe_bayar`
--

INSERT INTO `tipe_bayar` (`id_bayar`, `cara_bayar`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'Lainnya', '0', '2019-07-03 00:00:00', '0'),
(2, 'Cash', '0', '2019-07-03 00:00:00', '0'),
(3, 'Transfer Bank', '0', '2019-07-03 00:00:00', '0'),
(4, 'Wesel', '0', '2019-07-03 00:00:00', '0'),
(5, 'Cek Langsung', '0', '2019-07-03 00:00:00', '0'),
(6, 'caraku asd asd as', '1', '2019-07-03 06:53:23', 'AD2019050001'),
(7, 'caramuasdasda s sad', '1', '2019-07-03 06:53:18', 'AD2019050001'),
(8, 'asru ss sadsa dsa fasdsa', '1', '2019-07-03 06:53:14', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_biaya`
--

CREATE TABLE IF NOT EXISTS `tipe_biaya` (
  `id_biaya` int(11) NOT NULL AUTO_INCREMENT,
  `ket_tipe_biaya` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_biaya`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `tipe_biaya`
--

INSERT INTO `tipe_biaya` (`id_biaya`, `ket_tipe_biaya`, `is_delete`, `time_update`, `id_admin`) VALUES
(1, 'Biaya admin', '0', '0000-00-00 00:00:00', '0'),
(2, 'Biaya listrik', '0', '2019-07-03 00:00:00', '0'),
(3, 'Biaya Balik Nama xx', '1', '2019-07-03 06:58:13', 'AD2019050001'),
(4, 'Balik Badan zz', '1', '2019-07-03 06:58:08', 'AD2019050001'),
(5, 'Testing broo', '1', '2019-07-03 06:58:38', 'AD2019050001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` varchar(17) NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` text NOT NULL,
  `nama_user` text NOT NULL,
  `password` varchar(64) NOT NULL,
  `nama_com` text NOT NULL,
  `alamat_com` text NOT NULL,
  `tlp_com` varchar(13) NOT NULL,
  `sts_active` enum('0','1','2') NOT NULL,
  `sts_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `email`, `nama_user`, `password`, `nama_com`, `alamat_com`, `tlp_com`, `sts_active`, `sts_delete`, `time_update`, `id_admin`) VALUES
('USR20190706000001', 'surya', 'suryahanggara@gmail.com', 'surya hanggara', 'ea1b9d779a37fa378d87c40dd6a56fcd491a7c9bef3a1f6e40228031bf00ac68', 'filosofi_code', 'malang', '081230695774', '1', '0', '2019-07-06 07:20:47', '0');

--
-- Trigger `user`
--
DROP TRIGGER IF EXISTS `auto_id_user`;
DELIMITER //
CREATE TRIGGER `auto_id_user` BEFORE INSERT ON `user`
 FOR EACH ROW BEGIN
  INSERT INTO user_seq VALUES (NULL,left(NOW()+0, 8));
  SET NEW.id_user = CONCAT('USR', left(NOW()+0, 8), LPAD(LAST_INSERT_ID(), 6, '0'));
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_seq`
--

CREATE TABLE IF NOT EXISTS `user_seq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `user_seq`
--

INSERT INTO `user_seq` (`id`, `tgl`) VALUES
(1, '20190706');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_vert`
--

CREATE TABLE IF NOT EXISTS `user_vert` (
  `id_user` varchar(17) NOT NULL,
  `time` datetime NOT NULL,
  `param` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_vert_ch_pass`
--

CREATE TABLE IF NOT EXISTS `user_vert_ch_pass` (
  `id_user` varchar(17) NOT NULL,
  `time` datetime NOT NULL,
  `param` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
